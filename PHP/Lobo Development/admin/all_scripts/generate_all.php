<?php

	// Dump to htm Page
	include_once "../../dump_script.php";
	
// page generation function for search engine optimization
function GenerateAllPages() {
	global $db;
	
	echo "Initiating page generation <br>";


	// Portfolio
	echo "Generating Portfolio <br>";
	$portfolio_items = $db->get_results("SELECT * FROM Portfolio ORDER BY PageTitle");

	foreach ($portfolio_items as $portfolio_items_output) {
		$portfolio_Category_items = $db->get_row("SELECT * FROM PortfolioCategories WHERE CategoryName = '$portfolio_items_output->Category'");
		//echo $portfolio_Category_items->popup_menu_id;
		GenereateSearchPage('portfolio_' . $portfolio_items_output->id, $portfolio_items_output->PageTitle, $portfolio_items_output->Page,'#'.$portfolio_Category_items->popup_menu_id.'-LoadPortfolio('.$portfolio_items_output->id.')');
	}
	
	
	
	// Bio
	echo "Generating Bio <br>";
	$bio_items = $db->get_results("SELECT * FROM Bios ORDER BY PageTitle");

	foreach ($bio_items as $bio_items_output) {				
		GenereateSearchPage('bio_' . $bio_items_output->id, $bio_items_output->PageTitle, $bio_items_output->Page, '#LoadBio('.$bio_items_output->id.')');
	}
	
	// On the Boards
	echo "Generating On the Boards <br>";
	$bio_items = $db->get_results("SELECT * FROM OnTheBoards ORDER BY PageTitle");

	foreach ($bio_items as $bio_items_output) {				
	//	GenereateSearchPage('bio_' . $bio_items_output->id, $bio_items_output->PageTitle, $bio_items_output->Page, '#LoadBio('.$bio_items_output->id.')');
		GenereateSearchPage('on_the_boards_' . $bio_items_output->id, $bio_items_output->Title, $bio_items_output->Page, '#OTBContent');
	}


	// Web Pages
	echo "Generating Web Pages <br>";
	$items = $db->get_results("SELECT * FROM WebPages ORDER BY PageTitle");

	foreach ($items as $results) {				
	//	GenereateSearchPage('bio_' . $bio_items_output->id, $bio_items_output->PageTitle, $bio_items_output->Page, '#LoadBio('.$bio_items_output->id.')');
		//GenereateSearchPage('on_the_boards_' . $bio_items_output->id, $bio_items_output->Title, $bio_items_output->Page, '#OTBContent');
		GenereateSearchPage($results->Category.'_'.$results->PageName, $results->PageName, $results->Page, '#'.$results->Category.'Content('.$results->PageName.')');		
	}

	// Under Construction
	echo "Generating Under Construction<br>";
	$items = $db->get_results("SELECT * FROM UnderConstruction ORDER BY PageTitle");

	foreach ($items as $results) {				
	//	GenereateSearchPage('bio_' . $bio_items_output->id, $bio_items_output->PageTitle, $bio_items_output->Page, '#LoadBio('.$bio_items_output->id.')');
		//GenereateSearchPage('on_the_boards_' . $bio_items_output->id, $bio_items_output->Title, $bio_items_output->Page, '#OTBContent');
		//GenereateSearchPage($results->Category.'_'.$results->PageName, $results->PageName, $results->Page, '#'.$results->Category.'Content('.$results->PageName.')');	
		GenereateSearchPage('under_construction_' . $results->content_id, $results->PageTitle, $results->Page, '#UCContent');			
	}	
	
	

	echo "<br>Page generation complete";
}

?>

