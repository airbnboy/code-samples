<?PHP

include_once "../all_scripts/auth.php";include_once "../ez_sql.php";


include_once "../all_scripts/admin_functions.php";
$classes_id     = $_POST["classes_id"];
$class_name = $_POST['class_name'];
$course_number = $_POST['course_number'];   
$UserRef = $_SESSION["id"];


// Get DB action from either post or get

$action = $_GET['action'];
if ($action == NULL) {
	$action = $_POST['action'];
}

$CalendarId = $_GET['CalendarId'];
if ($CalendarId == NULL) {
	$CalendarId = $_POST['CalendarId'];
}

$next_link = $_GET['next_link'];
if ($next_link == NULL) {
	$next_link = $_POST['next_link'];
}

$Term = $_GET['Term'];
if ($Term == NULL) {
	$Term = $_POST['Term'];
}

$CalendarName = $_GET['CalendarName'];
if ($CalendarName == NULL) {
	$CalendarName = $_POST['CalendarName'];
}

$CalendarType = $_GET['CalendarType'];
if ($CalendarType == NULL) {
	$CalendarType = $_POST['CalendarType'];
}

$Event     = addslashes($_POST["Event"]);
$startmonth      = $_POST["startmonth"];
$startday      = $_POST["startday"];
$startyear      = $_POST["startyear"];
$starthour      = $_POST["starthour"];
$startminute      = $_POST["startminute"];
$startsecond      = $_POST["startsecond"];
$endmonth	= $_POST["endmonth"];
$endday     = $_POST["endday"];
$endyear      = $_POST["endyear"];
$endhour      = $_POST["endhour"];
$endminute      = $_POST["endminute"];
$endsecond      = $_POST["endsecond"];
$Page     = addslashes($_POST["FCKeditor1"]);

//Load next page --> calendar_events.php
if ($next_link == NULL) {
  header ("Location: calendar_events.php?CalendarType=$CalendarType&CalendarName=".("$CalendarName")."&Term=".("$Term")); 
 } else {
  header("Location: $next_link.php?"); 
 } 


// Special Calendar case for blank times i.e. spring break with no definite time
//start time
if ($starthour == NULL && $startminute == NULL && $startsecond == NULL){
	$starthour      = 12;
	$startminute      = 12;
	$startsecond      = 12;
}

// End time
if ($endhour == NULL && $endminute == NULL && $endsecond == NULL){
	$endhour      = 12;
	$endminute      = 12;	
	$endsecond      = 12;
}




// setup the time struct
$complete_start_date = $startyear."-".$startmonth."-".$startday." ".$starthour.":".$startminute.":".$startsecond;
$complete_end_date = $endyear."-".$endmonth."-".$endday." ".$endhour.":".$endminute.":".$endsecond;


if ($action == "drop") {

	// Kill the Schedule
	$results = $db->query("DELETE FROM Calendar WHERE CalendarId = '$CalendarId'");

} else if ($action == "add") {

 	// Insert the information
 	$results = $db->query("INSERT INTO Calendar (EventStartDateTime, EventEndDateTime, Event, CalendarName, CalendarType, Term, UserRef, Page) VALUES ('$complete_start_date', '$complete_end_date', '$Event', '$CalendarName', '$CalendarType', '$Term', '$UserRef','$Page')"); 		
 
} else if ($action == "edit") {

	 // Modify the information
	$results = $db->query("UPDATE Calendar SET EventStartDateTime = '$complete_start_date', EventEndDateTime = '$complete_end_date' , Event = '$Event', UserRef = '$UserRef', Page = '$Page' WHERE CalendarId = '$CalendarId'");	
} else if ($action == "term") {

	 // Modify the information
	$results = $db->query("UPDATE Calendar SET Term = '$Term' WHERE CalendarType = '$CalendarType'");	
}
			 	
  $db->vardump($results);

?>