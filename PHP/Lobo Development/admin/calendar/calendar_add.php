<?PHP

include_once "../all_scripts/auth.php";	include_once "../ez_sql.php";
	

include_once "../all_scripts/admin_functions.php";
// check SESSION vars for idUser --> Paste all this code to the beginning of each application form!
if (!isset($_SESSION["id"]))
{
 header("Location: index.php");
 exit;
}

 // YES idUser --> set user_id & get all data from CONTACT
$idUser  = $_SESSION["id"];
$username = $_SESSION["username"];
$email    = $_SESSION["email"]; 
$CalendarId 	= $_GET["CalendarId"];

$CalendarName = $_GET['CalendarName'];
if ($CalendarName == NULL) {
	$CalendarName = $_POST['CalendarName'];
};

$CalendarType = $_GET['CalendarType'];
if ($CalendarType == NULL) {
	$CalendarType = $_POST['CalendarType'];
};

$Term = $_GET['Term'];
if ($Term == NULL) {
	$Term = $_POST['Term'];
};

							  				
?>
<HTML xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns="http://www.w3.org/TR/REC-html40">
<HEAD>
		<title>Content Manager</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="robots" content="noindex, nofollow">
		<link href="../sample.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="../fckeditor/fckeditor.js"></script>

		
<script type="text/javascript">
<!--
	var oFCKeditor = new FCKeditor( 'FCKeditor1' ) ;

window.onload = function()
{

	// Automatically calculates the editor base path based on the _samples directory.
	// This is usefull only for these samples. A real application should use something like this:
	// oFCKeditor.BasePath = '/fckeditor/' ;	// '/fckeditor/' is the default value.
    var sBasePath = document.location.pathname.substring(0,document.location.pathname.lastIndexOf('_samples')) ;

	
	oFCKeditor.BasePath	= '../fckeditor/' ;
	oFCKeditor.ToolbarSet = 'Default' ;
	oFCKeditor.Height	= 500 ;
	oFCKeditor.ReplaceTextarea() ; 
}


function MM_jumpMenu(targ,selObj,restore){ //v3.0
  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}
//-->
</script>
	<link href="papa_CSS.css" rel="stylesheet" type="text/css" /></head>
	<body vlink="blue">
<p><img src="../images/logo.gif"></p>
<table width="100%" border="0" cellpadding="4" cellspacing="0" bgcolor="#EFEFEF">
  <tr> 
    <td width="87%"><font color="#FFFFFF" size="2" face="Arial"><a href="../adminpanel.php"><strong>Administration</strong></a><strong><font color="#000000"> &gt; <a href="calendar.php">Calendars</a> &gt; <a href="calendar_events.php?CalendarType=<? echo $CalendarType; ?>&CalendarName=<? echo $CalendarName; ?>">Calendar
    Events</a> </font></strong></font><strong><font size="2" face="Arial">&gt; Update
    Calendar </font></strong></td>
    <td width="13%" align="right" valign="middle"><a href="./index.php"><img src="../images/exit.png" width="16" height="16" border="0"></a> 
      <font size="2" face="Arial"><a href="../all_scripts/logout.php"><strong>Sign Out </strong></a></font></td>
  </tr>
</table>

<form action="calendar_insert.php" method="post">
  <p><font size="2" face="Arial, Helvetica, sans-serif">Course Start Date <font color="#FFFFFF">|||||||||||||</font>Month 
      <input name="startmonth" type="text" class="formWords" id="month3" value="" size="2" maxlength="2">
    Day 
    <input name="startday" type="text" class="formWords" id="day2" value="" size="2" maxlength="2">
    Year 
    <input name="startyear" type="text" class="formWords" id="year2" value="" size="4" maxlength="4">
    <font color="#FFFFFF">||||||||||||||||||</font>Hour 
    <input name="starthour" type="text" class="formWords" id="hour2" value=""  size="2" maxlength="2">
    Minute 
    <input name="startminute" type="text" class="formWords" id="minute2" value=""  size="2" maxlength="2">
    Second 
    <input name="startsecond" type="text" class="formWords" id="second2" value="" size="2" maxlength="2">
    </font></p>
  <p><font size="2" face="Arial, Helvetica, sans-serif">Course End Date <font color="#FFFFFF">|||||||||||||</font> 
    Month 
    <input name="endmonth" type="text" class="formWords" id="month" value="" size="2" maxlength="2">
    Day 
    <input name="endday" type="text" class="formWords" id="day" value="" size="2" maxlength="2">
    Year 
    <input name="endyear" type="text" class="formWords" id="year" value="" size="4" maxlength="4">
    <font color="#FFFFFF">||||||||||||||||||</font>Hour 
    <input name="endhour" type="text" class="formWords" id="hour" value="" size="2" maxlength="2">
    Minute 
    <input name="endminute" type="text" class="formWords" id="minute" value="" size="2" maxlength="2">
    Second 
    <input name="endsecond" type="text" class="formWords" id="second" value="" size="2" maxlength="2">
    </font></p>
  <p><font size="2" face="Arial, Helvetica, sans-serif">Event 
      <input name="Event" type="text" class="formWords" id="Event" value="<? echo $page_content->Event ; ?>" size="100">
  </font></p>
  <p><font size="2" face="Arial, Helvetica, sans-serif">Description</font></p>
  <p>
    <textarea name="FCKeditor1" class="formWords" style="WIDTH: 100%; HEIGHT: 500px" onChange="UpdateExportedFCK(this)"><? echo stripslashes($page_content->Page); ?></textarea>
  </p>
  <p>
  
   <input name="CalendarId" type="hidden" id="CalendarId" value="<?  echo $CalendarId ;  ?>">
   <input name="CalendarName" type="hidden" id="CalendarName" value="<?  echo $CalendarName ;  ?>">
   <input name="CalendarType" type="hidden" id="CalendarType" value="<?  echo $CalendarType ;  ?>">
   <input name="Term" type="hidden" id="Term" value="<?  echo $Term ;  ?>">   
   <input name="action" type="hidden" id="action" value="add">
   <input type="submit" class="formButton" value="Submit">
  </p>
  </form>
  <p>&nbsp;</p>
<p><b><font size="2" face="Arial">&nbsp; Custom interface provided by TaGG Studios<br>
  &nbsp; <a href="mailto:info@taggstudios.com">info@taggstudios.com</a>&nbsp;&nbsp; 
  505.246.8244</font></b> <br>
  <a href="http://www.taggstudios.com" target="_blank"><img src="../images/taggstudios_logo.jpg" alt="TaGG Studios" width="204" height="141" border="0"></a></p>
	</body>
</html>