<?PHP

include_once "../all_scripts/auth.php";// adminpanel.php

include_once "../ez_sql.php";


include_once "../all_scripts/admin_functions.php";
// check SESSION vars for idUser --> Paste all this code to the beginning of each application form!
if (!isset($_SESSION["id"]))
{
 header("Location: index.php");
 exit;
}

 // YES idUser --> set user_id & get all data from CONTACT
$idUser  = $_SESSION["id"];
$username = $_SESSION["username"];
$email    = $_SESSION["email"];


$results = $db->get_results("SELECT * FROM Calendar ORDER BY CalendarName");

$result_administrator  = $db->get_row("SELECT * FROM AdminUser WHERE id = " .$_SESSION["id"]);
// $db->vardump($result_administrator);			
?>



<HTML xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns="http://www.w3.org/TR/REC-html40">
<HEAD>
<meta HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=windows-1252">
<meta http-equiv="Content-Language" content="en-us">


<TITLE>Content Manager</TITLE>



<script type="text/javascript">
<!--
function delete_confirmation(delete_address) {
var answer = confirm("Are you sure you want to delete this entry?")
if (answer){
  window.location = delete_address;
}

}
//-->
</script>
		<SCRIPT language="JavaScript">
		<!--
			function reset_page(address)
			{
				 var delete_it= confirm("This will take your page back to the original state.  Do you really want to reset the page?");
				 if (delete_it== true) {
					window.location=address;
					}
			}
			//-->
</SCRIPT>

<link href="papa_CSS.css" rel="stylesheet" type="text/css" /></head>
<BODY style="text-align: left" vlink="blue">





<h2><img src="../images/logo.gif"></h2>
<table width="100%" border="0" cellpadding="4" cellspacing="0" bgcolor="#EFEFEF">
  <tr>
    <td width="87%"><font color="#FFFFFF" size="2" face="Arial"><a href="../adminpanel.php"><strong>Administration</strong></a><strong><font color="#000000"> 
      &gt; Calendars</font></strong></font></td>
    <td width="13%" align="right" valign="middle"><a href="./index.php"><img src="../images/exit.png" width="16" height="16" border="0"></a> 
      <font size="2" face="Arial"><a href="./all_scripts/logout.php"><strong>Sign Out</strong></a></font></td>
  </tr>
</table>
<h2 align="left"><font color="#000000" face="Arial, Helvetica, sans-serif"><strong><a name="page_select"></a><img src="../images/appointment.png" width="32" height="32">Calendars</strong></font></h2>
<p align="left"><a href="storyeditor.php?content_id=1&page=CurrentEvents"><font size="2" face="Arial, Helvetica, sans-serif"><img src="../images/b_edit.png" alt="edit" width="16" height="16" border="0"></font></a><font size="2" face="Arial, Helvetica, sans-serif"> 
  = Edit </font></p>
<table width="763" border="0" cellpadding="0" cellspacing="1" bgcolor="#BABABA">
  <tr bgcolor="#BABABA"> 
    <td align="center" valign="bottom"> <div align="center"><font color="#FFFFFF" size="1" face="Arial, Helvetica, sans-serif">Options</font><font color="#FFFFFF"></font></div></td>
    <td width="697"> <div align="center"></div>
      <div align="center"><font color="#FFFFFF"></font></div>
      <div align="center"><font color="#FFFFFF"></font></div>
      <div align="center"><font color="#FFFFFF"><strong>Calendars</strong></font></div></td>
  </tr>
  
  <?php 
  
  // get list of webpages for dynamic editing
  if ($results != NULL) {
  	foreach ($results as $results_output) {
	
		// List only names as they change from the asc ordered type
		if ($CalendarName != $results_output->CalendarName) {
		  	$CalendarName = $results_output->CalendarName;
  			echo '
  <tr bgcolor="#FFFFFF">
    <td align="center" ><a href="calendar_events.php?CalendarType='.$results_output->CalendarType.'&CalendarName='.$results_output->CalendarName.'&Term='.$results_output->Term.'"><img src="../images/b_edit.png" alt="edit" width="16" height="16" border="0"></a><font size="2" face="Arial, Helvetica, sans-serif"><font color="#FFFFFF">i</font></font></td>
    <td align="left"><font size="2" face="Arial, Helvetica, sans-serif"><strong>'.$results_output->CalendarName.'</strong> ('.$results_output->Term.') ';

	// only allow the term to be modified if the user is an administrator
	if (strtolower($result_administrator->administrator) == 'yes') {	
		echo '<a href="calendar_term.php?CalendarType='.$results_output->CalendarType.'">modify term</a>'.'</font>';
	}
	
	echo '</td>
  </tr>';
  		} //if
  	} // foreach
   } // if
  
  ?>
</table>

 
<p>&nbsp;</p>
<p><b><font size="2" face="Arial">&nbsp; Custom interface provided by TaGG Studios<br>
  &nbsp; <a href="mailto:info@taggstudios.com">info@taggstudios.com</a>&nbsp;&nbsp; 
  505.246.8244</font></b> <br>
  <a href="http://www.taggstudios.com" target="_blank"><img src="../images/taggstudios_logo.jpg" alt="TaGG Studios" width="204" height="141" border="0"></a></p>
</BODY>
</HTML>