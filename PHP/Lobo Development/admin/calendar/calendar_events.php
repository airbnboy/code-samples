<?PHP

include_once "../all_scripts/auth.php";// adminpanel.php

include_once "../ez_sql.php";


include_once "../all_scripts/admin_functions.php";
// check SESSION vars for idUser --> Paste all this code to the beginning of each application form!
if (!isset($_SESSION["id"]))
{
 header("Location: index.php");
 exit;
}

 // YES idUser --> set user_id & get all data from CONTACT
$idUser  = $_SESSION["id"];
$username = $_SESSION["username"];
$email    = $_SESSION["email"];
$CalendarType = $_GET['CalendarType'];
$CalendarName = $_GET['CalendarName'];
$Term = $_GET['Term'];
if ($Term == NULL) {
	$Term = $_POST['Term'];
};


$results = $db->get_results("SELECT *, UNIX_TIMESTAMP(EventStartDateTime) AS FORMATED_TIME_BEGIN, UNIX_TIMESTAMP(EventEndDateTime) AS FORMATED_TIME_END FROM Calendar WHERE CalendarType = '$CalendarType' ORDER BY EventStartDateTime");
//$db->vardump($results);

$result_administrator  = $db->get_row("SELECT * FROM AdminUser WHERE id = " .$_SESSION["id"]);

?>



<HTML xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns="http://www.w3.org/TR/REC-html40">
<HEAD>
<meta HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=windows-1252">
<meta http-equiv="Content-Language" content="en-us">


<TITLE>Content Manager</TITLE>



<script type="text/javascript">
<!--
function delete_confirmation(delete_address) {
var answer = confirm("Are you sure you want to delete this entry?")
if (answer){
  window.location = delete_address;
}

}
//-->
</script>
		<SCRIPT language="JavaScript">
		<!--
			function reset_page(address)
			{
				 var delete_it= confirm("This will take your page back to the original state.  Do you really want to reset the page?");
				 if (delete_it== true) {
					window.location=address;
					}
			}
			//-->
</SCRIPT>

<link href="papa_CSS.css" rel="stylesheet" type="text/css" /></head>
<BODY style="text-align: left" vlink="blue">





<h2><img src="../images/logo.gif"></h2>
<table width="100%" border="0" cellpadding="4" cellspacing="0" bgcolor="#EFEFEF">
  <tr>
    <td width="87%"><font color="#FFFFFF" size="2" face="Arial"><a href="../adminpanel.php"><strong>Administration</strong></a><strong><font color="#000000"> 
      &gt; <a href="calendar.php">Calendars</a> &gt; Calendar Events </font></strong></font></td>
    <td width="13%" align="right" valign="middle"><a href="./index.php"><img src="../images/exit.png" width="16" height="16" border="0"></a> 
      <font size="2" face="Arial"><a href="../all_scripts/logout.php"><strong>Sign Out</strong></a></font></td>
  </tr>
</table>
<h2 align="left"><font color="#000000" face="Arial, Helvetica, sans-serif"><strong><a name="page_select"></a><img src="../images/appointment.png" width="32" height="32">Calendar
      Events </strong></font>  <? if ($results != NULL) {echo ' - ' .$results[0]->CalendarName.' ('.$results[0]->Term.')' ;} ?></h2>
<p align="left"><a href="storyeditor.php?content_id=1&page=CurrentEvents"><font size="2" face="Arial, Helvetica, sans-serif"><img src="../images/b_edit.png" alt="edit" width="16" height="16" border="0"></font></a><font size="2" face="Arial, Helvetica, sans-serif"> 
  = Edit </font> <a href="storyeditor.php?content_id=1&page=CurrentEvents"><font size="2" face="Arial, Helvetica, sans-serif"><img src="../images/b_drop.png" alt="edit" width="16" height="16" border="0"></font></a><font size="2" face="Arial, Helvetica, sans-serif"> =
  Delete </font></p>
<form name="form2" method="post" action="calendar_add.php">
  <input name="Submit2" type="submit" class="formButton" value="Add New Event">
  <input name="CalendarType" type="hidden" id="CalendarType" value="<? echo $CalendarType ?>">
  <input name="CalendarName" type="hidden" id="CalendarName" value="<? echo $CalendarName ?>">
  <input name="Term" type="hidden" id="Term" value="<? echo $Term ?>">  
</form><table width="763" border="0" cellpadding="0" cellspacing="1" bgcolor="#BABABA">
  <tr bgcolor="#BABABA"> 
    <td width="60" align="center" valign="bottom"> <div align="center"><font color="#FFFFFF" size="1" face="Arial, Helvetica, sans-serif">Options</font><font color="#FFFFFF"></font></div></td>
    <td width="458"> <div align="center"></div>
      <div align="center"><font color="#FFFFFF"></font></div>
      <div align="center"><font color="#FFFFFF"></font></div>
    <div align="center"><font color="#FFFFFF"><strong>Events</strong></font></div></td>
    <td width="120" align="center" valign="bottom"> <div align="center"><font color="#FFFFFF" size="1" face="Arial, Helvetica, sans-serif">Start</font><font color="#FFFFFF"></font></div></td>
    <td width="120" align="center" valign="bottom"> <div align="center"><font color="#FFFFFF" size="1" face="Arial, Helvetica, sans-serif">End</font><font color="#FFFFFF"></font></div></td>
  </tr>
  
  <?php
  
  
  // get list of webpages for dynamic editing  
  if ($results != NULL) {
  	foreach ($results as $results_output) {
  echo '
  <tr bgcolor="#FFFFFF">
    <td align="center" >';
	
	
		// only allow the term to be modified if the user is an administrator or the owner of the date
	if (strtolower($result_administrator->administrator) == 'yes' || $results_output->UserRef == $_SESSION["id"]) {
	echo '<a href="calendar_edit.php?CalendarId='.$results_output->CalendarId.'&action=edit&CalendarType='.$CalendarType.'&CalendarName='.$CalendarName.'&Term='.$results_output->Term.'"><img src="../images/b_edit.png" alt="edit" width="16" height="16" border="0"></a>'; 
	
		// Don't allow deletion of a single item
		if ($results[1] != NULL) { 
		echo '<font color="white">|||||</font><a href="calendar_insert.php?CalendarId='.$results_output->CalendarId.'&action=drop&CalendarType='.$CalendarType.'&CalendarName='.$CalendarName.'&Term='.$results_output->Term.'"><img src="../images/b_drop.png" alt="drop" width="16" height="16" border="0"></a><font size="2" face="Arial, Helvetica, sans-serif"><font color="#FFFFFF">i</font></font>';
		}
	
	} // if administrator/user
	
	 echo '</td>
    <td align="left"><font size="2" face="Arial, Helvetica, sans-serif"><strong>'.$results_output->Event.'</strong></font></td>';
	
					/// in the case of 12 :12 :12 assume that there is not any time inputted dont show time otherwise show full date/time
					if ((date("h",$results_output->FORMATED_TIME_BEGIN).date("i",$results_output->FORMATED_TIME_BEGIN).date("s",$results_output->FORMATED_TIME_BEGIN)) == ('121212')) {
					
	 echo   '<td align="center" valign="bottom"> <div align="center"><font color="#000000" size="1" face="Arial, Helvetica, sans-serif">'.date("Y",$results_output->FORMATED_TIME_BEGIN).'-'.date("m",$results_output->FORMATED_TIME_BEGIN).'-'.date("d",$results_output->FORMATED_TIME_BEGIN).'</font><font color="#000000"></font></div></td>
		    <td align="center" valign="bottom"> <div align="center"><font color="#000000" size="1" face="Arial, Helvetica, sans-serif">'.date("Y",$results_output->FORMATED_TIME_END).'-'.date("m",$results_output->FORMATED_TIME_END).'-'.date("d",$results_output->FORMATED_TIME_END).'</font><font color="#000000"></font></div></td>'; 
			
			} else	{		
	 echo   '<td align="center" valign="bottom"> <div align="center"><font color="#000000" size="1" face="Arial, Helvetica, sans-serif">'.$results_output->EventStartDateTime.'</font><font color="#000000"></font></div></td>
		    <td align="center" valign="bottom"> <div align="center"><font color="#000000" size="1" face="Arial, Helvetica, sans-serif">'.$results_output->EventEndDateTime.'</font><font color="#000000"></font></div></td>';
			}
			
  echo '</tr>';
  	}
   }
  
  ?>
</table>
<br>
 
<form name="form2" method="post" action="calendar_add.php">
  <input name="Submit2" type="submit" class="formButton" value="Add New Event">
  <input name="CalendarType" type="hidden" id="CalendarType" value="<? echo $CalendarType ?>">
  <input name="CalendarName" type="hidden" id="CalendarName" value="<? echo $CalendarName ?>">
  <input name="Term" type="hidden" id="Term" value="<? echo $Term ?>">  
</form>
<p>&nbsp;</p>
<p><b><font size="2" face="Arial">&nbsp; Custom interface provided by TaGG Studios<br>
  &nbsp; <a href="mailto:info@taggstudios.com">info@taggstudios.com</a>&nbsp;&nbsp; 
  505.246.8244</font></b> <br>
  <a href="http://www.taggstudios.com" target="_blank"><img src="../images/taggstudios_logo.jpg" alt="TaGG Studios" width="204" height="141" border="0"></a></p>
</BODY>
</HTML>