<?

include_once "../ez_sql.php";


// helper
//REDW functions
function REDWBlogCategoryPageReference($blog_category){

		// page for respective blog categories
		if ($blog_category=='Firm News') {
			return 'firm-news_full_story.php?Page=firm-news';
		}
		
		if ($blog_category=='Industry News') {
			return 'journal_comments.php?Page=industry-news';
		}

		if ($blog_category=='Media Room') {
			return 'news_full_story.php?Page=media-room';
		}


		if ($blog_category=='Publications') {
			return 'news_full_story.php?Page=publications';
		}		

		if ($blog_category=='Printable Media') {
			return 'news_full_story.php?Page=printable-media';
		}	
		
		if ($blog_category=='Accolades') {
			return 'news_full_story.php?Page=accolades';
		}			
}


// generate
function generate_rss($blog_category){
global $db;
$current_date_time = date("D, d M Y H:i:s T");
 
 // output blog header
 $xml_output = '<?xml version="1.0" ?> 
<rss version="2.0">
	<channel>
		<title>REDW - '.$blog_category.'</title>
		<link>http://www.redw.com</link>
		<description>Latest REDW Blog Posts</description>
		<language>en-us</language>
		<copyright>Copyright '.date("Y").' REDW</copyright>
		<docs>http://www.redw.com/rss/blog.xml</docs>
		<lastBuildDate>'.$current_date_time.' PST</lastBuildDate>'."\n";


// Output individual posts
$results = $db->get_results("SELECT *, UNIX_TIMESTAMP(BlogTime) AS FORMATED_TIME FROM Blog WHERE Category='$blog_category' ORDER BY BlogTime DESC");
if ($results  != NULL) {
foreach ($results as $result) {

$xml_output .= '		<item>
			<title>'.str_replace('&','&amp;',stripslashes($result->BlogTitle)).'</title>
			<description>'.str_replace('&','&amp;',stripslashes($result->BlogSubTitle)).'</description>
			<link>http://www.redw.com/j'.REDWBlogCategoryPageReference($blog_category).'&amp;Blog_Ref='.$result->blog_id.'</link>
			<author>'.str_replace('&','&amp;',stripslashes($result->BlogAuthor)).'</author>
			<pubDate>'.date("D, d M Y H:i:s T",$result->FORMATED_TIME).'</pubDate>
		</item>'."\n";

}
} // if


// close tags
$xml_output .= '	</channel>
</rss>';


//echo $xml_output; // debug

// write the file

$rssFileName = "../../rss/blog_".str_replace(' ','',$blog_category).".xml";

// delete file first
if (file_exists($rssFileName)) {
	unlink($rssFileName);
}

// Generate new one
$fh = fopen($rssFileName, 'w') or die("can't open file");
fwrite($fh, $xml_output);
fclose($fh);

} // function generate






?>
