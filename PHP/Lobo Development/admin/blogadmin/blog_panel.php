<?PHP

include_once "../all_scripts/auth.php";// adminpanel.php

include_once "../ez_sql.php";

include_once "../all_scripts/admin_functions.php";


// check SESSION vars for idUser --> Paste all this code to the beginning of each application form!
if (!isset($_SESSION["id"]))
{
 header("Location: index.php");
 exit;
}

 // YES idUser --> set user_id & get all data from CONTACT
$idUser  = $_SESSION["id"];
$username = $_SESSION["username"];
$email    = $_SESSION["useremail"];
$admin = $_GET["admin"];

if (strtolower($admin) == 'yes') {
	$blog = $db->get_results("SELECT * FROM Blog ORDER BY Category, BlogAuthor, BlogTime DESC"); //ORDER BY blog_id DESC
} else {
	$blog = $db->get_results("SELECT * FROM Blog WHERE UserRef = $idUser ORDER BY Category, BlogTime DESC"); //ORDER BY blog_id DESC
	$admin = 'panel';
};

?>



<HTML xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns="http://www.w3.org/TR/REC-html40">
<HEAD>
<meta HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=windows-1252">
<meta http-equiv="Content-Language" content="en-us">


<title>Content Manager</title>


<script type="text/javascript">
<!--
function delete_confirmation(delete_address) {
var answer = confirm("Are you sure you want to delete this entry?")
if (answer){
  window.location = delete_address;
}

}
//-->
</script>

<link href="../papa_CSS.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
.style1 {color: #E3E3E3}
-->
</style>
</HEAD>
<BODY style="text-align: left" >





<p><img src="../images/logo.gif"></p>
<h2 align="left"><font size="2" face="Arial"><a href="../adminpanel.php">Administration</a> 
  <font color=#000000>&gt; </font>Blog Administration</font></h2>
<p align="left">&nbsp;</p>
      
<h2><font color="#0033FF"><a href="insert.php"><img src="./images/b_insrow.png" width="16" height="16" border="0"></a> 
  <a href="insert.php?admin=<? echo $admin; ?>">Insert new blog</a></font></h2>

<table width="601" border="0" cellspacing="1" cellpadding="0">
  <tr> 
    <th colspan="3" bgcolor="#8b8b8b"><div align="center"><font color="#FFFFFF"></font></div></th>
    <th width="162" bgcolor="#8b8b8b"><div align="center"><font color="#FFFFFF"><strong>Date/Time</strong></font></div></th>
    <th width="299" bgcolor="#8b8b8b"><div align="center"><font color="#FFFFFF"><strong>Title</strong></font></div></th>
    <th width="238" bgcolor="#8b8b8b"><div align="center"><font color="#FFFFFF"><strong>Author</strong></font></div></th>
  </tr>
  <?
  	if ($blog != NULL) {
	
		$i = 0;
  		foreach ( $blog as $blog_output ) {
		
				// Place category heading
		if ($blog_output->Category != $previous_category) {
		
			// mark the change
			$previous_category = $blog_output->Category;
		  	echo ' <tr><td></td><td></td></tr>
  <tr bgcolor="#CCFFFF">
    <td align="center" ></td><td></td><td></td>
    <td align="left" bgcolor=""><font size="2" face="Arial, Helvetica, sans-serif"><strong>'.$blog_output->Category.'</strong></font></td><td></td><td></td>
  </tr>';		
		}
		
  			if ($i & 1) {
  				echo "<tr bgcolor=\"#CCCCCC\"> \n"; 
			} else {
				echo "<tr> \n"; 
			}
			echo "<td width=\"22\" align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\"><a href=\"moderate_panel.php?blog_id=$blog_output->blog_id&blog_title=$blog_output->blog_title&admin=$admin\"><img src=\"./images/aim.png\" alt=\"Moderate Responses\" width=\"16\" height=\"16\" border=\"0\"></a> </td>\n";
    		echo "<td width=\"22\" align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\"><a href=\"edit.php?blog_id=$blog_output->blog_id&admin=$admin\"><img src=\"./images/b_edit.png\" alt=\"Edit Blog\" width=\"16\" height=\"16\" border=\"0\"></a> </td>\n";
    		echo "<td width=\"18\" align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\"><a href=\"javascript:delete_confirmation('delete.php?blog_id=$blog_output->blog_id&admin=$admin')\"><img src=\"./images/b_drop.png\" width=\"16\" height=\"16\" border=\"0\"></a></td>\n";
   	 		echo "<td><div align=\"center\"><font color=\"#000000\">$blog_output->BlogTime</font></div></td>\n";
    		echo "<td><div align=\"center\"><font color=\"#000000\"> $blog_output->BlogTitle</font></div></td>\n";
    		echo "<td><div align=\"center\"><font color=\"#000000\">$blog_output->BlogAuthor</font></div></td>\n";
    		echo "</tr>\n";
			$i++;
		}
	} else {
	   		echo "<td colspan=\"6\"><div align=\"center\"><font color=\"#000000\" face=\"arial\" size=\"2\"> There are currently no blog entries.</font></div></td>\n";
	}
  ?>
</table>

<p><font color="#FF0000"></font></p>
<p>
<p>
<p>
<p>
<p class="style1">
<p>
<p>
<p><b><font face="Arial" size="2"><br>
  &nbsp; Custom interface provided by TaGG Studios<br>
  &nbsp; <a href="mailto:info@taggstudios.com">info@taggstudios.com</a>&nbsp;&nbsp; 
  505.246.8244</font></b> 
</BODY>
</HTML>