<?PHP

include_once "../all_scripts/auth.php";// edit.php

include_once "../ez_sql.php";

include_once "../all_scripts/admin_functions.php";

// check SESSION vars for idUser --> Paste all this code to the beginning of each application form!
if (!isset($_SESSION["id"]))
{
header("Location: index.php");
 exit;
}

$idUser  		= $_SESSION["id"];
$Response_id  		= $_GET["Response_id"];




// Select row
$Responses = $db->get_row("SELECT * FROM Responses WHERE Response_id = $Response_id");
$page = $Responses->Response;

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<title>Content Manager</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="robots" content="noindex, nofollow">
		<link href="../sample.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="fckeditor/fckeditor.js"></script>

		<script type="text/javascript">

window.onload = function()
{
	// Automatically calculates the editor base path based on the _samples directory.
	// This is usefull only for these samples. A real application should use something like this:
	// oFCKeditor.BasePath = '/fckeditor/' ;	// '/fckeditor/' is the default value.
	var sBasePath = document.location.pathname.substring(0,document.location.pathname.lastIndexOf('_samples')) ;

	var oFCKeditor = new FCKeditor( 'FCKeditor1' ) ;
	oFCKeditor.BasePath	= 'fckeditor/' ;
	oFCKeditor.Height	= 500 ;
	oFCKeditor.ReplaceTextarea() ;
}

		</script>
	<link href="../papa_CSS.css" rel="stylesheet" type="text/css" /></HEAD>
	<body vlink="blue">
<p><img src="../images/logo.gif"></p>
<table width="601" border="0" cellpadding="4" cellspacing="0" bgcolor="#8b8b8b">
  <tr> 
    <td width="78%"><font size="2" face="Arial"><a href="../adminpanel.php"><strong>Administration</strong></a> 
      <strong><font color=#000000>&gt; </font><a href="pageupdater.php"> </a></strong> <a href="blog_panel.php"><strong>Blog 
      Administration</strong></a> <strong><font color=#000000>&gt; </font><a href="moderate_panel.php">Response 
      Panel</a> <font color=#000000>&gt; </font>Edit Response</strong></font></td>
    <td width="22%" align="right" valign="middle"><a href="../all_scripts/logout.php"><img src="../images/exit.png" width="16" height="16" border="0"></a> 
      <font size="2" face="Arial"><a href="../all_scripts/logout.php"><strong>Sign 
      Out </strong></a></font></td>
  </tr>
</table>
		<form action="moderate_edit_insert.php" method="post">
<p>Posted By: 

    <input name="ResponseAuthor" type="text" size="30" maxlength="60" value="<? echo $Responses->ResponseAuthor; ?>">
</p>
<hr>

			
  <div> 
    <textarea name="FCKeditor1" style="WIDTH: 601; HEIGHT: 500px"><? echo $page; ?></textarea>
  </div>
			<br>
			<input type="hidden" name="Response_id" value="<? echo $Response_id; ?>">
			<input type="submit" value="Submit">
		</form>
	</body>

</html>
