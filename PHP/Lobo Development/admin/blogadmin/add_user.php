<?PHP

include_once "../all_scripts/auth.php";// insert.php

include_once "../ez_sql.php";

include_once "../all_scripts/admin_functions.php";

// check SESSION vars for id --> Paste all this code to the beginning of each application form!
if (!isset($_SESSION["id"]))
{
	header("Location: index.php");
	exit;
}

 // YES idUser --> set user_id & get all data from CONTACT
//$id  = $_SESSION["id"];
//$new_username = $_SESSION["new_username"];
//$email    = $_SESSION["email"];


?>


<HTML xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns="http://www.w3.org/TR/REC-html40">
<HEAD>
<meta HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=windows-1252">
<meta http-equiv="Content-Language" content="en-us">


<title>Content Manager</title>


<script>
function FrontPage_Form1_Validator(theForm)
{
  if (theForm.fullname.value == "")
  {
    alert("Please enter a value for the \"Full Name\" field.");
    theForm.fullname.focus();
    return (false);
  }

  if (theForm.new_username.value == "")
  {
    alert("Please enter a value for the \"Username\" field.");
    theForm.new_username.focus();
    return (false);
  }
  
  if (theForm.new_password.value == "")
  {
    alert("Please enter a value for the \"Password\" field.");
    theForm.new_password.focus();
    return (false);
  }
     
   if (theForm.new_administrator.value == "")
  {
    alert("Please enter a value for the \"Administrator\" field.");
    theForm.new_administrator.focus();
    return (false);
  }
   
  return (true);
}
//--></script>

<style type="text/css">
<!--
body,td,th {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #999999;
}
a:link {
	color: FF1010;
	text-decoration: none;
}
a:visited {
	text-decoration: none;
	color: FF1010;
}
a:hover {
	text-decoration: underline;
	color: FF1010;
}
a:active {
	text-decoration: none;
	color: FF1010;
}
.style1 {color: #000000}
-->
</style></HEAD>
<BODY style="text-align: left">
<p><img src="../images/logo.gif"></p>
<table width="601" border="0" cellpadding="3" cellspacing="0" bgcolor="#8b8b8b">
  <tr> 
    <td width="601"><font size="2" face="Arial"><a href="../admin/adminpanel.php"><strong>Administration</strong></a> 
      <strong><font color=#000000>&gt; </font><a href="usermanager.php">User Manager</a> <font color=#000000>&gt; </font><span class="style1">Insert User</span></strong></font></td>
    <td width="17%" align="right" valign="middle"><a href="../blogadmin/./index.php"><img src="../blogadmin/images/exit.png" width="16" height="16" border="0"></a> 
      <font size="2" face="Arial"><a href="index.php"><strong>Sign Out </strong></a></font></td>
  </tr>
</table>
<div align="center">
<form  method="post" action="add_user_insert.php" onsubmit="return FrontPage_Form1_Validator(this)"  language="JavaScript" name="FrontPage_Form1">
    <table width="601" height="227" border="0" align="left" cellpadding="5" cellspacing="1">
      <tr> 
        <td width="470" height="9" align="left" bgcolor="#8b8b8b"><font color="#FFFFFF">First 
          and Last Name </font> <div align="left"></div></td>
      </tr>
      <tr> 
        <td height="10" align="left" bgcolor="#8b8b8b"> <input name="fullname" type="text"  id="fullname" size="60" maxlength="60"> 
        </td>
      </tr>
      <tr> 
        <td height="20" align="left" bgcolor="#8b8b8b"><font color="#FFFFFF">Username</font></td>
      </tr>
      <tr align="center"> 
        <td height="12" align="left" bgcolor="#8b8b8b"> <p> 
            <input name="new_username" type="text"  id="new_username" size="60" maxlength="60">
          </p></td>
      </tr>
      <tr> 
        <td height="13" bgcolor="#8b8b8b"><font color="#FFFFFF">Password</font></td>
      </tr>
      <tr align="center"> 
        <td height="34" align="left" bgcolor="#8b8b8b"> <p> 
            <input name="new_password" type="new_password"  id="new_password" size="60" maxlength="60">
          </p></td>
      </tr>

	  <tr align="center"> 
        <td height="34" align="left" bgcolor="#8b8b8b"><input name="new_administrator" type="checkbox" id="new_administrator" value="yes"  $user_admin >
          <font color="#FFFFFF">User Administration</font> 
          <input name="blog_admin" type="checkbox" id="blog_admin" value="yes" $blog_admin>
          <font color="#FFFFFF">Blog Administration</font> 
          <input name="career_admin" type="checkbox" id="career_admin" value="yes" $career_admin>
          <font color="#FFFFFF">Careers Administration</font></td>
      </tr>
      <tr> 
        <td bgcolor="#8b8b8b"> <div align="center"> 
            <input type="submit" name="Submit" value="Add New User"  class="formButton">
          </div></td>
      </tr>
    </table>
  </form>
</div>
<p align="left"><b><font face="Arial" size="2"><br>
  &nbsp;</font></b>
</BODY>
</HTML>

