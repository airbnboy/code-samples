<?PHP

include_once "../all_scripts/auth.php";// adminpanel.php

include_once "../ez_sql.php";

include_once "../all_scripts/admin_functions.php";


// check SESSION vars for idUser --> Paste all this code to the beginning of each application form!
if (!isset($_SESSION["id"]))
{
 header("Location: index.php");
 exit;
}

 // YES idUser --> set user_id & get all data from CONTACT
$idUser  = $_SESSION["id"];
$username = $_SESSION["username"];
$email    = $_SESSION["useremail"];



$career = $db->get_results("SELECT * FROM Careers ORDER BY CareerDate DESC"); //ORDER BY CareerDate DESC


?>



<HTML xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns="http://www.w3.org/TR/REC-html40">
<HEAD>
<meta HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=windows-1252">
<meta http-equiv="Content-Language" content="en-us">


<title>Content Manager</title>


<script type="text/javascript">
<!--
function delete_confirmation(delete_address) {
var answer = confirm("Are you sure you want to delete this entry?")
if (answer){
  window.location = delete_address;
}

}
//-->
</script>

<link href="../papa_CSS.css" rel="stylesheet" type="text/css" /></HEAD>
<BODY style="text-align: left" >





<p><img src="../images/logo.gif"></p>
<h2 align="left"><font size="2" face="Arial"><a href="../adminpanel.php">Administration</a> 
  <font color=#000000>&gt; </font>Career Postings</font></h2>
<p align="left">&nbsp;</p>
      
<h2><font color="#0033FF"><a href="career_insert.php"><img src="./images/b_insrow.png" width="16" height="16" border="0"></a> 
  <a href="career_insert.php">Insert new job posting</a></font></h2>

<table width="601" border="0" cellspacing="1" cellpadding="0">
  <tr> 
    <th colspan="2" bgcolor="#8b8b8b"><div align="center"><font color="#FFFFFF"></font></div></th>
    <th width="141" bgcolor="#8b8b8b"><div align="center"><font color="#FFFFFF"><strong>Date/Time</strong></font></div></th>
    <th width="249" bgcolor="#8b8b8b"><div align="center"><font color="#FFFFFF"><strong>Posting 
        Title</strong></font></div></th>
    <th width="202" bgcolor="#8b8b8b"><div align="center"><font color="#FFFFFF"><strong>Author</strong></font></div></th>
  </tr>
  <?

  
  	if ($career != NULL) {
	
		$i = 0;
  		foreach ( $career as $career_output ) {
  			if ($i & 1) {
  				echo "<tr bgcolor=\"#CCCCCC\"> \n"; 
			} else {
				echo "<tr> \n"; 
			}
    		echo "<td width=\"22\" align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\"><a href=\"career_edit.php?career_id=$career_output->id\"><img src=\"./images/b_edit.png\" alt=\"Edit Posting\" width=\"16\" height=\"16\" border=\"0\"></a> </td>\n";
    		echo "<td width=\"18\" align=\"center\" valign=\"top\" bgcolor=\"#FFFFFF\"><a href=\"javascript:delete_confirmation('career_delete.php?career_id=$career_output->id')\"><img src=\"./images/b_drop.png\" width=\"16\" height=\"16\" border=\"0\"></a></td>\n";
   	 		echo "<td><div align=\"center\"><font color=\"#000000\">$career_output->CareerDate</font></div></td>\n";
    		echo "<td><div align=\"center\"><font color=\"#000000\"> $career_output->CareerTitle</font></div></td>\n";
    		echo "<td><div align=\"center\"><font color=\"#000000\">$career_output->CareerAuthor</font></div></td>\n";
    		echo "</tr>\n";
			$i++;
		}
	} else {
	   		echo "<td colspan=\"6\"><div align=\"center\"><font color=\"#000000\" face=\"arial\" size=\"2\"> There are currently no job postings.</font></div></td>\n";
	}
  ?>
</table>

<p><font color="#FF0000"></font></p>
<p>
<p>
<p>
<p>
<p>
<p>
<p>
<p><b><font face="Arial" size="2"><br>
  &nbsp; Custom interface provided by <a href="http://www.taggstudios.com">TaGG Studios</a><br>
  &nbsp; <a href="mailto:info@taggstudios.com">info@taggstudios.com</a>&nbsp;&nbsp; 
  505.246.8244</font></b> 
</BODY>
</HTML>