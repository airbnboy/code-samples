<?PHP

include_once "../all_scripts/auth.php";// edit.php

include_once "../ez_sql.php";

include_once "../all_scripts/admin_functions.php";

// check SESSION vars for idUser --> Paste all this code to the beginning of each application form!
if (!isset($_SESSION["id"]))
{
header("Location: index.php");
 exit;
}

$idUser  		= $_SESSION["id"];
$blog_id  		= $_GET["blog_id"];
$admin  		= $_GET["admin"];


// Select row FROM login.htm WHERE username/password/email = POST data USING ../ez_sql.php
$blog = $db->get_row("SELECT * FROM Blog WHERE blog_id = $blog_id");
$page = $blog->BlogText;

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<title>Content Manager</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="robots" content="noindex, nofollow">
		<link href="../sample.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="../fckeditor/fckeditor.js"></script>

		<script type="text/javascript">
		
		var oFCKeditor = new FCKeditor( 'FCKeditor1' ) ;


window.onload = function()
{
	// Automatically calculates the editor base path based on the _samples directory.
	// This is usefull only for these samples. A real application should use something like this:
	// oFCKeditor.BasePath = '/fckeditor/' ;	// '/fckeditor/' is the default value.
    var sBasePath = document.location.pathname.substring(0,document.location.pathname.lastIndexOf('_samples')) ;

	
	oFCKeditor.BasePath	= '../fckeditor/' ;
	oFCKeditor.ToolbarSet = 'Default' ;
	oFCKeditor.Height	= 500 ;
	oFCKeditor.ReplaceTextarea() ; 
}

		</script>
	<link href="../papa_CSS.css" rel="stylesheet" type="text/css" /></HEAD>
	<body vlink="blue">
<p><img src="../images/logo.gif"></p>
<table width="601" border="0" cellpadding="4" cellspacing="0" bgcolor="#8b8b8b">
  <tr> 
    <td width="76%"><font size="2" face="Arial"><a href="../adminpanel.php"><strong>Administration</strong></a> 
      <strong><font color=#000000>&gt; </font><a href="pageupdater.php"> </a> <a href="blog_panel.php<? echo "?admin=$admin"; ?>">Blog 
      Panel</a><font color="#000000"> <font color=#000000>&gt; </font>Edit Blog</font></strong></font></td>
    <td width="24%" align="right" valign="middle"><a href="../all_scripts/logout.php"><img src="../images/exit.png" width="16" height="16" border="0"></a> 
      <font size="2" face="Arial"><a href="../all_scripts/logout.php"><strong>Sign 
      Out </strong></a></font></td>
  </tr>
</table>
<hr>
<form action="editor_insert.php" method="post">
  <p>Category 
    <label>
    <select name="Category" id="Category">
    <option value="">none</option>
    <?   
	 if ($blog->Category != NULL) {
	  	echo '<option value="'.stripslashes($blog->Category).'" selected>'.stripslashes($blog->Category).'</option>'; 
	  } 
	  $BlogCategories = $db->get_results("SELECT * FROM BlogCategories ORDER BY CategoryName");
	  
	  	if ($BlogCategories != NULL) {
	 		foreach ( $BlogCategories as $BlogCategories_output) {
	  			echo '<option value="'.stripslashes($BlogCategories_output->CategoryName).'">'.stripslashes($BlogCategories_output->CategoryName).'</option>'; 
	  		}
		} // if 
	  ?>    
      </select>
    </label>
  </p>
  <p>Title
    <input name="BlogTitle" type="text" id="title" value="<? echo stripslashes($blog->BlogTitle); ?>" size="60" maxlength="300">
</p>
  <p>SubTitle 
    <input name="subtitle" type="text" id="title2" value="<? echo stripslashes($blog->BlogSubTitle); ?>" size="60" maxlength="300">
  </p>
  <div> 
    <textarea name="FCKeditor1" style="WIDTH: 601; HEIGHT: 500px"><? echo stripslashes($page); ?></textarea>
  </div>
			<br>
			<!-- <input type="hidden" name="page" value="<? echo stripslashes($page); ?>"> -->
            <input type="hidden" name="admin" value="<? echo $admin; ?>">
			<input type="hidden" name="blog_id" value="<? echo $blog_id; ?>">
			<input type="submit" value="Submit">
		</form>
	</body>

</html>
