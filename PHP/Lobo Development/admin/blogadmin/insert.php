<?PHP

include_once "../all_scripts/auth.php";// insert.php

include_once "../ez_sql.php";

include_once "../all_scripts/admin_functions.php";

// check SESSION vars for idUser --> Paste all this code to the beginning of each application form!
if (!isset($_SESSION["id"]))
{
header("Location: index.php");
 exit;
}

 // YES idUser --> set user_id & get all data from CONTACT
$idUser  = $_SESSION["id"];
$username = $_SESSION["username"];
$email    = $_SESSION["useremail"];
$admin = $_GET["admin"];


 $results = $db->get_row("SELECT FullName FROM AdminUser WHERE id = '$idUser'");

// $db->vardump($results);
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<title>Content Manager</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="robots" content="noindex, nofollow">
		<link href="../sample.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="../fckeditor/fckeditor.js"></script>

		<script type="text/javascript">

var oFCKeditor = new FCKeditor( 'FCKeditor1' ) ;

window.onload = function()
{

	// Automatically calculates the editor base path based on the _samples directory.
	// This is usefull only for these samples. A real application should use something like this:
	// oFCKeditor.BasePath = '/fckeditor/' ;	// '/fckeditor/' is the default value.
    var sBasePath = document.location.pathname.substring(0,document.location.pathname.lastIndexOf('_samples')) ;

	
	oFCKeditor.BasePath	= '../fckeditor/' ;
	oFCKeditor.ToolbarSet = 'Default' ;
	oFCKeditor.Height	= 500 ;
	oFCKeditor.ReplaceTextarea() ; 
}


function preview_popup(url) {
	window.open (url,"BlogCategories","location=0,status=0,scrollbars=1,resizable=1,width=650,height=270");  
}

		</script>
	<link href="../papa_CSS.css" rel="stylesheet" type="text/css" /></HEAD>
	<body vlink="blue">
<p><img src="../images/logo.gif"></p>
<table width="601" border="0" cellpadding="4" cellspacing="0" bgcolor="#8b8b8b">
  <tr> 
    <td width="80%"><font size="2" face="Arial"><a href="../adminpanel.php"><strong>Administration</strong></a> 
      <strong><font color=#000000>&gt; </font><a href="blog_panel.php<? echo "?admin=$admin"; ?>">Blog Panel</a><font color="#000000"> 
      <font color=#000000>&gt; </font>New Blog</font></strong></font></td>
    <td width="20%" align="right" valign="middle"><a href="../all_scripts/logout.php"><img src="../images/exit.png" width="16" height="16" border="0"></a> 
      <font size="2" face="Arial"><a href="../all_scripts/logout.php"><strong>Sign 
      Out </strong></a></font></td>
  </tr>
</table>
<hr>
		<form action="insert_insert.php" method="post">        
  <p>Author: <?php 
  
  
  // for admin (all userers) or prsonal (individual )
  if ($admin == 'yes') {
  
   		$blog_name_list = $db->get_results("SELECT * FROM AdminUser WHERE blog = 'yes'");
  	     echo '<select name="id" id="id">';
		  foreach ($blog_name_list as $blog_name_list_output) {
		  
		  	if ($idUser == $blog_name_list_output->id) {
				echo '<option value="'.$blog_name_list_output->id.'" selected>'.$blog_name_list_output->FullName.'</option>';
			
			} else {
				echo '<option value="'.$blog_name_list_output->id.'">'.$blog_name_list_output->FullName.'</option>';
			}
		  
		  }
   	 	  echo '</select>';
  } else {
  		echo '<b>'.$results->FullName.'</b> <input name="id" type="hidden" id="id" value="'.$idUser.'">';
  }
     ?>
  

  

    <input name="admin" type="hidden" id="admin" size="60" maxlength="100" value="<?php echo $admin  ?>">



  </p>
  <p>Title 
    <input name="title" type="text" id="title" size="60" maxlength="200">
  </p>
  <p>SubTitle 
    <input name="subtitle" type="text" id="title2" size="60" maxlength="300">
  </p>
  <p>Category
    <label>
    <select name="Category" id="Category">
    <option value="">none</option>
      <? 
	  
	  	$BlogCategories = $db->get_results("SELECT * FROM BlogCategories ORDER BY CategoryName");
	  
	  	if ($BlogCategories != NULL) {
	 		foreach ( $BlogCategories as $BlogCategories_output) {
	  			echo '<option value="'.stripslashes($BlogCategories_output->CategoryName).'">'.stripslashes($BlogCategories_output->CategoryName).'</option>'; 
	  		}
		} // if 
	  ?>
    </select>
    </label>
    <?
	if ($admin == 'yes'){
	    echo '<a href="javascript: preview_popup(\'../BlogCategories/pageupdater.php\')" ><img src="../images/BlogCategories_icon.png" width="16" height="16" align="top">Add New Blog Category</a></p>';
	}
	?>
  <p>Month 
    <input name="month" type="text" id="month" size="2" maxlength="2" value="<?php echo date("m") ?>">
    Day 
    <input name="day" type="text" id="day" size="2" maxlength="2" value="<?php echo date("d") ?>">
    Year 
    <input name="year" type="text" id="year" size="4" maxlength="4" value="<?php echo date("Y") ?>">
    <font color="#FFFFFF">|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||</font>Hour 
    <input name="hour" type="text" id="hour" size="2" maxlength="2" value="<?php echo date("H") ?>">
    Minute 
    <input name="minute" type="text" id="minute" size="2" maxlength="2" value="<?php echo date("i") ?>">
    Second 
    <input name="second" type="text" id="second"  size="2" maxlength="2" value="<?php echo date("s") ?>">
  </p>
			<div>

				<textarea name="FCKeditor1" style="WIDTH: 601; HEIGHT: 500px"></textarea>
			</div>
			<br>
			<input type="submit" value="Submit">
		</form>
	</body>

</html>
