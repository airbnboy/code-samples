<?PHP

include_once "../all_scripts/auth.php";// insert_insert.php

//Load next page --> adminpanel.php
header("Location: pageupdater.php"); 



include_once "../all_scripts/admin_functions.php";
include_once "../ez_sql.php";
require_once('../xml/minixml.inc.php');


$NavXMLFileName = "../../test-flash/mainnav2.xml";

$featured_list = $_POST['featured_list'];




// for use with Gabe's flash xml menu structure to rebuild menus
// using his flash protocol flash://function(id)
function RebuildDynamicXMLMenu($mxl_popup_reference_menu_id, &$SORTED_EZ_SQL_MENU_ITEMS_ARRAY) {

	global $NavXMLFileName;
	$xmlDoc = new MiniXMLDoc( );
	$xmlArray = array();
	$xmlDoc->fromFile(  $NavXMLFileName );

	$generic_doc_root =& $xmlDoc->getRoot();  // top top level before any tags
 	$flash_menu =& $generic_doc_root->getAllChildren(); // first top level tag
	$menu =& $flash_menu[0]->getAllChildren(); // first next level tag
 
 	// Modify menu data
 	for($i = 0; $i < $flash_menu[0]->numChildren(); $i++){

			$menu_name =& $menu[$i]->getElement('name'); // name element	
			$menu_options =& $menu[$i]->getElement('menu_options'); // name element		
					
   			//$itsValue =& $menu_name->getValue();
			//echo $itsValue;
			
			// Stop at the menu id from the table
			if (trim($menu_name->getValue()) == $mxl_popup_reference_menu_id){
				
				// Erase all of the links to prepare for rebuild
				$menu_options->removeAllChildren();
								
				// Rebuild Portfolio Menus	
				if ($SORTED_EZ_SQL_MENU_ITEMS_ARRAY != NULL) {			
				$t = 1;	
				$items_size = sizeof($SORTED_EZ_SQL_MENU_ITEMS_ARRAY);		

		
				foreach($SORTED_EZ_SQL_MENU_ITEMS_ARRAY as $updated_menu_array){
						
					// build menu option and link
					$menu_option =& $menu_options->createChild('menu_option');
					
					if (trim($updated_menu_array->Featured) == 'true') {	
						//echo "featured ".$updated_menu_array->Featured."\n";										
						$menu_option->attribute('featured_item', $updated_menu_array->id);						 					
					}
					$menu_option_name =& $menu_option->createChild('menu_option_name');
					$menu_option_link =& $menu_option->createChild('menu_option_link');

					$menu_option_name->text($updated_menu_array->PageTitle);
					$menu_option_link->text('flash://LoadBio('.$updated_menu_array->id.')');					
					
					// Add the menu spacers if its not at the end
					if ($t < $items_size) {
					
						// build menu option and link
					/*	$menu_option =& $menu_options->createChild('menu_option');
						$menu_option->attribute('no_highlight', 'true');						
						$menu_option_name =& $menu_option->createChild('menu_option_name');
						$menu_option_link =& $menu_option->createChild('menu_option_link');
						$menu_option_name->text(':');	
						$menu_option_link->text('#');		*/				
					}					
					$t++;
				} //foreach ...		
				}										
			}  // if ...						
	} // for...
	
	// store to the XML file
	$fh = fopen($NavXMLFileName, 'w') or die("can't open file");
	$stringData = $flash_menu[0]->toString();
	$stringData = str_replace("> ", ">", str_replace(" <", "<", $stringData));
	fwrite($fh, $stringData);
	fclose($fh);
	
	
	//echo ($flash_menu[0]->toString());
	
}  //function...


// update	  
	
	//clear all featured flags
	$results = $db->query("UPDATE Bios SET Featured = NULL");

		
	

	// flag all the selected ids as featured and rebuild the xml file
	if ($featured_list != NULL) {
		foreach ($featured_list as $featured_list_output) {
			$results = $db->query("UPDATE Bios SET Featured = 'true' WHERE ID = '$featured_list_output'");	
		}	
		
	} 
	
		// Get the list of ids for that category from the portfolio table sorted
	$bio_items = $db->get_results("SELECT * FROM Bios ORDER BY Featured Desc, PageTitle");		
		
	// Rebuild XML Menu File
	 RebuildDynamicXMLMenu(105, $bio_items);




// $db->vardump($results);


?>


