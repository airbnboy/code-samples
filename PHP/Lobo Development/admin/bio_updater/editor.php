<?PHP

include_once "../all_scripts/auth.php";
include_once "../all_scripts/admin_functions.php";
$idUser  = $_SESSION["id"];
$username = $_SESSION["username"];
$email    = $_SESSION["email"];

	$content_id = $_GET["content_id"];
	$page		= $_GET["page"]; 
	
	// image refresh data
	$RefreshImages = $_GET["RefreshImages"];

	$NoImage = '<img src="images/noimageregular.jpg" width="347" height="162">';
	$NoThumb = '<img src="images/blankthumb.jpg" width="51" height="51">';

 
	
	include_once "../ez_sql.php";

   // special user 
   $AdminUser = $db->get_row("SELECT * FROM AdminUser WHERE $idUser = id");
   


	// Check to see if user information doen't exist
	if ($content_id != NULL) {
		$page_content = $db->get_row("SELECT * FROM Bios  WHERE (id = '$content_id')");	
		$logged_in_user = false;		
	} else {
		$page_content = $db->get_row("SELECT * FROM Bios  WHERE (UserRef = '$idUser')");
		$content_id = $page_content->id;
		$logged_in_user = true;	
	}
	
	
	// Get categories for the DropDown
	//$PortfolioCategoryResults = $db->get_results("SELECT * FROM PortfolioCategories");	

	
	//$db->vardump($page_content);
	
	// Overwrite what the DB provides if RefreshImages is true, because they are being edited
	if($RefreshImages == "true") {
		$FCKeditor1  = $_SESSION["FCKeditor1"];	
		$FCKeditor2  = $_SESSION["FCKeditor2"];			
		$PageTitle   = $_GET["PageTitle"];
		$Location   = $_GET["Location"];	
		$Category    = $_GET["Category"];
		$ShortURL      = $_SESSION["ShortURL"];	
		$Image1      = $_GET["Image1"];
		$Thumb1      = $_GET["Thumb1"];
		$Image2      = $_GET["Image2"];
		$Thumb2      = $_GET["Thumb2"];
		$Image3      = $_GET["Image3"];
		$Thumb3      = $_GET["Thumb3"];
		$Image4      = $_GET["Image4"];
		$Thumb4      = $_GET["Thumb4"];			
	} else {
		$FCKeditor1 = stripslashes($page_content->Page);
		$FCKeditor2 = stripslashes($page_content->Page2);
		$PageTitle = stripslashes($page_content->PageTitle);	
		$Location = stripslashes($page_content->Location);						
		$Category = stripslashes($page_content->Category);
		$ShortURL = stripslashes($page_content->ShortURL);
		$Image1 = stripslashes($page_content->Image1);
		$Thumb1 = stripslashes($page_content->Thumb1);
		$Image2 = stripslashes($page_content->Image2);	
		$Thumb2 = stripslashes( $page_content->Thumb2);
		$Image3 = stripslashes($page_content->Image3);				
		$Thumb3 = stripslashes($page_content->Thumb3);
		$Image4 = stripslashes($page_content->Image4);
		$Thumb4 = stripslashes($page_content->Thumb4);						
	}
	
	
	
	include_once "../admin_settings.php";

?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<title>Content Manager</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="robots" content="noindex, nofollow">
		<!-- <link href="../sample.css" rel="stylesheet" type="text/css" /> -->
		<!-- <link href="../../flash.css" rel="stylesheet" type="text/css" /> -->
		<script type="text/javascript" src="../fckeditor/fckeditor.js"></script>

<script language="javascript">

createNotes=function(){

            showNote=function(){
                        // gets note1 element
                        var note1=document.getElementById('note1');
                        // shows note1 element
                        note1.style.visibility='visible';
            }

            hideNote=function(){
                        // gets note1 element
                        var note1=document.getElementById('note1');
                        // hides note1 element
                        note1.style.visibility='hidden';
            }
}
// execute code once page is loaded
window.onload=createNotes;
</script>

<style type="text/css">

p {
            font: normal 12px "Verdana", Arial, Helvetica, sans-serif;
            color: #000;
}

#note1 {
            position: absolute;
            top: 350px;
            left: 40px;
			width: 60%;
            background: #ffc;
            padding: 10px;
            border: 4px solid #000;
            z-index: 2;
            visibility: hidden;
            font: bold 20px "Verdana", Arial, Helvetica, sans-serif;
            color: #000;
		    background-attachment: fixed;
}
</style>
		

<script type="text/javascript">
//<!--
	var oFCKeditor = new FCKeditor( 'FCKeditor1' ) ;
	var oFCKeditor2 = new FCKeditor( 'FCKeditor2' ) ;

window.onload = function()
{

<?  if($RefreshImages == "true") {echo 'window.location.hash="imagework"';}  // jump to images if they are being worked on ?>
	// Automatically calculates the editor base path based on the _samples directory.
	// This is usefull only for these samples. A real application should use something like this:
	// oFCKeditor.BasePath = '/fckeditor/' ;	// '/fckeditor/' is the default value.
    var sBasePath = document.location.pathname.substring(0,document.location.pathname.lastIndexOf('_samples')) ;

	
	oFCKeditor.BasePath	= '../fckeditor/' ;
	oFCKeditor.ToolbarSet = 'Default' ;
	oFCKeditor.Height	= 500 ;
	oFCKeditor.ReplaceTextarea() ; 


	oFCKeditor2.BasePath	= '../fckeditor/' ;
	oFCKeditor2.ToolbarSet = 'Default' ;
	oFCKeditor2.Height	= 200 ;
	oFCKeditor2.ReplaceTextarea() ; 	
}


function preview_popup(ImageName) {

	window.open ("full_size.php?PreviewImage="+ ImageName,"Preview","location=0,status=0,scrollbars=1,resizable=1,width=750,height=370");  

}

// Selects the upload data for the current form
function AppendFormVars(FormObject) {


	var oEditor = FCKeditorAPI.GetInstance('FCKeditor1');
	var oEditor2 = FCKeditorAPI.GetInstance('FCKeditor2');
	FormObject.content_id.value = document.mainForm.content_id.value;
	FormObject.PageTitle.value = document.mainForm.PageTitle.value;	
	FormObject.Location.value = document.mainForm.Location.value;	
	FormObject.ShortURL.value = document.mainForm.ShortURL.value;		
	FormObject.Category.value = document.mainForm.Category.value;
		
	FormObject.FCKeditor1.value = oEditor.GetXHTML( true );	
	FormObject.FCKeditor2.value = oEditor2.GetXHTML( true );
	FormObject.page.value = document.mainForm.page.value;	
}

function UpdateExportedFCK(FCKObj) {
	document.mainForm.FCKHidden.value = FCKObj.value;
}

function alphaNumericCheck(form_item) {
	var regex=/^[0-9A-Za-z\-]+$/; //^[a-zA-z]+$/
	if(regex.test(form_item)){
		return false;
	} else {
		return true;
	}
}

// Main Form Submission Fuctions
function MainFormSubmit() {

	
	// validation section
	if (alphaNumericCheck(document.mainForm.ShortURL.value)) {
		alert('Please enter an alpha-numeric characters or a dash in \'Shortened URL \' only please');
		return false;
	} else {
	
		// show the loading banner/note
		note1.style.visibility='visible';
		return true;
	}

}
//-->
</script>
	<link href="../papa_CSS.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
<!--
.style2 {font-size: x-small} 
.style3 {color: #FFFFFF}
.style4 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
}
-->
    </style>


	</head>
	<body vlink="blue">

<div id="note1">Building Website...</div>
	
<p><img src="../images/logo.gif"></p>
<table width="100%" border="0" cellpadding="4" cellspacing="0" bgcolor="#EFEFEF">
  <tr> 
    <td width="87%"><font size="2" face="Arial"><a href="../adminpanel.php"><strong>Administration</strong></a> 
      <strong>&gt; 
      <? 
	  	// show breadcrumb if admin
	  	if ($logged_in_user == false) {
			echo '<a href="pageupdater.php">Bio Manager</a><font color="#000000"> &gt;';
		}
		  
		// add/edit   	
	  	if ($content_id == NULL) {
			echo " Add Bio";
		} else {
			echo " Edit Bio";
		}
		 ?></font></strong></font></td>
    <td width="13%" align="right" valign="middle"><a href="index.php"><img src="../images/exit.png" width="16" height="16" border="0"></a> 
      <font size="2" face="Arial"><a href="../all_scripts/logout.php"><strong>Sign Out</strong></a></font></td>
  </tr>
</table>
<form action="editor_insert.php" method="post" name="mainForm" id="mainForm"  enctype="multipart/form-data" onSubmit="return MainFormSubmit()">
  <p>Name <b><? echo $PageTitle; ?></b>
    <input name="PageTitle" type="hidden" id="PageTitle" size="40" value="<? echo $PageTitle; ?>">
  <input name="FCKHidden" type="hidden" id="FCKHidden" value="">  
  </p>
  <p>Title/Position 
    <input name="Location" type="text" id="Location" size="40" value="<? echo $Location; ?>">
</p>
   
<!--    <P>
    
       Shortened URL
    <b>/</b><?
    if (strtolower($AdminUser->administrator) == 'yes') {
   		echo '<input name="ShortURL" type="text" id="ShortURL" size="60" value="'. $ShortURL.'">';
    } else {
	echo '<b>'.$ShortURL.'</b>';
	echo '<input name="ShortURL" type="hidden" id="ShortURL" size="60" value="'. $ShortURL.'">';
	}
	
    ?>   
    </p> -->
    
    
   <p>Project Category
    <select name="Category">
      <?
  	if ($Category != NULL) {
  		echo '<option value="'.$Category.'">'.$Category.'</option>';
  	}
		echo '<option value="Board">Board</option>'."\n";
		echo '<option value="Staff">Staff</option>'."\n";
	?>
    </select>
  </p> <span class="style4">Description</span><br>
<span class="style2"><a href="#imagework">(jump to image updater) </a></span>
<div>
    <p>
      <textarea name="FCKeditor1" class="formWords" style="WIDTH: 100%; HEIGHT: 500px" onChange="UpdateExportedFCK(this)"><? echo stripslashes($FCKeditor1); ?></textarea>
    </p>
    
 	<span class="style4">Personal Background</span><br>   
    <p>
      <textarea name="FCKeditor2" class="formWords" style="WIDTH: 100%; HEIGHT: 200px" onChange="UpdateExportedFCK(this)"><? echo stripslashes($FCKeditor2); ?></textarea>
    </p>
    
    
    
  </div>
  <? echo $HTMLEditorInstructions; ?>
  <input type="hidden" name="content_id" value="<? echo $page_content->id; ?>">
  <input type="hidden" name="page" value="<? echo $page; ?>">
  <input name="Image1" type="hidden" id="Image1" value="<? echo $Image1; ?>">
  <input name="Thumb1" type="hidden" id="Thumb1" value="<? echo $Thumb1; ?>">
  <input name="Image2" type="hidden" id="Image2" value="<? echo $Image2; ?>">
  <input name="Thumb2" type="hidden" id="Thumb2" value="<? echo $Thumb2; ?>">
  <input name="Image3" type="hidden" id="Image3" value="<? echo $Image3; ?>">
  <input name="Thumb3" type="hidden" id="Thumb3" value="<? echo $Thumb3; ?>">
  <input name="Image4" type="hidden" id="Image4" value="<? echo $Image4; ?>">
  <input name="Thumb4" type="hidden" id="Thumb4" value="<? echo $Thumb4; ?>">
</form>
<p>
  <input name="submit2" type="submit" class="formButton" value="Save Bio" onClick="javascript: document.mainForm.submit();">
</p>
<p><A NAME="imagework"></A></p>
<p>&nbsp;</p>
<table width="414" border="0">
  <tr>
    <td  bgcolor="#CCCCCC"><span class="style3">Image 1</span></td>
  </tr>
  <tr>
    <td align="center" valign="bottom">
	
	<? if ($Image1 == NULL){ echo $NoImage; } else {echo '<a href="javascript:preview_popup(\'../../UserFiles/'.$Image1.'\')"><img src="../../UserFiles/'.$Image1.'" onClick="" border="0"></a>'; } ?></td>
  </tr>
  <tr align="left" valign="top">
    <td>
<?
	echo'<form name="Image1Form" method="post" action="image_upload_insert.php" enctype="multipart/form-data" onSubmit="AppendFormVars(this)">'."\n";
	echo '<input name="ModifiedImageName" type="hidden"  value="Image1">'."\n";	
	echo '<input name="content_id" type="hidden"  value="'.$content_id.'">'."\n";
	echo '<input name="PageTitle" type="hidden" value="'.$PageTitle.'">'."\n";
	echo '<input name="Location" type="hidden" value="'.$Location.'">'."\n";	
	echo '<input name="Category" type="hidden" value="'.$Category.'">'."\n";	
	echo '<input name="page" type="hidden"  value="'.$page.'">'."\n";
	echo '<input name="FCKeditor1" type="hidden" value="">'."\n";	
	echo '<input name="FCKeditor2" type="hidden" value="">'."\n";	
	echo '<input name="Image1" type="hidden" value="'.$Image1.'">'."\n";
	echo '<input name="Thumb1" type="hidden" value="'.$Thumb1.'">'."\n";
	echo '<input name="Image2" type="hidden" value="'.$Image2.'">'."\n";
	echo '<input name="Thumb2" type="hidden" value="'.$Thumb2.'">'."\n";
	echo '<input name="Image3" type="hidden" value="'.$Image3.'">'."\n";
	echo '<input name="Thumb3" type="hidden" value="'.$Thumb3.'">'."\n";
	echo '<input name="Image4" type="hidden" value="'.$Image4.'">'."\n";
	echo '<input name="Thumb4" type="hidden" value="'.$Thumb4.'">'."\n";	
	echo '<input name="ShortURL" type="hidden" value="'.$ShortURL.'">'."\n";			

		if ($Image1 == NULL){ 
	 		echo '<input name="UploadFileName" type="file" size="40">';	
	   		echo '<input name="UploadSubmit" type="submit"  value="Upload">';
	   } else {
	  		echo '<input name="ClearImage" type="hidden" value="true">';	
	  		echo '<input name="ClearSubmit" type="submit"  value="Clear">';
	   }
	   
	 echo'</form>';
	   
?>

</td></tr>
  <tr>
    <td  bgcolor="#CCCCCC"><span class="style3">Image 2</span></td>
  </tr>
  <tr>
  <tr>
    <td align="center" valign="bottom">
	
	<? if ($Image2 == NULL){ echo $NoImage; } else {echo '<a href="javascript:preview_popup(\'../../UserFiles/'.$Image2.'\')"><img src="../../UserFiles/'.$Image2.'" onClick="" border="0"></a>'; } ?></td>
  </tr>
  <tr><td>
 
  <?	   	
	   
	 echo'<form name="Image2Form" method="post" action="image_upload_insert.php" enctype="multipart/form-data" onSubmit="AppendFormVars(this)">'."\n";
	echo '<input name="ModifiedImageName" type="hidden"  value="Image2">'."\n";	
	echo '<input name="content_id" type="hidden"  value="'.$content_id.'">'."\n";
	echo '<input name="PageTitle" type="hidden" value="'.$PageTitle.'">'."\n";
	echo '<input name="Location" type="hidden" value="'.$Location.'">'."\n";	
	echo '<input name="Category" type="hidden" value="'.$Category.'">'."\n";	
	echo '<input name="page" type="hidden"  value="'.$page.'">'."\n";
	echo '<input name="FCKeditor1" type="hidden" value="">'."\n";	
	echo '<input name="FCKeditor2" type="hidden" value="">'."\n";	
	echo '<input name="Image1" type="hidden" value="'.$Image1.'">'."\n";
	echo '<input name="Thumb1" type="hidden" value="'.$Thumb1.'">'."\n";
	echo '<input name="Image2" type="hidden" value="'.$Image2.'">'."\n";
	echo '<input name="Thumb2" type="hidden" value="'.$Thumb2.'">'."\n";
	echo '<input name="Image3" type="hidden" value="'.$Image3.'">'."\n";
	echo '<input name="Thumb3" type="hidden" value="'.$Thumb3.'">'."\n";
	echo '<input name="Image4" type="hidden" value="'.$Image4.'">'."\n";
	echo '<input name="Thumb4" type="hidden" value="'.$Thumb4.'">'."\n";	
	echo '<input name="ShortURL" type="hidden" value="'.$ShortURL.'">'."\n";		

		if ($Image2 == NULL){ 
	 		echo '<input name="UploadFileName" type="file" size="40">'."\n";	
	   		echo '<input name="UploadSubmit" type="submit"  value="Upload">'."\n";
	   } else {
	  		echo '<input name="ClearImage" type="hidden" value="true">'."\n";	
	  		echo '<input name="ClearSubmit" type="submit"  value="Clear">'."\n";
	   }
	   
	   
	   
	  echo'</form>';
?>	</td>
  </tr>

</table>

<p>
  <input name="submit" type="submit" class="formButton" value="Save Bio" onClick="javascript: document.mainForm.submit();">
</p>
<p>&nbsp;</p>
<p><b><font size="2" face="Arial">&nbsp; Custom interface provided by TaGG Studios<br>
  &nbsp; <a href="mailto:info@taggstudios.com">info@taggstudios.com</a>&nbsp;&nbsp; 
  505.246.8244</font></b> <br>
  <a href="http://www.taggstudios.com" target="_blank"><img src="../images/taggstudios_logo.jpg" alt="TaGG Studios" width="204" height="141" border="0"></a></p>
	</body>

</html>
