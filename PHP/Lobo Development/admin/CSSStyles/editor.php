<?PHP
	include_once "../all_scripts/auth.php";
	include_once "../all_scripts/admin_functions.php";

	$idUser  = $_SESSION["id"];
	$username = $_SESSION["username"];
	$email    = $_SESSION["email"];

	// Page selection vars
	$content_id = $_GET["content_id"];
	$page		= $_GET["page"]; 
	

	include_once "../ez_sql.php";
	


	// Load Content
	if ($content_id != NULL) {
 		$CSSStyles = $db->get_row("SELECT * FROM CSSStyles WHERE (id = '$content_id')");		
	}
	
	// Load Admin User rights - optional
	$AdminUser = $db->get_row("SELECT * FROM AdminUser WHERE (id = '$idUser')");
	
	//$db->vardump($page_content); // dbdebug
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<title>Content Manager :: CSS Styles</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="robots" content="noindex, nofollow">
		<link href="../papa_CSS.css" rel="stylesheet" type="text/css" />
        
        <!-- Hilighting -->        
        <script src="../CodeHighlighter/lib/codemirror.js"></script>
		<link rel="stylesheet" href="../CodeHighlighter/lib/codemirror.css">
        <script src="../CodeHighlighter/mode/css/css.js"></script>
		<link rel="stylesheet" href="../CodeHighlighter/theme/tagg_css.css">
       
    	
        
         <!-- End Hilighting -->
        
        
		<!-- _FCK_EDITOR_ -->
	</head>
	<body>
	<p><img src="../images/logo.gif"></p>
	<table width="100%" border="0" cellpadding="4" cellspacing="0" bgcolor="#EFEFEF">
  <tr> 
		    <td width="87%"><font size="2" face="Arial">
            	<a href="../adminpanel.php"><strong>Administration</strong></a> 
		      	<strong>&gt; <a href="pageupdater.php">CSS Styles</a><font color="#000000"> 
		      &gt; <? 	
			  	if ($content_id == NULL) {
					echo "Add Page";
				} else {
					echo "Edit Page";
				}
				 ?></font></strong></font>
  		  	</td>
    		<td width="13%" align="right" valign="middle">
    			<a href="../all_scripts/logout.php"><img src="../images/exit.png" width="16" height="16" border="0"></a> 
      			<font size="2" face="Arial"><a href="../all_scripts/logout.php"><strong>Sign Out</strong></a></font>
      		</td>
  		</tr>
	</table>
    <form action="editor_insert.php" method="post" name="CSSStyles" id="CSSStyles"  enctype="multipart/form-data">
    <input type="hidden" name="content_id" value="<? echo $content_id; ?>">
    
		<? if (true) {
 				echo '<p>CSS Style Name <input type="text" id="CSSStyleName" name="CSSStyleName" class="" value="'; if (trim($CSSStyles->CSSStyleName) == NULL) { echo stripslashes(""); } else { echo stripslashes("$CSSStyles->CSSStyleName"); } echo '"> </p>';
 			 } else {
			 	echo '<input type="hidden" id="'.stripslashes("CSSStyleName").'"  name="'.stripslashes("CSSStyleName").'" value="'; if (trim($CSSStyles->CSSStyleName) == NULL) { echo stripslashes(""); } else { echo stripslashes("$CSSStyles->CSSStyleName"); } echo '">';
			 } ?>

		<? /* if (strtolower($AdminUser->administrator) == 'yes') {
 				echo '<p>File Name (placed in css folder) <input type="text" id="FileName" name="FileName" class="" value="'; if (trim($CSSStyles->FileName) == NULL) { echo stripslashes(""); } else { echo stripslashes("$CSSStyles->FileName"); } echo '"> </p>';
 			 } else {
			 	echo '<input type="hidden" id="'.stripslashes("FileName").'"  name="'.stripslashes("FileName").'" value="'; if (trim($CSSStyles->FileName) == NULL) { echo stripslashes(""); } else { echo stripslashes("$CSSStyles->FileName"); } echo '">';
			 } */ ?>

		<? if (true) {
 				echo '<p>Comments <input type="text" id="Comments" name="Comments" class="" value="'; if (trim($CSSStyles->Comments) == NULL) { echo stripslashes(""); } else { echo stripslashes("$CSSStyles->Comments"); } echo '"> </p>';
 			 } else {
			 	echo '<input type="hidden" id="'.stripslashes("Comments").'"  name="'.stripslashes("Comments").'" value="'; if (trim($CSSStyles->Comments) == NULL) { echo stripslashes(""); } else { echo stripslashes("$CSSStyles->Comments"); } echo '">';
			 } ?>

		<? if (true) {
 				echo '<p>CSS Code <br>
		<textarea  name="CSSCode" id="CSSCode" class="formWords" style="WIDTH: 100%;">'; if (trim($CSSStyles->CSSCode) == NULL) { echo stripslashes(""); } else { echo stripslashes("$CSSStyles->CSSCode"); } echo '</textarea></p>';
 			 } else {
			 	echo '<input type="hidden" id="'.stripslashes("CSSCode").'"  name="'.stripslashes("CSSCode").'" value="'; if (trim($CSSStyles->CSSCode) == NULL) { echo stripslashes(""); } else { echo stripslashes("$CSSStyles->CSSCode"); } echo '">';
			 } ?>

		
	</form>
    <p>
  		<input name="submit" type="submit" class="formButton" value="Save Page" onClick="javascript: document.CSSStyles.submit();">
	</p>
    
    <script>
      var editor = CodeMirror.fromTextArea(document.getElementById("CSSCode"), {});
    </script>
	
    <p>&nbsp;</p>
	</body>
</html>
