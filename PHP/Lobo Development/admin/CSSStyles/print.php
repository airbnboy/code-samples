<?PHP
	include_once "../all_scripts/auth.php";
	include_once "../all_scripts/admin_functions.php";

	$idUser  = $_SESSION["id"];
	$username = $_SESSION["username"];
	$email    = $_SESSION["email"];
	$patient_id    = $_SESSION["patient_id"];

	// Page selection vars
	$content_id = $_GET["content_id"];
	$page		= $_GET["page"]; 
	$print    = $_GET["print"];

	include_once "../ez_sql.php";
	


	// Load Content
	if ($content_id != NULL) {
 		$CSSStyles = $db->get_row("SELECT *, UNIX_TIMESTAMP(StartTime) AS FORMATED_TIME_BEGIN FROM CSSStyles WHERE (id = '$content_id')");		
	}
	
	// Load Admin User rights - optional
	$AdminUser = $db->get_row("SELECT * FROM AdminUser WHERE (id = '$idUser')");
	
	// get basic paitent infor for top
	if ($patient_id != NULL) {
 		$PatientProfileContent = $db->get_row("SELECT * FROM PatientProfiles WHERE id = $patient_id");		
	}
	
	//$db->vardump($page_content); // dbdebug
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<title>Content Manager :: CSS Styles</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="robots" content="noindex, nofollow">
		<link href="../papa_CSS.css" rel="stylesheet" type="text/css" />
		<!-- _FCK_EDITOR_ -->
		<style type="text/css">
			
		/* Properties that both side lists have in common */
		div.link-list {
        	width:25.2em;
        	position:absolute;
        	top:230;
        	/*font-size:80%; */
        	padding-left:1%;
        	padding-right:1%;
        	margin-left:40;
        	margin-right:0;
			font-weight:bold;
		}
				
		/* and then we put each list on its place */
		#patient_data_list_left {
        	left:10;
		}
		#patient_data_list_right {
        	right:0;
		}
		
		#main_content {
        	position:absolute;
        	top:480;
		}
		#single_indent {
			text-indent:50px;
		}
		center {
			font-weight:bold;
		}

		
		
			@media print {
				body * {
					display:none;
				}
				#print_div {
					display:block;
				}
				p {
		      		display:block;
		     	}
				table {
		      		display:block;
		     	}		
				IMG {
		      		display:block;
		     	}	
				b {
		      		display:block;
		     	} 	
				hr {
					display:block;
				}
			/*	#middle_div {
		    		width:250px;
		    		padding:10px;
		    		background-color:#efe;
		    		margin-right:auto;
		    		margin-bottom:60px;
		    		margin-left:auto;
					display:block;
				 }*/
				 h3 {
				 	display:block;
				 }
				 
				 	/* Properties that both side lists have in common */
		div.link-list {
        	width:22.2em;
        	position:absolute;
        	top:230;
        	/*font-size:80%; */
        	padding-left:1%;
        	padding-right:1%;
        	margin-left:0;
        	margin-right:0;
			display:block;
			font-weight:bold;
		}
				
		/* and then we put each list on its place */
		#patient_data_list_left {
        	left:15;
			
		}
		#patient_data_list_right {
        	right:0;
			
		}
		
		#main_content {
        	position:relative;
        	top:295;
			display:block;
		}
		
		#single_indent {
			text-indent:50px;
			display:block;
		}
		
		center {
			font-weight:bold;
			display:block;
		}


	
			}
		</style>      
	</head>
	<body>
        <?  if($print != NULL) { 
		echo'
	<table width="100%" border="0">
      <tr>
        <td align="right"><a href="javascript:window.print();"><img src="../images/print_printer.png" alt="print" width="16" height="16" border="0"><i><font size="2" face="Arial, Helvetica, sans-serif"> print report below</font></i></a></td>
      </tr>
    </table>'; }
	?>
    <div id="print_div">
	<p><img src="../images/logo.gif"></p>
    <center><b>CSS Styles</b></center>

    <div id="patient_data_list_left" class="link-list">
    
		<p> Patient Name: <? echo "$PatientProfileContent->LastName , $PatientProfileContent->FirstName"; ?> </p>
        <p>Address: <? echo $PatientProfileContent->Address; ?> </p>
        <p><div id="single_indent"><? echo $PatientProfileContent->CityStateZip; ?> </div><!-- single_indent --></p>
        <p>Phone: <? echo $PatientProfileContent->phone1; ?> </p>
        <p>SSN: <? echo $PatientProfileContent->SSN; ?></p>
        <p>DOB: <? echo $PatientProfileContent->DOB; ?></p>
        
        
            
    </div> <!-- patient_data_list_left -->
    <div id="patient_data_list_right" class="link-list">
    
    <p><b>Date of Report: </b><?php 
		echo date('d/m/Y',$ContactNoteContent->FORMATED_TIME_BEGIN); ?></p>
    
        	<p>Case Manager: <? 
			$CaseManager = $db->get_row("SELECT * FROM CaseManager WHERE (id = '$PatientProfileContent->CaseManagerRef')");			echo "$CaseManager->CaseManagerFullName"; 
		
			
			?></p>
            <p>CM Company: <? echo $CaseManager->CaseManagerAgency; ?></p>
            <p>CM Fax/Contact #: <? echo "$CaseManager->CaseManagerFax (fax)/$CaseManager->CaseManagerPhone"; ?></p>
            
            <p>Therapist: <? 
			$Therapist = $db->get_row("SELECT * FROM AdminUser WHERE (id = '$CSSStyles->TherapistRef')");
			echo "$Therapist->FullName, $Therapist->TherapistType";  ?></p>
           
            <p>Therapist #: <? echo $Therapist->ContactPhone1; ?></p>
            
            <p>ICD9: <? echo $PatientProfileContent->ICD9 ?></p>
  
    </div> <!-- patient_data_list_left -->
    
<div id="main_content" >
<hr/><p> </p>

    <?
	echo'<p>CSS Style Name</p> <p>'.stripslashes($CSSStyles->CSSStyleName).'</p>';

		echo'<p>Comments</p> <p>'.stripslashes($CSSStyles->Comments).'</p>';

		echo'<p>CSS Code</p> <p>'.stripslashes($CSSStyles->CSSCode).'</p>';

		
	?>
    <p>Signature:</p>

  <?
   		$AdminUserInfo = $db->get_row("SELECT * FROM AdminUser WHERE $CSSStyles->TherapistRef = id");
		echo '<IMG src="../../UserFiles/signatures/'.$AdminUserInfo->SignatureImage.'" height="50" width="250">';	
  ?>
	<p>&nbsp;</p>
    </div><!-- print div -->
    </div><!-- main div -->
</body>
</html>
