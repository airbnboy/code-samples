<?php

// adminpanel.php

include_once "../ez_sql.php";

include_once "../all_scripts/auth.php";
include_once "../all_scripts/admin_functions.php";



?>


<HTML xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns="http://www.w3.org/TR/REC-html40">
<HEAD>
<meta HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=windows-1252">
<meta http-equiv="Content-Language" content="en-us">
<title>Content Manager :: CSS Styles</title>
<script type="text/javascript">
<!--
function delete_confirmation(delete_address) {
var answer = confirm("Are you sure you want to delete this entry?")
if (answer){
  window.location = delete_address;
}

}

function out_popup() {

 window.open ("pageupdater_out.php","uncollectedwindow","location=0,status=0,scrollbars=1,resizable=0,width=850,height=570");  

}
//-->
</script>
<SCRIPT language="JavaScript">
<!--
function Update_DB() {			
	note1.style.visibility='visible';
	document.CheckForm.submit();	
									
}
			
function go_there(address) {
	var delete_it= confirm("Do you really want to delete this record?");
	if (delete_it== true) {
		window.location=address;
	}
}																		
//-->
</SCRIPT>

<link href="../papa_CSS.css" rel="stylesheet" type="text/css" /><link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">
<link rel="icon" href="../images/favicon.ico" type="image/x-icon"></head>

<BODY>
<?

	// Get Pages Query
	$CSSStyles = $db->get_results("SELECT * FROM CSSStyles ORDER BY CSSStyleName");
	//$db->vardump($CSSStyles);	

?>


<h2><img src="../images/logo.gif"></h2>
<table width="100%" border="0" cellpadding="4" cellspacing="0" bgcolor="#EFEFEF">
  <tr>
    <td width="87%"><font color="#FFFFFF" size="2" face="Arial"><a href="../adminpanel.php"><strong>Administration</strong></a><strong><font color="#000000"> 
    &gt; CSS Styles</font></strong></font></td>
    <td width="13%" align="right" valign="middle"><a href="../index.php"><img src="../images/exit.png" width="16" height="16" border="0"></a> 
      <font size="2" face="Arial"><a href="../all_scripts/logout.php"><strong>Sign Out</strong></a></font></td>
  </tr>
</table>
<h2 align="left"><font color="#000000" face="Arial, Helvetica, sans-serif"><strong><a name="page_select"></a><img src="../images/CSSStyles_icon.png" alt="" width="32" height="32" border="0"> CSS Styles</strong></font></h2>

<form name="form1" method="post" action="editor.php">
  <input class="formbutton" name="addUser2" type="button" id="addUser2" value="New Entry" onClick="parent.location='editor.php'">
</form>
<p><a href="storyeditor.php?content_id=1&page=CurrentEvents"><font size="2" face="Arial, Helvetica, sans-serif"><img src="../images/b_edit.png" alt="edit" width="16" height="16" border="0"></font></a><font size="2" face="Arial, Helvetica, sans-serif"> 
  = Edit  </font>, <a href="storyeditor.php?content_id=1&page=CurrentEvents"><font size="2" face="Arial, Helvetica, sans-serif"><img src="../images/b_drop.png" alt="edit" width="16" height="16" border="0"></font></a><font size="2" face="Arial, Helvetica, sans-serif"> =
Delete </font></p>
<form action="featured_insert.php" name="CheckForm" method="post">
<table width="1024" border="0" cellpadding="0" cellspacing="1" bgcolor="#BABABA">
  <tr bgcolor="#BABABA"> 
    <td align="center" valign="bottom"  colspan="2"> <div align="center"><font color="#FFFFFF" size="1" face="Arial, Helvetica, sans-serif">Options</font><font color="#FFFFFF"></font></div></td>
		<? 
 echo '<td align="center"><strong><font color="#FFFFFF">'.stripslashes('CSS Style Name').'</font></strong></td>';
echo '<td align="center"><strong><font color="#FFFFFF">'.stripslashes('
Comments').'</font></strong></td>';
 ?>
    </tr>
  
  <?php
 
  $i =1;
  
  if ($CSSStyles != NULL) {
  	foreach ($CSSStyles as $CSSStyles_output) {
		
	if ($i&1) {
  		echo '<tr bgcolor="#FFFFFF">';
	} else {
  		echo '<tr bgcolor="#E2F9FC">';	
	}
  
  echo '<td align="center" ><a href="editor.php?content_id='.$CSSStyles_output->id.'&page='.$CSSStyles_output->Provider.'&BusRef='.$CSSStyles_output->id.'"><img src="../images/b_edit.png" alt="edit" width="16" height="16" border="0"></a></td>
	<td align="center"><a href="#" onclick="go_there(\'editor_insert.php?content_id='.$CSSStyles_output->id.'&drop=true&BusRef='.$CSSStyles_output->id.'\')"><img src="../images/b_drop.png" alt="delete" width="16" height="16" border="0"></a></td>
	<font size="2" face="Arial, Helvetica, sans-serif"><font color="#FFFFFF">i</font></font></td>';

	echo '<td align="center">'.stripslashes($CSSStyles_output->CSSStyleName).'</td>';
echo '<td align="center">'.stripslashes($CSSStyles_output->Comments).'</td>';
	

echo  '</tr>';
  
  $i++;
  
  } // foreach  

} // if
  ?>
</table>
</form>
<p>
<form>
	<input class="formbutton" name="addUser" type="button" id="addUser" value="New Entry" onClick="parent.location='editor.php'">
</form>
</p>

</BODY>
</HTML>