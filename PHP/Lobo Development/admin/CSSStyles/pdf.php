<?PHP

	include_once "../all_scripts/admin_functions.php";
	define('FPDF_FONTPATH','../fpdf/font/');
	require('../fpdf/fpdf.php');

	$idUser  = $_SESSION["id"];
	$username = $_SESSION["username"];
	$email    = $_SESSION["email"];
	$patient_id    = $_SESSION["patient_id"];

	// Page selection vars
	$content_id = $_GET["content_id"];
	
	if ($content_id == NULL) {
		$content_id = $_POST["content_id"];
	}
	
	$page		= $_GET["page"]; 
	$mail    = $_POST["mail"];
	$mail_addresses    = $_POST["mail_addresses"];
	
	$therapist_address    = $_POST["therapist_address"];


	include_once "../ez_sql.php";
	
	
	// php 4 special char decoder
	function htmlspecialchars_decode_php4 ($str, $quote_style = ENT_COMPAT) {
   		return strtr($str, array_flip(get_html_translation_table(HTML_SPECIALCHARS, $quote_style)));
	}
	
	function file_extension($filename)
	{
    	$path_info = pathinfo($filename);
    	return $path_info['extension'];
	}



	// Load Content
	if ($content_id != NULL) {
 		$CSSStyles = $db->get_row("SELECT *, UNIX_TIMESTAMP(StartTime) AS FORMATED_TIME_BEGIN FROM CSSStyles WHERE (id = '$content_id')");		
	}
	
	// Load Admin User rights - optional
	$AdminUser = $db->get_row("SELECT * FROM AdminUser WHERE (id = '$idUser')");
	
	// get basic paitent infor for top
	if ($patient_id != NULL) {
 		$PatientProfileContent = $db->get_row("SELECT * FROM PatientProfiles WHERE id = $patient_id");	
		
	}
	
	$Therapist = $db->get_row("SELECT * FROM AdminUser WHERE (id = '$CSSStyles->TherapistRef')");	
	//$db->vardump($Therapist); // dbdebug

	$CaseManager = $db->get_row("SELECT * FROM CaseManager WHERE (id = '$PatientProfileContent->CaseManagerRef')");	
	 


class PDF extends FPDF
{

//Word Wrap - courtesy of http://www.fpdf.de/downloads/addons/49/ 
function WordWrap(&$text, $maxwidth)
{
    $text = trim($text);
    if ($text==='')
        return 0;
    $space = $this->GetStringWidth(' ');
    $lines = explode("\n", $text);
    $text = '';
    $count = 0;

    foreach ($lines as $line)
    {
        $words = preg_split('/ +/', $line);
        $width = 0;

        foreach ($words as $word)
        {
            $wordwidth = $this->GetStringWidth($word);
            if ($width + $wordwidth <= $maxwidth)
            {
                $width += $wordwidth + $space;
                $text .= $word.' ';
            }
            else
            {
                $width = $wordwidth + $space;
                $text = rtrim($text)."\n".$word.' ';
                $count++;
            }
        }
        $text = rtrim($text)."\n";
        $count++;
    }
    $text = rtrim($text);
    return $count;
}

//Page header
function Header()
{
/*    //Logo
    $this->Image('../images/logo.gif',10,8,60);
	//Line break
    $this->Ln(30);
    //Arial bold 15
    $this->SetFont('Arial','B',12);
    //Move to the right
    $this->Cell(80);
    //Title
    $this->Cell(30,10,'CSS Styles',0,0,'C');
    //Line break
    $this->Ln(20); */
}


//Page footer
function Footer()
{
    //Position at 1.5 cm from bottom
    $this->SetY(-15);
    //Arial italic 8
    $this->SetFont('Arial','I',8);
    //Page number
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}
function SetCol($col)
{
    //Set position at a given column
    $this->col=$col;
    $x=10+$col*65;
    $this->SetLeftMargin($x);
    $this->SetX($x);

}

}


//Instanciation of inherited class
$pdf=new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();

// single header
    //Logo
    $pdf->Image('../images/logo.gif',10,8,60);
	//Line break
    $pdf->Ln(30);
    //Arial bold 15
    $pdf->SetFont('Arial','B',12);
    //Move to the right
    $pdf->Cell(80);
    //Title
    $pdf->Cell(30,10,'CSS Styles',0,0,'C');
    //Line break
    $pdf->Ln(10);
	
	
	
// tow column header information	
$pdf->SetCol(0);


/*for($i=1;$i<=40;$i++)
    $pdf->Cell(0,10,'Printing line number '.$i,0,1); */
// Patient info
$pdf->Cell(0,8,stripslashes('Patient Name: '. $PatientProfileContent->LastName. ", ".$PatientProfileContent->FirstName),0,1);

$pdf->Cell(0,8,stripslashes('Address: '. $PatientProfileContent->Address),0,1);

$pdf->Cell(0,8,stripslashes('         '. $PatientProfileContent->CityStateZip),0,1);
$pdf->Cell(0,8,stripslashes("Phone: $PatientProfileContent->phone1"),0,1);
$pdf->Cell(0,8,stripslashes('SSN: '. $PatientProfileContent->SSN),0,1);
$pdf->Cell(0,8,stripslashes('DOB: '. $PatientProfileContent->DOB),0,1);

// second column
$pdf->SetCol(1);
$pdf->SetY(50);



$pdf->SetX(100);
$pdf->Cell(0,8,'Date of Report: '. date("m-d-Y",$CSSStyles->FORMATED_TIME_BEGIN),0,1);
$pdf->SetX(100);
$pdf->Cell(0,8,'Case Manager: '. "$CaseManager->CaseManagerFullName",0,1);
$pdf->SetX(100);
$pdf->Cell(0,8,'CM Company: '. "$CaseManager->CaseManagerAgency",0,1);
$pdf->SetX(100);
$pdf->Cell(0,8,'CM Fax/Contact #: '. "$CaseManager->CaseManagerFax (fax)/$CaseManager->CaseManagerPhone",0,1);
$pdf->SetX(100);
$pdf->Cell(0,8,'Case Manger Company: '. "$CaseManager->CaseManagerFullName",0,1);
$pdf->SetX(100);
$pdf->Cell(0,8,'Therapist: '. "$Therapist->FullName, $Therapist->TherapistType",0,1);
$pdf->SetX(100);
$pdf->Cell(0,8,stripslashes("Therapist #: $Therapist->ContactPhone1"),0,1);
$pdf->SetX(100);
$pdf->Cell(0,8,stripslashes("ICD9: $PatientProfileContent->ICD9"),0,1);

// reset column pointer to the left
$pdf->SetCol(0);

// document font
	$pdf->SetFont('Arial','',12);
	$pdf->Ln(15);

// Form Fields
$pdf->Write(5, strip_tags(htmlspecialchars_decode_php4(strip_tags(stripslashes('CSS Style Name')))));
	$pdf->Ln(8);$pdf->Write(5, strip_tags(htmlspecialchars_decode_php4(strip_tags(stripslashes($CSSStyles->CSSStyleName)))));
	$pdf->Ln(10);
	$pdf->Write(5, strip_tags(htmlspecialchars_decode_php4(strip_tags(stripslashes('Comments')))));
	$pdf->Ln(8);$pdf->Write(5, strip_tags(htmlspecialchars_decode_php4(strip_tags(stripslashes($CSSStyles->Comments)))));
	$pdf->Ln(10);
	$pdf->Write(5, strip_tags(htmlspecialchars_decode_php4(strip_tags(stripslashes('CSS Code')))));
	$pdf->Ln(8);$pdf->Write(5, strip_tags(htmlspecialchars_decode_php4(strip_tags(stripslashes($CSSStyles->CSSCode)))));
	$pdf->Ln(10);
		

$pdf->Ln(8);$pdf->Write(5,'Signed:');
$pdf->Ln(8);$pdf->Write(5,stripslashes("$Therapist->FullName, $Therapist->TherapistType"));
$pdf->Ln(15);

// ouput pdf signature
$pdf->Image("../../UserFiles/signatures/$Therapist->SignatureImage",NULL,NULL,120,50);


$SaveFileName = 'CSSStyles ('.date("m-d-Y",$CSSStyles->FORMATED_TIME_BEGIN).').pdf';

if ($mail == NULL) {
	// download version
	$pdf->Output($SaveFileName ,D);
	
// email it	
} else {

	// create directory 
	chmod('../../UserFiles/tmp', 0777);
	
	// remove file
	if (file_exists('../../UserFiles/tmp/'.$SaveFileName)) { 
		unlink ('../../UserFiles/tmp/'.$SaveFileName);
	}
	
	// create email version
	$pdf->Output('../../UserFiles/tmp/'.$SaveFileName ,F);
	

$file = '../../UserFiles/tmp/'.$SaveFileName;
$date_time = date("l, jS of F Y h:i:s A");
//define the receiver of the email
$to = "$therapist_address,$mail_addresses";
$email = $Therapist->email;
$name = "Creative Therapy Services Website";
//define the subject of the email
$subject = 'CSS Styles';
$comment = "	Date-Time: $date_time (central) \n\n
 			Attached is: $SaveFileName  \n\n".
			stripslashes("Therapist: $Therapist->FullName, $Therapist->TherapistType")."\n\n".
			"Client\n".
			stripslashes('Last Name: '. $PatientProfileContent->LastName).", ".stripslashes('First Name: '. $PatientProfileContent->FirstName)."\n\n".
			stripslashes('SSN: '. $PatientProfileContent->SSN)."\n\n Creative Therapy Service of New Mexico\n(505) 891-3777 | fax (505) 821-7671\nwww.creativetherapynm.com";

$To          = strip_tags($to);
$TextMessage =strip_tags(nl2br($comment),"<br>");
$HTMLMessage =nl2br($comment);
$FromName    =strip_tags($name);
$FromEmail   =strip_tags($email);
$Subject     =strip_tags($subject);

$boundary1   =rand(0,9)."-"
.rand(10000000000,9999999999)."-"
.rand(10000000000,9999999999)."=:"
.rand(10000,99999);
$boundary2   =rand(0,9)."-".rand(10000000000,9999999999)."-"
.rand(10000000000,9999999999)."=:"
.rand(10000,99999);

$attach      ='yes';
$end         ='';
// load single instance into array
$attachment[]=chunk_split(base64_encode(file_get_contents($file)));
$ftype[]       ='pdf';
$fname[]       = $SaveFileName;


/***************************************************************
 Creating Email: Headers, BODY
 1- HTML Email WIthout Attachment!! <<-------- H T M L ---------
 ***************************************************************/
#---->Headers Part
$Headers     =<<<AKAM
From: $FromName <$FromEmail>
Reply-To: $FromEmail
MIME-Version: 1.0
Content-Type: multipart/alternative;
    boundary="$boundary1"
AKAM;

#---->BODY Part
$Body        =<<<AKAM
MIME-Version: 1.0
Content-Type: multipart/alternative;
    boundary="$boundary1"

This is a multi-part message in MIME format.

--$boundary1
Content-Type: text/plain;
    charset="windows-1256"
Content-Transfer-Encoding: quoted-printable

$TextMessage
--$boundary1
Content-Type: text/html;
    charset="windows-1256"
Content-Transfer-Encoding: quoted-printable

$HTMLMessage

--$boundary1--
AKAM;

/***************************************************************
 2- HTML Email WIth Multiple Attachment <<----- Attachment ------
 ***************************************************************/
 
if($attach=='yes') {

$attachments='';
$Headers     =<<<AKAM
From: $FromName <$FromEmail>
Reply-To: $FromEmail
MIME-Version: 1.0
Content-Type: multipart/mixed;
    boundary="$boundary1"
AKAM;

//single file
//for($j=0;$j<count($ftype); $j++){
$attachments.=<<<ATTA
--$boundary1
Content-Type: $ftype[0];
    name="$fname[0]"
Content-Transfer-Encoding: base64
Content-Disposition: attachment;
    filename="$fname[0]"

$attachment[0]

ATTA;
//}

$Body        =<<<AKAM
This is a multi-part message in MIME format.

--$boundary1
Content-Type: multipart/alternative;
    boundary="$boundary2"

--$boundary2
Content-Type: text/plain;
    charset="windows-1256"
Content-Transfer-Encoding: quoted-printable

$TextMessage
--$boundary2
Content-Type: text/html;
    charset="windows-1256"
Content-Transfer-Encoding: quoted-printable

$HTMLMessage

--$boundary2--

$attachments
--$boundary1--
AKAM;
}

/***************************************************************
 Sending Email
 ***************************************************************/
$ok=mail($To, $Subject, $Body, $Headers);
//echo $ok?"<h1> Mail Sent</h1>":"<h1> Mail not SEND</h1>";

//echo $therapist_address." ".$mail_addresses;
header('Location: pageupdater.php');

}

?>