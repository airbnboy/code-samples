<?PHP

// adminpanel.php
	// Session includes
	session_start();
	if($_SESSION["logged"]!=1 or !isset($_SESSION["logged"])) 
		header("Location: index.php?timeout=true"); // redirect to ligin after session expires

include_once "all_scripts/admin_functions.php";
include_once "ez_sql.php";
include_once "admin_settings.php";



// check SESSION vars for idUser --> Paste all this code to the beginning of each application form!
if (!isset($_SESSION["id"]))
{
 header("Location: index.php");
 exit;
}

 // YES idUser --> set user_id & get all data from CONTACT
$idUser  = $_SESSION["id"];
$username = $_SESSION["username"];
$email    = $_SESSION["email"];

$admin_status = $db->get_row("SELECT * FROM AdminUser WHERE username = '$username'");  

$users = $db->get_results("SELECT * FROM AdminUser ORDER BY id");

$settings = $db->get_results("SELECT * FROM Settings ORDER BY id");

?>



<HTML xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns="http://www.w3.org/TR/REC-html40">
<HEAD>
<meta HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=windows-1252">
<meta http-equiv="Content-Language" content="en-us">


<TITLE>Content Manager</TITLE>
<META NAME="robots" CONTENT="noindex,nofollow">



<script type="text/javascript">
<!--
function delete_confirmation(delete_address) {
var answer = confirm("Are you sure you want to delete this entry?")
if (answer){
  window.location = delete_address;
}

}
//-->
</script>
		<SCRIPT language="JavaScript">
		<!--
			function reset_page(address)
			{
				 var delete_it= confirm("This will take your page back to the original state.  Do you really want to reset the page?");
				 if (delete_it== true) {
					window.location=address;
					}
			}
			//-->
</SCRIPT>

<link href="papa_CSS.css" rel="stylesheet" type="text/css" />
</head>
<BODY style="text-align: left" vlink="blue">
<h2><img src="images/logo.gif"></h2>
<table width="100%" border="0" cellpadding="4" cellspacing="0" bgcolor="#EFEFEF">
  <tr>
    <td width="83%"><font color="#FFFFFF" size="2" face="Arial"><a href="adminpanel.php"><strong>Administration</strong></a></font></td>
    <td width="17%" align="right" valign="middle"><a href="homepage.htm"><img src="images/exit.png" width="16" height="16" border="0"></a> 
      <font size="2" face="Arial"><a href="./all_scripts/logout.php"><strong>Sign Out</strong></a></font></td>
  </tr>
</table>
<blockquote>
<?
// Blog Admin
if ($admin_status->blog == "yes")  {
echo '<br><br><b>'.$admin_status->FullName.'\'s Panel</b>';

 echo '<p><font color="#000000" face="Arial, Helvetica, sans-serif"><strong><a href="bio_updater/editor.php"><img src="images/dvi.png" alt="" width="32" height="32" border="0">Bio Editor</a></strong></font></p>';

echo '<font color="#000000" face="Arial, Helvetica, sans-serif"><strong><a href="blogadmin/blog_panel.php"><img src="blogadmin/images/aim.png" alt="" width="32" height="32" border="0">Blog Manager</a></strong></font>';

// don't list the calendar twice if it's the administrator
if (strtolower($admin_status->administrator) != "yes") {
echo '<p><font color="#000000" face="Arial, Helvetica, sans-serif"><strong><a href="calendar/calendar.php"><img src="images/appointment.png" alt="" width="32" height="32" border="0">Schedule/Calendar Manager</a></strong></font></p>';

}


  	// Admin users or site managers only if this account has administrative privlidges
	if (strtolower($admin_status->administrator) != "yes" || strtolower($admin_status->SiteManager) != "yes") {

		echo  '<p align="left"><font color="#000000" face="Arial, Helvetica, sans-serif"><strong><a href="portfolio_updater_expanding/pageupdater.php"><img src="images/camera.png" width="32" height="32" border="0">Photo Manager</a></strong></font></p>';
	
	}

} 	

  	// Admin users or site managers only if this account has administrative privlidges
	if (strtolower($admin_status->administrator) == "yes" || strtolower($admin_status->SiteManager) == "yes") {
echo '<br><Br><br><Br><b>Site Administrator\'s Panel</b>';	

echo '<p align="left"><font color="#000000" face="Arial, Helvetica, sans-serif"><strong><a href="page_updater_image/pageupdater.php?Emergency=yes"><img src="images/status_unknown.png" width="32" height="32" border="0">Emergency/Urgent Alerts</a></strong></font></p>';

 echo '<p><font color="#000000" face="Arial, Helvetica, sans-serif"><strong><a href="bio_updater/pageupdater.php"><img src="images/dvi.png" alt="" width="32" height="32" border="0">Global Bio Manager</a></strong></font></p>';
 
echo '<p><font color="#000000" face="Arial, Helvetica, sans-serif"><strong><a href="blogadmin/blog_panel.php?admin=yes"><img src="images/aim.png" alt="" width="32" height="32" border="0">Global Blog Manager</a></strong></font></p>';

echo '<p><font color="#000000" face="Arial, Helvetica, sans-serif"><strong><a href="calendar/calendar.php"><img src="images/appointment.png" alt="" width="32" height="32" border="0">Schedule/Calendar Manager</a></strong></font></p>';

echo '<p align="left"><font color="#000000" face="Arial, Helvetica, sans-serif"><strong><a href="page_updater_image/pageupdater.php?Emergency=no"><img src="images/xedit.png" width="32" height="32" border="0">Page Updater/Editor</a></strong></font></p>';
		
		 
echo  '<p align="left"><font color="#000000" face="Arial, Helvetica, sans-serif"><strong><a href="portfolio_updater_expanding/pageupdater.php"><img src="images/camera.png" width="32" height="32" border="0"> Portfolio Manager</a></strong></font></p>';
if ($admin_status->blog != "yes")  {
}
  		 

  

}




// _DB_ADMIN_PANEL_SECTIONS_ 

?>
    <?

  	// Admin users only if this account has administrative privlidges
	if (strtolower($admin_status->administrator) == "yes") {
	    echo '<a name="user_admin"></a>';
				
   		// Table Header
  		echo '
  			<!-- start user admin table -->  
  			<p></p>  
  			<p align="left"><font color="#000000" face="Arial, Helvetica, sans-serif"><strong><a href="user_manager/usermanager.php"><img src="./images/yast_sysadmin.png" border="0">Administrator Manager</strong></font></a></p>';
  			


	}  // if (administrator == "Yes") 
	
	
	// special tools
	if (strtolower($username) == "gabe" || strtolower($username) == "tristan") {
		echo '<br><br><bR>';
		
	  echo'<b>TaGG Studios LLC / Apreware LLC use only. This section is not autorized for use by anyone other than TaGG Studios or Apreware Authority without expressed written permission.</b><br>';  	  
	  echo '<p><font color="#000000" face="Arial, Helvetica, sans-serif"><strong><a href="CSSStyles/pageupdater.php"><img src="images/CSSStyles_icon.png" width="32" height="32" border="0"> CSS Styles</a></strong></font></p>';
	  echo '<p><font color="#000000" face="Arial, Helvetica, sans-serif"><strong><a href="Templates/pageupdater.php"><img src="images/Templates_icon.png" width="32" height="32" border="0"> Templates</a></strong></font></p>';
	  echo '<p><font color="#000000" face="Arial, Helvetica, sans-serif"><strong><a href="TaGGDBScaffold/pageupdater.php"><img src="images/configure.png" alt="" width="32" height="32" border="0"> TaGG Scaffolding</a></strong></font></p>'; 
	  echo '<br><br><i>CMS/Template Version: <strong>'.$settings[0]->CMSVersion.'</strong></i><br><br>';
	 }
	 
  ?>
</blockquote>
<p>&nbsp;</p>
<p><b><font size="2" face="Arial">&nbsp; Custom interface provided by TaGG Studios<br>
  &nbsp; <a href="mailto:info@taggstudios.com">info@taggstudios.com</a>&nbsp;&nbsp; 
  505.246.8244</font></b> <br>
  <a href="http://www.taggstudios.com" target="_blank"><img src="images/taggstudios_logo.jpg" alt="TaGG Studios" width="204" height="141" border="0"></a></p>
</BODY>
</HTML>