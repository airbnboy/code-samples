# User Settings
# XML-RPC
username = 'admin' #the user
pwd = 'f@st3r' #the password of the user
dbname = 'GP_Warehouse_v23' #the database

# Direct DB
conn_string = "dbname='GP_Warehouse_v23' user='gabe' password='admin'"

# CSV File
filename = 'OpenERP_Product_Pacific_Final.csv'

# Parent Product Category

# Pacific
#product_category_name = 'All Products - Pacific'
#company_id = '5'

# General Procurement
product_category_name = 'All Products'
company_id = '3'


'''
username = 'admin' #the user
pwd = 'd1ng0' #the password of the user
dbname = 'ConsolidatedGP' #the database

# Direct DB
conn_string = "dbname='ConsolidatedGP' user='openerp' password='Dungeon.9'"

# CSV File
filename = 'Import_141204_Kingston_CustomKingston.csv'

# Pacific
#product_category_name = 'All Products - Pacific'
#company_id = '5'

# General Procurement
product_category_name = 'All Products'
company_id = '3'
'''

# Rapid insert of Products and Product Categories from csv.  This will take the Parent Category.  If Blank it will assume Category id of 1.
# XML-RPC is used for the product categories to ensure the tree structure is properly maintained
# Direct Product insert is for performance.

import csv
import psycopg2
from HTMLParser import HTMLParser
import xmlrpclib

class MLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.fed = []
    def handle_data(self, d):
        self.fed.append(d)
    def get_data(self):
        return ''.join(self.fed)

def addslashes(s):
    #l = ["\\", '"', "'", "\0", ]
    #for i in l:
    #   if i in s:
            #s = s.replace(i, '\\'+i)
    s = s.replace("'", "")
    return s

def strip_tags(html):
    
    s = MLStripper()
    s.feed(html)
    #s = s.replace("'", "")
    return (s.get_data()).replace("'", "")


def clean_string(s):
    return (''.join(c for c in s if c not in ('"', "'"))).rstrip()


def read_file(self):
    with open(self, 'rb') as f:
        data = [row for row in csv.reader(f.read().splitlines())]
    return data



#########################################################################################

# Get the uid

sock_common = xmlrpclib.ServerProxy('http://localhost:8069/xmlrpc/common')
uid = sock_common.login(dbname, username, pwd)

#replace localhost with the address of the server
sock = xmlrpclib.ServerProxy('http://localhost:8069/xmlrpc/object')

# Direct DB Connect
conn = psycopg2.connect(conn_string)
cursor = conn.cursor()

# CSV
reader = csv.reader(open(filename,'rb'))



# Get Parent Category based on Name
try:
    if product_category_name != '':

        statement = 'SELECT "product_category".id FROM "product_category" WHERE name = \''+product_category_name+'\''

        cursor.execute(statement)
        conn.commit()
        parent_category_id = cursor.fetchone()[0]
        
    else:
        parent_category_id = '1' # Use default All Products Category
        print "Using category 1 as default category - All Products"
except:    
        print "Make sure the parent category you have listed in this scripts configuration using the product_category_name variable.  Create the parent category in the system or use existing category \n\n\n"
    
        

print "\n\n\nParent Category: " + product_category_name + " > id: " + str(parent_category_id)
        
       
previous_product_category = ''  
category_id = parent_category_id

i = 0      
for row in reader:
    #if i == 10:
    #    break
    
    try:
#        statement = "SELECT product_product.default_code FROM product_product,product_template WHERE product_product.default_code = '"+clean_string(row[0])+"' AND product_template.company_id = '"+company_id+"'"
        statement = "SELECT product_product.default_code FROM product_product INNER JOIN product_template ON product_template.id = product_product.product_tmpl_id  WHERE product_product.default_code = '"+clean_string(row[0])+"' AND product_template.company_id = '"+company_id+"'"

        cursor.execute(statement)                                                      
        conn.commit()                                                                      
        print "code " + str(cursor.fetchone()[0]) 
        if cursor.fetchone() != 'None':
            #reader.next()
            i = i + 1
            continue
                
    except:
        #print "broken"
        pass
   
    try:
        #Check if product category exists on a different prodcut category per csv line
        if previous_product_category != row[1]:
            rows_count = 0
            statement = 'SELECT "product_category".id FROM "product_category" WHERE parent_id = \''+str(parent_category_id)+'\' AND name = \''+clean_string(row[1])+'\''
            cursor.execute(statement)
            conn.commit()
            fetch = cursor.fetchone()
            
            if str(fetch) != 'None':
                category_id = str(fetch[0])
    
            else:
        
                # Create new Prodcut Category
                product_category = {
                        'name': clean_string(row[1]),
                        'parent_id':parent_category_id,
                }

                category_id = sock.execute(dbname, uid, pwd, 'product.category', 'create', product_category)
                previous_product_category = category_id
                print "\n\nNew Category: " + row[1] + " > id: " + str(category_id)  
                print "Importing Products"              
    except:
        pass


    # Insert Products and Product Categories
    try:        
 
        # adjust name
        name = ''
        if (clean_string(row[4])): 
            name = clean_string(row[4])
        else:
            name = clean_string(row[5])
            
        print "row " + name
            
        statement = "INSERT INTO product_template (create_uid,create_date,write_date,write_uid,name,standard_price,list_price,mes_type,uom_id,uom_po_id," \
    "type,procure_method,cost_method,categ_id,supply_method,sale_ok,purchase_ok, company_id,description" \
    ",warranty,weight,weight_net, volume, product_manager, state,produce_delay,rental,sale_delay" \
    ") VALUES (1,(now() at time zone 'UTC'),(now() at time zone 'UTC'),1,'" + name + \
    "','1.000000','1.000000','fixed',1,1,'product','make_to_order','standard',"+str(category_id)+",'buy',True,True,'"+company_id+"','"+clean_string(strip_tags(row[5]))+"'"\
    ",0,'0.000000', '0.000000',0,1,'sellable',1,'False',7) RETURNING id"

        cursor.execute(statement)
        conn.commit()
        templateid = cursor.fetchone()[0]

        statement = "INSERT INTO product_product (create_date,write_date,write_uid,track_production, track_incoming, track_outgoing,price_margin, create_uid, color, price_extra, product_tmpl_id,default_code,active,valuation,x_web_description,x_specifications,x_upc) VALUES \
    ((now() at time zone 'UTC'),(now() at time zone 'UTC'),1,'False','False','False','1.000000',1,0,'0.000000'," + str(templateid) + ",'" + clean_string(row[0]) + "',True,'manual_periodic','"+clean_string((row[6]))+"','"+clean_string((row[7]))+"','" +"')"
        cursor.execute(statement)
        conn.commit()
        #print "in"
        
    except:
        print "Failed: " + row[0]
        pass
    i = i + 1
   

print "complete import of " + str(i) + " Products"       
