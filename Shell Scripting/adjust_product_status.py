

# Direct DB
conn_string = "dbname='GP_Warehouse_v22' user='gabe' password='admin'"

# CSV File
#filename = 'Pacific_Price.csv'




# Rapid update of product prices.  Direct DB Query built for performance.
import csv
import psycopg2
from HTMLParser import HTMLParser
import xmlrpclib

class MLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.fed = []
    def handle_data(self, d):
        self.fed.append(d)
    def get_data(self):
        return ''.join(self.fed)

def addslashes(s):
    #l = ["\\", '"', "'", "\0", ]
    #for i in l:
    #   if i in s:
            #s = s.replace(i, '\\'+i)
    s = s.replace("'", "")
    return s

def strip_tags(html):
    
    s = MLStripper()
    s.feed(html)
    #s = s.replace("'", "")
    return (s.get_data()).replace("'", "")


def clean_string(s):
    return (''.join(c for c in s if c not in ('"', "'"))).rstrip()

def read_file(self):
    with open(self, 'rb') as f:
        data = [row for row in csv.reader(f.read().splitlines())]
    return data



#########################################################################################
                                                                                       
                                                                                       
                                                                                       
# Direct DB Connect                                                                    
conn = psycopg2.connect(conn_string)                                                   
cursor = conn.cursor()                                                                 
                                                                                       
# CSV                                                                                  
#reader = csv.reader(open(filename,'rb'))                                               
                                                                                       
print "Updating Products - Disabling sales of products that have not cost"                                                             
                                                                                       
                                                                     
                                                                                       
# Update Product Prices                                                            
                                                                                
statement = "UPDATE product_template SET \"sale_ok\" = 'False'  WHERE product_template.standard_price = '0.000000' OR product_template.standard_price = '1.000000'"
cursor.execute(statement)                                                      
conn.commit()                                                                  
template_id = str(cursor.fetchone())                                        
print template_id
                                                                      
print  "Product Update Complete"

