

# Direct DB
conn_string = "dbname='GP_Warehouse_v18' user='gabe' password='admin'"

# CSV File
filename = 'Pacific_Price.csv'




# Rapid update of product prices.  Direct DB Query built for performance.
import csv
import psycopg2
from HTMLParser import HTMLParser
import xmlrpclib

class MLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.fed = []
    def handle_data(self, d):
        self.fed.append(d)
    def get_data(self):
        return ''.join(self.fed)

def addslashes(s):
    #l = ["\\", '"', "'", "\0", ]
    #for i in l:
    #   if i in s:
            #s = s.replace(i, '\\'+i)
    s = s.replace("'", "")
    return s

def strip_tags(html):
    
    s = MLStripper()
    s.feed(html)
    #s = s.replace("'", "")
    return (s.get_data()).replace("'", "")


def clean_string(s):
    return (''.join(c for c in s if c not in ('"', "'"))).rstrip()

def read_file(self):
    with open(self, 'rb') as f:
        data = [row for row in csv.reader(f.read().splitlines())]
    return data



#########################################################################################
                                                                                       
                                                                                       
                                                                                       
# Direct DB Connect                                                                    
conn = psycopg2.connect(conn_string)                                                   
cursor = conn.cursor()                                                                 
                                                                                       
# CSV                                                                                  
reader = csv.reader(open(filename,'rb'))                                               
                                                                                       
print "Updating Pricelist"                                                             
                                                                                       
i = 0                                                                                  
g = 0                                                                                  
f = 0                                                                                  
print "Importing ..."                                                                  
for row in reader:                                                                     
    if i == 1000:                                                                     
        break                                                                         
                                                                                       
    # Update Product Prices                                                            
    try:                                                                               
                                                                                       
        statement = "SELECT product_product.product_tmpl_id FROM product_product WHERE product_product.default_code = '"+clean_string(row[0])+"'"
        cursor.execute(statement)                                                      
        conn.commit()                                                                  
        template_id = str(cursor.fetchone()[0])                                        
        g = g + 1                                                                      
        print "template_id " + template_id + ":   " + str(g)                          
                                                                                       
        statement = "UPDATE product_template SET standard_price = '"+clean_string(row[1])+"'::numeric WHERE id = '"+template_id+"'::integer" # , list_price = '"+clean_string(row[2])+"'::numeric
        cursor.execute(statement)                                                      
        conn.commit()                                                                  

    except:
        f = f + 1                                                                      
        #print "Failed: " + row[0]                                                     
        pass                                                                           
    i = i + 1                                                                          


print ""
print "Total Product Prices: "  + str(i)                                               
print "Found and Updated: " + str(g)                                                   
print "Failed: " + str(f) 