# -*- coding: utf-8 -*-
##############################################################################
# Module by Kranbery Techonolgies LLC
##############################################################################

from openerp.osv import osv, orm, fields
from openerp.tools.translate import _
#from openerp.tools import html_email_clean
from datetime import *
from random import shuffle
#from tools.translate import _
import threading
from openerp import SUPERUSER_ID
from openerp import tools
from openerp.tools.safe_eval import safe_eval as eval
from openerp.tools.translate import _




# inherited
class sale_order(osv.osv):
    _inherit = 'sale.order'
sale_order()

class account_account(osv.osv):
    _inherit = 'account.account'
account_account()


class customer_service(osv.osv):
    _name = 'customer_service.customer_service'
 
    _columns = {
        'customer_svc_name': fields.char('Customer Service Name', size=256, help='The type of customer i.e. ortodontist, denitst, practicioner, etc'),
        'customer_svc_date': fields.char('Date', size=256, help='The type of customer i.e. ortodontist, denitst, practicioner, etc'),
        'customer_svc_place': fields.char('Place', size=256, help='The type of customer i.e. ortodontist, denitst, practicioner, etc'),
        }

customer_service()

# configuration items
class customer_service_ticket(osv.osv):
    """ Customer Ticket """
    _name = 'customer_service.ticket'
    _rec_name = 'ticket_number'   
    _inherit = ['mail.thread','ir.needaction_mixin']





    def create(self, cr, uid, vals, context=None):

        # determine if single instance item and set state accordingly
        current_ticket_ref = vals['ticket_type']
        ticket_info = self.pool.get('customer_service.service_type').browse(cr, uid, current_ticket_ref, context=context) 
        
        # Single instance ticket       
        if ticket_info.single_instance_item:
            ticket_state = 'precomplete'
            workflow_state = 'Ticket Created'
            next_workflow_task = "Press 'Complete Ticket' button to finish and close"
            vals['workflow_state'] = workflow_state
            vals['next_workflow_task'] = next_workflow_task             
            completed_ticket = True
        
        # Automatically handoff if not single instance and get first workflow needs to be followed      
        else:
            ticket_state = 'active'                                       
            completed_ticket = False
        
            
        # Save Ticket States
        vals['state'] = ticket_state  
        
        # Set context for first handoff             
        context['current_state'] = ticket_state            
        context['ticket_type'] = vals['ticket_type']#ticket_info.ticket_type.id 
        context['ticket_workflow_position_id'] = 'start'
        context['workflow_group_state'] = 'start'        
        context['new_ticket_customer_id'] = vals['cs_partner_id']       
        
        #intiate first ticket handoff 
        if not completed_ticket:
            self.handoff_ticket(cr, uid, [], context=context)             
            workflow_state = context['workflow_state'] 
            next_workflow_task = context['next_workflow_task']
            vals['workflow_state'] = workflow_state
            vals['next_workflow_task'] = next_workflow_task 
            # vals['workflow_group_state'] = 'working'


        # create initial call log entry
        # customer_service_call_log.create(cr, uid, vals, context=context)
        #self.output_ticket_workflow(cr, uid, [], ticket_info.ticket_type.id, ticket_info.workflow_group_state, ticket_info.workflow_group_state, context)
        #res = self._popup_customer_service_call_log(cr, uid, True, context=context)
        #self.write(cr, uid, [], res, context=context)  


        # Save ticket
        ticket_ref = super(customer_service_ticket, self).create(cr, uid, vals, context=context)      
        return ticket_ref


    def shipping_address_info(self, cr, uid, ids, partner_id, context=None):

        # Get partner information - search for shipping first, if none, then do default adderss.
        if partner_id:
            partner = self.pool.get('res.partner').browse(cr, uid, partner_id, context=context)  
            address_type_label = 'Default Address: '
            # get shipping address first
            if partner.child_ids:
                for child_contact in partner.child_ids:
                    if child_contact.type == 'delivery':
                        partner = child_contact
                        address_type_label = 'Shipping Address: '
                                
            # Strip Shipping reference if any in the name
            customer_name = partner.name
            customer_name = customer_name.replace("/ Shipping","");

            
            # Build Shipping Address Clipboard
            shipping_address_clipboard_info = address_type_label +'\r\n' + ' ' + '\r\n' 
            if partner.name:
                shipping_address_clipboard_info += customer_name +'\r\n'             
            if partner.street:     
                shipping_address_clipboard_info += partner.street+'\r\n' 
            if partner.street2:                            
                shipping_address_clipboard_info += partner.street2+'\r\n'                
            if partner.city:    
                shipping_address_clipboard_info += partner.city
            #state_name = self.pool.get('res.country.state').browse(cr, uid, , context=context) 
            if partner.state_id.name:
                shipping_address_clipboard_info += ', ' + partner.state_id.name + ' '                
            if partner.zip:    
                shipping_address_clipboard_info += partner.zip + '\r\n'
            if partner.country_id.name:
                if partner.country_id.name != 'United States':
                    shipping_address_clipboard_info +=  partner.country_id.name                            
            return  '\r\n' + shipping_address_clipboard_info
 
            
               
    # Column declarations
    _columns = {
                
        # Ticket states        
        'state': fields.selection([
            ('modify', 'Modify Ticket'),
            ('draft', 'Create Ticket'),
            ('active', 'Active Ticket'),            
            ('precomplete', 'Pre-Completion'),
            ('cancel', 'Cancelled'),
            ('done', 'Complete')],
            'Ticket State', readonly=True, required=True,
            track_visibility='onchange',
            help='Customer service ticket flow/process.'),
                
        #Traccks init of group state        
        'workflow_group_state': fields.selection([
            ('start', 'start'),
            ('working', 'working'),
            ('finished', 'finished')],
            'Workflow Group State', readonly=True, required=True,
            track_visibility='onchange',
            help='Workflow_group_state. Internal state tracking enumeration'),                
                                            
        'workflow_state': fields.char('Workflow Status', size=512,required=False, help="Workflow status indicator", readonly=True),  
        'next_workflow_task': fields.char('Next Workflow Task', size=512,required=False, help="Next workflow task", readonly=True),  
        
        'customer_active_tickets_warning': fields.char('Warnings', size=256,required=False, help="Indicates that customer has other active tickets", readonly=True),                
                                                                                          
        # Customer Info                  
        'cs_partner_id':  fields.many2one('res.partner', 'Customer Name', required=True, domain="[('customer', '=', True),('is_company', '=', True)]"),
        'category_ids': fields.char('Customer Type Tags', size=128,required=False, help="Customer Type Tags", readonly=True),         
        'image_medium': fields.related('cs_partner_id','image',type='binary',string='Image', readonly=True),
        
        # 'CustomerType': fields.related('cs_partner_id','CustomerType',type='char',string='Customer Type'),      
        'title': fields.related('cs_partner_id','title',type='char',string='Title', readonly=True),     
                
        # Address info
        'street': fields.related('cs_partner_id','street',type='char',string='Street', readonly=True),
        'street2': fields.related('cs_partner_id','street2',type='char',string='Street2', readonly=True),
        'city': fields.related('cs_partner_id','city',type='char',string='City', readonly=True),  
        'state_id': fields.related('cs_partner_id','state_id',type='many2one', relation="res.country.state", string='State', readonly=True),
        'zip': fields.related('cs_partner_id','zip',type='char',string='Zip', readonly=True),
        'country_id': fields.related('cs_partner_id', 'country_id',
                    type='many2one', relation='res.country', string='Country', readonly=True, states={'done': [('readonly', True)]}),
                
        'shipping_address_clipboard': fields.text('Shipping Address', readonly=True, required=False, help='Customer contact information'),    
        #'shipping_address_clipboard': fields.function(shipping_address_info, type='text', fnct_search=None, obj=None, method=False, store=False, multi=False),            
        
        # contact info
        'phone': fields.related('cs_partner_id','phone',type='char',string='Phone', readonly=True),
        'mobile': fields.related('cs_partner_id','mobile',type='char',string='Mobile', readonly=True),
        'fax': fields.related('cs_partner_id','fax',type='char',string='Fax', readonly=True),     
        'website': fields.related('cs_partner_id','website',type='char',string='Website', readonly=True),   
        'email': fields.related('cs_partner_id','email',type='char',string='Email', readonly=True),  
        'title': fields.related('cs_partner_id','title',type='char',string='Title', readonly=True), 
            
        # Ticket info        
        'ticket_date': fields.datetime('Start Date', help="Date of ticket issue", readonly=True),
        'service_category': fields.many2one('customer_service.service_category', 'Category', help="Ticket Service Category", readonly=False, required=True),  
        #'ticket_type': fields.related('service_category','Service Type',type='many2one',string='Title', readonly=True), , domain="[('service_category_id','=',service_category)]"
        'ticket_type': fields.many2one('customer_service.service_type', 'Type', help="Status of the ticket", domain="[('service_category','=',service_category)]",  readonly=False, required=True),

        
        'current_group': fields.many2one('customer_service.group', 'Current Group', help="Currnet group in the workflow queue", readonly=True, required=False),
        'ticket_workflow_position_id': fields.integer('Workflow Group', required=False, readonly=True, help="Priority of customer service groups"), #many2one('customer_service.groups', 'Current Group in Workflow queue id', help="Currnet group in the workflow queue", readonly=False, required=False),  
        'cs_ticket_owner': fields.many2one('res.users', 'Ticket Owner', help="Owner of the ticket", readonly=False),
        #'cs_ticket_owner_vis': fields.boolean('',required=False),
        'ticket_status': fields.selection([('normal', 'Normal'),('urgent', 'Urgent'),('pending', 'Pending')],
            'Status', readonly=True, required=True,
            track_visibility='onchange',
            help='Status and priority of ticket'), 
                
        #many2one('customer_service.status_type', 'Ticket Status', help="Status of the ticket", required=True),
        'ticket_number': fields.char('Ticket #',size=256,required=True, help="Number of customer service ticket", readonly=True),
        #'ticket_priority': fields.selection([('priority_normal', 'Normal'), ('priority_high', 'High'), ('urgent', 'Urgent')], 'Priority', required=True, select=True, help="Ticket Status"),
        
        # Intial contact log info
        'c_log_type_initial_note': fields.many2one('customer_service_call_log.customer_service_contact_type', 'Customer Contact Type', readonly=False, required=True),
        'c_log_initial_note': fields.text('Service Issue', readonly=False, required=True, help='Customer contact information'),
    
        'c_special_note': fields.text('Special Note', readonly=False, required=False, help='Customer contact information'),


    }
    
    _defaults = {
        
        'state': 'draft',
        'ticket_status':'normal',
        'workflow_group_state': 'start',
        'ticket_date': lambda *a: datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
        'ticket_number': lambda self,cr,uid,context={}: self.pool.get('ir.sequence').get(cr, uid, 'customer_service.ticket'),  # : lambda obj, cr, uid, context: obj.pool.get('customer_service.ticket').seq_get(cr, uid),
        'cs_ticket_owner': lambda s, cr, u, c: u,  # current user 
        'ticket_workflow_position_id' : 1,
        'c_log_type_initial_note': 1,
    }
    
    _order = "state, ticket_status desc, ticket_date desc"
    
    

    # helper
    def return_to_ticket_tree_view(self, cr, uid, ids, context):
        return {
            'name': 'My Tickets',    
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'customer_service.ticket',
            'type': 'ir.actions.act_window',
            'target': 'self',
            'context': "{'search_default_active_ticket':'active', 'search_default_active_ticket':'precomplete'}",
            'domain': "[('cs_ticket_owner','=',uid), ('state','!=','done'), ('state','!=','cancel'), ('ticket_status','!=','pending')]",
            #'search_view_id':'view_cs_ticket_filter',
            'nodestroy': True,                 
            }
        
    def return_to_ticket_create_view(self, cr, uid, ids, context):
        return {
                'id': 'customer_service_tickets',
                'model': 'ir.actions.act_window',
                'name': 'My Tickets',
                'view_id': 'customer_service_ticket_tree',
                'view_type': 'form',
                'view_mode': 'tree,form',         
                'domain': "[('cs_ticket_owner','=',uid), ('state','!=','done'), ('state','!=','cancel')]"                   
                }

    #Inherited
    def message_post(self, cr, uid, thread_id, body='', subject=None, type='notification',
                        subtype=None, parent_id=False, attachments=None, context=None,
                        content_subtype='html', **kwargs):
        """ Post a new message in an existing thread, returning the new
            mail.message ID.

            :param int thread_id: thread ID to post into, or list with one ID;
                if False/0, mail.message model will also be set as False
            :param str body: body of the message, usually raw HTML that will
                be sanitized
            :param str type: see mail_message.type field
            :param str content_subtype:: if plaintext: convert body into html
            :param int parent_id: handle reply to a previous message by adding the
                parent partners to the message in case of private discussion
            :param tuple(str,str) attachments or list id: list of attachment tuples in the form
                ``(name,content)``, where content is NOT base64 encoded

            Extra keyword arguments will be used as default column values for the
            new mail.message record. Special cases:
                - attachment_ids: supposed not attached to any document; attach them
                    to the related document. Should only be set by Chatter.
            :return int: ID of newly created mail.message
        """

        
        if context is None:
            context = {}
        if attachments is None:
            attachments = {}
        mail_message = self.pool.get('mail.message')
        ir_attachment = self.pool.get('ir.attachment')

        assert (not thread_id) or \
                isinstance(thread_id, (int, long)) or \
                (isinstance(thread_id, (list, tuple)) and len(thread_id) == 1), \
                "Invalid thread_id; should be 0, False, an ID or a list with one ID"
        if isinstance(thread_id, (list, tuple)):
            thread_id = thread_id[0]

        # if we're processing a message directly coming from the gateway, the destination model was
        # set in the context.
        model = False
        if thread_id:
            model = context.get('thread_model', self._name) if self._name == 'mail.thread' else self._name
            if model != self._name:
                del context['thread_model']
                return self.pool.get(model).message_post(cr, uid, ids, thread_id, body=body, subject=subject, type=type, subtype=subtype, parent_id=parent_id, attachments=attachments, context=context, content_subtype=content_subtype, **kwargs)

        # 0: Parse email-from, try to find a better author_id based on document's followers for incoming emails
        email_from = kwargs.get('email_from')
        if email_from and thread_id and type == 'email' and kwargs.get('author_id'):
            email_list = tools.email_split(email_from)
            doc = self.browse(cr, uid, thread_id, context=context)
            if email_list and doc:
                author_ids = self.pool.get('res.partner').search(cr, uid, [
                                        ('email', 'ilike', email_list[0]),
                                        ('id', 'in', [f.id for f in doc.message_follower_ids])
                                    ], limit=1, context=context)
                if author_ids:
                    kwargs['author_id'] = author_ids[0]
        author_id = kwargs.get('author_id')
        if author_id is None:  # keep False values
            author_id = self.pool.get('mail.message')._get_default_author(cr, uid, context=context)

        # 1: Handle content subtype: if plaintext, converto into HTML
        if content_subtype == 'plaintext':
            body = tools.plaintext2html(body)

        # 2: Private message: add recipients (recipients and author of parent message) - current author
        #   + legacy-code management (! we manage only 4 and 6 commands)
        partner_ids = set()
        kwargs_partner_ids = kwargs.pop('partner_ids', [])
        for partner_id in kwargs_partner_ids:
            if isinstance(partner_id, (list, tuple)) and partner_id[0] == 4 and len(partner_id) == 2:
                partner_ids.add(partner_id[1])
            if isinstance(partner_id, (list, tuple)) and partner_id[0] == 6 and len(partner_id) == 3:
                partner_ids |= set(partner_id[2])
            elif isinstance(partner_id, (int, long)):
                partner_ids.add(partner_id)
            else:
                pass  # we do not manage anything else
        if parent_id and not model:
            parent_message = mail_message.browse(cr, uid, parent_id, context=context)
            private_followers = set([partner.id for partner in parent_message.partner_ids])
            if parent_message.author_id:
                private_followers.add(parent_message.author_id.id)
            private_followers -= set([author_id])
            partner_ids |= private_followers

        # 3. Attachments
        #   - HACK TDE FIXME: Chatter: attachments linked to the document (not done JS-side), load the message
        attachment_ids = kwargs.pop('attachment_ids', []) or []  # because we could receive None (some old code sends None)
        if attachment_ids:
            filtered_attachment_ids = ir_attachment.search(cr, SUPERUSER_ID, [
                ('res_model', '=', 'mail.compose.message'),
                ('create_uid', '=', uid),
                ('id', 'in', attachment_ids)], context=context)
            if filtered_attachment_ids:
                ir_attachment.write(cr, SUPERUSER_ID, filtered_attachment_ids, {'res_model': model, 'res_id': thread_id}, context=context)
        attachment_ids = [(4, id) for id in attachment_ids]
        # Handle attachments parameter, that is a dictionary of attachments
        for name, content in attachments:
            if isinstance(content, unicode):
                content = content.encode('utf-8')
            data_attach = {
                'name': name,
                'datas': base64.b64encode(str(content)),
                'datas_fname': name,
                'description': name,
                'res_model': model,
                'res_id': thread_id,
            }
            attachment_ids.append((0, 0, data_attach))

        # 4: mail.message.subtype
        subtype_id = False
        if subtype:
            if '.' not in subtype:
                subtype = 'mail.%s' % subtype
            ref = self.pool.get('ir.model.data').get_object_reference(cr, uid, *subtype.split('.'))
            subtype_id = ref and ref[1] or False

        # automatically subscribe recipients if asked to
        if context.get('mail_post_autofollow') and thread_id and partner_ids:
            partner_to_subscribe = partner_ids
            if context.get('mail_post_autofollow_partner_ids'):
                partner_to_subscribe = filter(lambda item: item in context.get('mail_post_autofollow_partner_ids'), partner_ids)
            self.message_subscribe(cr, uid, [thread_id], list(partner_to_subscribe), context=context)

        # _mail_flat_thread: automatically set free messages to the first posted message
        if self._mail_flat_thread and not parent_id and thread_id:
            message_ids = mail_message.search(cr, uid, ['&', ('res_id', '=', thread_id), ('model', '=', model)], context=context, order="id ASC", limit=1)
            parent_id = message_ids and message_ids[0] or False
        # we want to set a parent: force to set the parent_id to the oldest ancestor, to avoid having more than 1 level of thread
        elif parent_id:
            message_ids = mail_message.search(cr, SUPERUSER_ID, [('id', '=', parent_id), ('parent_id', '!=', False)], context=context)
            # avoid loops when finding ancestors
            processed_list = []
            if message_ids:
                message = mail_message.browse(cr, SUPERUSER_ID, message_ids[0], context=context)
                while (message.parent_id and message.parent_id.id not in processed_list):
                    processed_list.append(message.parent_id.id)
                    message = message.parent_id
                parent_id = message.id


        # Modify message thread to include partner
        subject = subject or False
        
        # Add Partner Name to the subject
        item = self.browse(cr, uid, thread_id)
        cs_partner_id = item.cs_partner_id        
        if subject:

            subject = subject +' - '  + cs_partner_id.name
        else:
            subject = cs_partner_id.name
            
            
            
        values = kwargs
        values.update({
            'author_id': author_id,
            'model': model,
            'res_id': thread_id or False,
            'body': body,
            'subject': subject,
            'type': type,
            'parent_id': parent_id,
            'attachment_ids': attachment_ids,
            'subtype_id': subtype_id,
            'partner_ids': [(4, pid) for pid in partner_ids],
        })

        # Avoid warnings about non-existing fields
        for x in ('from', 'to', 'cc'):
            values.pop(x, None)

        # Create and auto subscribe the author
        msg_id = mail_message.create(cr, uid, values, context=context)
        message = mail_message.browse(cr, uid, msg_id, context=context)
        if message.author_id and thread_id and type != 'notification' and not context.get('mail_create_nosubscribe'):
            self.message_subscribe(cr, uid, [thread_id], [message.author_id.id], context=context)
        return msg_id
    
    
    
    # Initiate Tickets to count to show next to menu
    def _needaction_domain_get(self, cr, uid, context=None):            
        return [('cs_ticket_owner','=',uid), ('state','!=','done'), ('state','!=','cancel'), ('ticket_status','!=','pending')]        
        
        
   # def _needaction_domain_get(self, cr, uid, context=None):
   #     return [('cs_ticket_owner','=',uid), ('state','!=','done'), ('state','!=','cancel'), ('ticket_status','=','pending')]    #('to_read', '=', True)
       
    def get_initial_ticket_workgourp_id(self, cr, uid, ids, context):
        
         # Output current group owner in Current Group Field                      
        ticket_type = context.get('ticket_type', False)
        ticket_workflow_position_id = context.get('ticket_workflow_position_id', False)
        customer_service_type = self.pool.get('customer_service.service_type').browse(cr, uid, ticket_type, context)
        if customer_service_type:
            res = customer_service_type.assigned_workflow.group_ids[0].id
        else:
            raise osv.except_osv(_('Attention'),_('No "Service Types" have been set... talk to an administrator to have that set in the Customer Service Configuration'))
        return res    
    
    
    def get_and_set_next_ticket_workgourp_id(self, cr, uid, ids, context):
                                                
        ticket_type = context.get('ticket_type', False)
        workflow_group_state = context.get('workflow_group_state', False)
        ticket_workflow_position_id = context.get('ticket_workflow_position_id', False)
                
                
        # Get Workflow position ID
        customer_service_type = self.pool.get('customer_service.service_type').browse(cr, uid, ticket_type, context)
        if customer_service_type:             
                                        
            # Check if this is first ticket
            if (workflow_group_state == 'start'):                
                next_workflow_id = customer_service_type.assigned_workflow.group_ids[0].id
                self.write(cr, uid, ids, {'workflow_group_state': 'working'}, context=context)    
                self.write(cr, uid, ids, {'ticket_workflow_position_id': next_workflow_id}, context=context)
                if (len(customer_service_type.assigned_workflow.group_ids) == 1):
                        self.write(cr, uid, ids, {'state': 'precomplete'}, context=context)                            
                            
                # Return workflow ID
                return next_workflow_id
            
            # Otherwise dig for next work group
            else:
                # mark the state as 'working' - sending the ticket through the workflow
                #self.write(cr, uid, ids, {'workflow_group_state': 'working'}, context=context)
                                                                    
                # Get counts
                i = 0
                group_counter = 1
                group_count = len(customer_service_type.assigned_workflow.group_ids)
                
                # Search the tickets
                for assigned_workflow_i in customer_service_type.assigned_workflow.group_ids:    
                                        
                    # set state to pre-complete if last position in workflow queue(which hides handoff button and shows complete ticket)
                    if group_counter >= (group_count-1):
                        self.write(cr, uid, ids, {'state': 'precomplete'}, context=context)
                    
                    
                    # found workflow id in the list                              
                    if (assigned_workflow_i.id == ticket_workflow_position_id):
                        
                        #check next
                        if group_counter < group_count:
                            next_workflow_id = customer_service_type.assigned_workflow.group_ids[i+1].id 
                            
                            self.write(cr, uid, ids, {'workflow_group_state': 'working'}, context=context)    
                            self.write(cr, uid, ids, {'ticket_workflow_position_id': next_workflow_id}, context=context)                            
                            
                            # Return workflow ID
                            return next_workflow_id
                        
                            
                        

                            
                    i += 1  # increment the count
                    group_counter += 1  # increment the count
                
            # Set workflowid
            # mark the state as 'working' - sending the ticket through the workflow
            return False
                         
            
        
        # No Customer service type record exists or selected    
        else: 
            raise osv.except_osv(_('Attention'),_('No "Service Types" have been set... talk to an administrator to have that set in the Customer Service Configuration'))
            return False
                
    def has_active_tickets(self, cr, uid, ids, partner_id, record_exists, context):           
        
         # Search for ticket type and that it's a single instance item
         cs_ticket_obj = self.pool.get('customer_service.ticket') #user_id_i.name.id                        
         ticket_count = cs_ticket_obj.search(cr, uid, [('cs_partner_id', '=', partner_id),('state', '=', ['active','precomplete'])], count = True)

         # ticket is a single instance type -> ticket_user_count should equal 1 record returned
         # If record exists expected count is 1 else it's 0         
         if record_exists:
             expected_ticket_count = 1
         else:
            expected_ticket_count = 0
         
         # Notify of duplicates
         if ticket_count > expected_ticket_count:
             return True
         else:
             return False            
                    
    def is_single_instance_ticket(self, cr, uid, ids, context):
         current_ticket_ref = context.get('current_ticket_ref', False)
         
         # Search for ticket type and that it's a single instance item
         #cs_ticket_obj = self.pool.get('customer_service.ticket') #user_id_i.name.id                        
         #ticket_user_count = cs_ticket_obj.search(cr, uid, [('id', '=', current_ticket_ref),('ticket_type', '=', ticket_type), ('ticket_type.single_instance_item', '=', True)], count = True)
         
         ticket_info = self.pool.get('customer_service.ticket').browse(cr, uid, current_ticket_ref, context=context)
         is_single_instance =  ticket_info.ticket_type.single_instance_item

         return is_single_instance
         # ticket is a single instance type -> ticket_user_count should equal 1 record returned
         #if ticket_user_count > 0:
         #    return True
         #else:
         #    return False
            
            
            
    def output_ticket_workflow(self, cr, uid, ids, ticket_type, ticket_workflow_position_id, current_state, context):
                      
        # Output current group owner in Current Group Field
        self.output_ticket_current_group_owner(cr, uid, ids, ticket_type, ticket_workflow_position_id, current_state, context)                       
        
        # Inits/defaults
        if current_state == 'start':  
            current_workflow_state = '> '
        else :
             current_workflow_state = ''     
        current_workflow_state += 'Start - '    
        
        
        
        # Get list of users in the current group and get the user with the least active tickets in their queue
        customer_service_type = self.pool.get('customer_service.service_type').browse(cr, uid, ticket_type, context)
        
        if current_state == 'start':
                if customer_service_type.first_service_task:
                    current_workflow_task = customer_service_type.first_service_task
                else:
                    current_workflow_task = ''
        
        # Biuld workflow
        cs_type_list_size = len(customer_service_type.assigned_workflow.group_ids)  #self.pool.get('customer_service.service_type').search(cr, uid, [], context, count = True)
        i = 1
        
        if customer_service_type:     
            for assigned_workflow_i in customer_service_type.assigned_workflow.group_ids:
                
                # Mark the current department process and show current group
                if assigned_workflow_i.id == ticket_workflow_position_id and current_state != 'start':          
                    
                    # Output Next Workflow Task Information
                    if assigned_workflow_i.service_task != False:                       
                        current_workflow_task = assigned_workflow_i.service_task # Next Task
                    else:
                        current_workflow_task = ""
                                         
                                         
                    # formatting for singleton of department
                    if (cs_type_list_size == i):            
                        current_workflow_state += " > "+assigned_workflow_i.group.group_name
                    else: 
                        current_workflow_state += " > "+assigned_workflow_i.group.group_name + " -  "                    
                        
                else:
                    
                    # Format for last of department
                    if (cs_type_list_size == i):   
                        current_workflow_state += assigned_workflow_i.group.group_name
                    else: 
                        current_workflow_state += assigned_workflow_i.group.group_name + " - "
                i+=1 # increment counter
        
                # current_workflow_state = current_workflow_state + " - Complete"
        self.write(cr, uid, ids, {'workflow_state': current_workflow_state, 'next_workflow_task': current_workflow_task}, context=context)
        
        context['workflow_state'] = current_workflow_state
        context['next_workflow_task'] = current_workflow_task
        
        return current_workflow_state
          #outputs workflow list with current selection

    def output_ticket_current_group_owner(self, cr, uid, ids, ticket_type, ticket_workflow_position_id, current_state, context):
        #ticket_type = context.get('ticket_type', False)
        #ticket_workflow_position_id = context.get('ticket_workflow_position_id', False)
        current_workflow_state = ''
        
        # Get list of users in the current group and get the user with the least active tickets in their queue
        customer_service_type = self.pool.get('customer_service.service_type').browse(cr, uid, ticket_type, context)
        if customer_service_type.assigned_workflow.group_ids:     
            for assigned_workflow_i in customer_service_type.assigned_workflow.group_ids:
                
                # Mark the current department process and show current group
                if assigned_workflow_i.id == ticket_workflow_position_id:                    
                    return self.write(cr, uid, ids, {'current_group': assigned_workflow_i.group.id}, context=context)
        else:
            raise osv.except_osv(_('Attention'),_('No workflows or assigned groups have been set... talk to an administrator to have that set in the Customer Service Settings.'))
            

                             
    def is_current_user_in_ticket_queue(self, cr, uid, ids, context={}):         
        ticket_type = context.get('ticket_type', False)
        
        # Get list of users in the current group and check to see if the of this ticket user is in this group
        customer_service_type = self.pool.get('customer_service.service_type').browse(cr, uid, ticket_type, context)
        if customer_service_type: 
            user_ids = customer_service_type[0].assigned_group.user_ids
            if user_ids:
                    for user_id_i in user_ids:
                        userid_from_group_list = user_id_i.name.id
                        if uid == userid_from_group_list:
                            return True
        return False # id is not in this group for this type
        
    def do_ticket_handoff_to_available_to_next_group(self, cr, uid, ids, ticket_type, ticket_workflow_position_id, current_state, context):    
            
            
        # Check if this is the first group    
        if ticket_workflow_position_id == 'start':
            customer_service_type = self.pool.get('customer_service.service_type').browse(cr, uid, ticket_type, context)
             # output ticket workflow
            self.output_ticket_workflow(cr, uid, ids, ticket_type, ticket_workflow_position_id, current_state, context)
            self.get_and_set_next_ticket_workgourp_id(cr, uid, ids, context)
            return
                
        # Set the current state to active for output        
        current_state = 'active' 
                
        #Get next ticket ID
        ticket_workflow_position_id = self.get_and_set_next_ticket_workgourp_id(cr, uid, ids, context)

        # Don't do anything if there is no valid next group        
        if (ticket_workflow_position_id == False):
            return True
                    
        # User Handoff
        # Init vars 
        previous_user_ticket_count = 0
        ticket_user_count = 0
        handed_ticket_user = False
        
        
                        
        # Get list of users in the current group and get the user with the least active tickets in their queue
        cs_ticket_group_obj = self.pool.get('customer_service.groups') #user_id_i.name.id                        
        user_ids = cs_ticket_group_obj.browse(cr, uid, ticket_workflow_position_id,context).group.user_ids 
                    
        if user_ids:
                    
                # Shuffle user id's to be fair in ticket assignments
                shuffle(user_ids)
                    
                # Get users id
                for user_id_i in user_ids:
                        
                    # Handoff to user if the user is available
                    if user_id_i and user_id_i.skip_user == False:
                            
                        # get ticket counts for all ticket users in the group
                        cs_ticket_obj = self.pool.get('customer_service.ticket') #user_id_i.name.id                        
                        ticket_user_count = cs_ticket_obj.search(cr, uid, [('cs_ticket_owner.id', '=', user_id_i.name.id), ('state', '=', 'active')], count = True) # [('cs_ticket_owner', '=', user_id_i.name)]  ,  # , ('state', '=', 'active')  ('cs_ticket_owner', '=', user_id_i.name)                                                                            
                            
                        # Get initial user with ticket size       
                        if ticket_user_count == 0:
                            handed_ticket_user = user_id_i.name.id                                
                            break
                                                                 
                        elif (previous_user_ticket_count == 0 and ticket_user_count > 0):
                            previous_user_ticket_count = ticket_user_count
                            handed_ticket_user = user_id_i.name.id
                            
                        elif ticket_user_count < previous_user_ticket_count:
                            previous_user_ticket_count = ticket_user_count
                            handed_ticket_user = user_id_i.name.id                            
                                                                                                                    
        else:
            raise osv.except_osv(_('Attention'),_('There are no users in the system that are assigned to this ticket type, please have administrator contact ticket user.'))
                            
                            
        # Get chatter information
        transfered_group_name = cs_ticket_group_obj.browse(cr, uid, ticket_workflow_position_id,context).group.group_name
        
        # Post to chatter when it's not the first handoff, Chatter has to be initialized before it can be posted
        if (current_state != 'start'):
            self.message_post(cr, uid, ids, _("Ticket has been passed to user: "+user_id_i.name.name+" in group: "+ transfered_group_name), _( 'Ticket Group'), context=context)                    
                            

        # output ticket workflow
        self.output_ticket_workflow(cr, uid, ids, ticket_type, ticket_workflow_position_id, current_state, context)

        # output handed user
        return self.write(cr, uid, ids, {'cs_ticket_owner': handed_ticket_user}, context=context)


    
    def _get_cutomer_type_tag_list(self, cr, uid, ids, partner_id, context={}):
        # Get Category ID's          
        category_ids = ''    
        customer = self.pool.get('res.partner').browse(cr, uid, partner_id, context)
        if customer:
            for customer_category_id in customer.category_id:
                category_ids += customer_category_id.name + " / "
        return category_ids
        
    def update_ticket(self, cr, uid, ids, context={}):
        current_state = context.get('current_state', False)[0]
        ticket_type = context.get('ticket_type', False)
        ticket_workflow_position_id = context.get('ticket_workflow_position_id', False)
        workflow_group_state = context.get('workflow_group_state', False)[0]                

        # set use inplace editor for initial contact form 
        if (current_state == 'draft'):
            is_initial_contact_log = True
        else:
            is_initial_contact_log = False
        
        # if single ticket, then close ticket right away                    
        if self.is_single_instance_ticket(cr, uid, ids, context):
            self.write(cr, uid, ids, {'state': 'done'}, context=context) 
            
        # otherwise process as normal ticket
        else:                    
            # output ticket workflow for handoffs
            self.output_ticket_workflow(cr, uid, ids, ticket_type, ticket_workflow_position_id, workflow_group_state, context)

            # write state
            self.write(cr, uid, ids, {'state': 'active'}, context=context)        
                
        # Call log Popup
        return self._popup_customer_service_call_log(cr, uid, is_initial_contact_log, context=context)  

    def handoff_ticket(self, cr, uid, ids, context={}):
        ticket_type = context.get('ticket_type', False)
        ticket_workflow_position_id = context.get('ticket_workflow_position_id', False)
        current_state = context.get('workflow_group_state', False)  
        ticket_status = context.get('ticket_status', False)  
        check_ticket_owner = True
            
        #Make sure that customer service person can't handoff a ticket that's not thiers! (unless they are a manager)   
        group_id = self.pool.get('ir.model.data').get_object_reference(cr, 1, 'customer_service', 'customer_service_manager')
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        user_group = user.groups_id        
        user_group_ids = [g.id for g in user_group]        
        cs_group_id = group_id[1]
        
        # get current ticket owner
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        
        # Check to make sure the tickets are not marked as updatable
        for id in ids: 
            cs_ticket = self.pool.get('customer_service.ticket').browse(cr, uid, id)
            ticket_owner = cs_ticket.cs_ticket_owner            
        try:
            ticket_owner
        except:
            check_ticket_owner = False
                        
        # check if ticket owner before handing off    
        if  check_ticket_owner and (cs_group_id not in user_group_ids) and (ticket_owner.id != uid):
        
            ## Raise your error message
            raise osv.except_osv(_('Attentinon'), _("Ticket doesn't belong to you.  Only managers have ability to handoff other user's tickets."))
        

        
        # check to ensure that ticket workflow  and groups are assigned    
        customer_service_type = self.pool.get('customer_service.service_type').browse(cr, uid, ticket_type, context)
        if not customer_service_type:   
            raise osv.except_osv(_('Attention'),_('No "Workflow" or "Single Instance Ticket" has been setup yet... Contact administrator to setup in Customer Service settings'))
            return False
         
        if not customer_service_type.assigned_workflow:   
            raise osv.except_osv(_('Attention'),_('No "Workflow" or "Single Instance Ticket" has been setup yet... Contact administrator to setup in Customer Service settings'))
            return False 
        
        if not customer_service_type.assigned_workflow.group_ids:   
            raise osv.except_osv(_('Attention'),_('No "Groups" have been setup for this workflow yet... Contact administrator to setup in Customer Service settings'))
            return False 
        
        # set to pending if normal
        if(ticket_status == 'pending'):
            ticket_status = 'normal'

        # produce first popup if
        #self.update_ticket(cr, uid, ids, context)        
       # self.update_ticket(self, cr, uid, context) 

        self.write(cr, uid, ids, {'state': 'active', 'workflow_group_state': 'working' , 'ticket_status':ticket_status}, context=context)
        self.do_ticket_handoff_to_available_to_next_group(cr, uid, ids, ticket_type, ticket_workflow_position_id, current_state, context)    
          
        return {  
            'id':'customer_service_ticket_form',
            'view_type': 'form',
            'view_mode': 'form,tree',
            'res_model': 'customer_service.ticket',
            'type': 'ir.actions.act_window',
            'view_ref':'customer_service_ticket_form',
            'target': 'blank',
            #'context': context,
                        
        }
        
        # popup on first call   
        #if (current_state == 'start'):        
        #    return self._popup_customer_service_call_log(cr, uid, True, context)
        #else:
        #    return {}  

    def pending_status(self, cr, uid, ids, context={}):         
        self.write(cr, uid, ids, {'ticket_status': 'pending'}, context=context)
        ticket_status = context.get('ticket_status', False)
        
        # Display pending form if pending
        if(ticket_status == 'pending'):
            return {
                    'name': 'My Pending Tickets',    
                    'view_type': 'form',
                    'view_mode': 'tree,form',
                    'res_model': 'customer_service.ticket',
                    'type': 'ir.actions.act_window',
                    'target': 'self',
                    'context': "{'search_default_active_ticket':'active'}",
                    'domain': "[('cs_ticket_owner','=',uid), ('state','!=','done'), ('state','!=','cancel'), ('ticket_status','=','pending')]",
                    #'search_view_id':'view_cs_ticket_filter',
                    'nodestroy': True,                 
            }
        else:
            return {
                    'name': 'My Tickets',    
                    'view_type': 'form',
                    'view_mode': 'tree,form',
                    'res_model': 'customer_service.ticket',
                    'type': 'ir.actions.act_window',
                    'target': 'self',
                    'context': "{'search_default_active_ticket':'active', 'search_default_active_ticket':'precomplete'}",
                    'domain': "[('cs_ticket_owner','=',uid), ('state','!=','done'), ('state','!=','cancel'), ('ticket_status','!=','pending')]",
                    #'search_view_id':'view_cs_ticket_filter',
                    'nodestroy': True,                 
            }
    
    def own_ticket(self, cr, uid, ids, context={}):
        ticket_status = context.get('ticket_status', False)  
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        self.message_post(cr, uid, ids, _("<span>" + user.name+" took ownership of ticket"), _( 'Ticket Owner Change'), context=context)

         # set to pending if normal
        if(ticket_status == 'pending'):
            ticket_status = 'normal'      
              
        self.write(cr, uid, ids, {'cs_ticket_owner': uid, 'ticket_status': ticket_status}, context=context)
         
        # Display pending form if pending
        if(ticket_status == 'pending'):
            return {
                    'name': 'My Pending Tickets',    
                    'view_type': 'form',
                    'view_mode': 'tree,form',
                    'res_model': 'customer_service.ticket',
                    'type': 'ir.actions.act_window',
                    'target': 'self',
                    'context': "{'search_default_active_ticket':'active'}",
                    'domain': "[('cs_ticket_owner','=',uid), ('state','!=','done'), ('state','!=','cancel'), ('ticket_status','=','pending')]",
                    #'search_view_id':'view_cs_ticket_filter',
                    'nodestroy': True,                 
            }
        else:
            return {
                    'name': 'My Tickets',    
                    'view_type': 'form',
                    'view_mode': 'tree,form',
                    'res_model': 'customer_service.ticket',
                    'type': 'ir.actions.act_window',
                    'target': 'self',
                    'context': "{'search_default_active_ticket':'active', 'search_default_active_ticket':'precomplete'}",
                    'domain': "[('cs_ticket_owner','=',uid), ('state','!=','done'), ('state','!=','cancel'), ('ticket_status','!=','pending')]",
                    #'search_view_id':'view_cs_ticket_filter',
                    'nodestroy': True,                 
            }


    def multi_own_ticket(self, cr, uid, ids, context={}):
        group_id = self.pool.get('ir.model.data').get_object_reference(cr, 1, 'customer_service', 'customer_service_manager')
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        user_group = user.groups_id
        
        user_group_ids = [g.id for g in user_group]        
        cs_group_id = group_id[1]
        if  cs_group_id not in user_group_ids:
            
            ## Raise your error message
            raise osv.except_osv(_('Access Rights Error'), _("Only managers have access to this function."))
        else:     
            # Check to make sure the tickets are not marked as updatable
            for id in ids:
                ticket_status = self.pool.get('customer_service.ticket').browse(cr, uid, id).state
            
                if (ticket_status == "draft" or ticket_status == "cancel" or ticket_status == "done"):
                    flag_ticket_not_updatable = True
                else:
                    self.write(cr,uid,[id],{'cs_ticket_owner':uid},context)                 
                    self.message_post(cr, uid, [id], _(user.name+" took ownership of ticket (multi-own option)"), _( 'Ticket Owner Change'), context=context)
        return
    
    def multi_set_pending_ticket(self, cr, uid, ids, context={}):
        
        flag_ticket_not_updatable = False
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        
        # Check to make sure the tickets are not marked as updatable
        for id in ids:            
            cs_ticket = self.pool.get('customer_service.ticket').browse(cr, uid, id)
            ticket_owner = cs_ticket.cs_ticket_owner
            ticket_status = cs_ticket.state
            
            if cs_ticket.cs_ticket_owner.id == uid:
                if (ticket_status == "draft" or ticket_status == "cancel" or ticket_status == "done"):
                    flag_ticket_not_updatable = True
                else:
                    self.write(cr,uid,[id],{'ticket_status':'pending'},context) 
                    self.message_post(cr, uid, [id], _(user.name+" set ticket to pending (multi-set option)"), _( 'Ticket Status Change'), context=context)
                                            
        # Raise warning
        #if(flag_ticket_not_updatable):
            ## Raise your error message
           # raise osv.except_osv(_('Warning'), _("Some of the tickets were left unmodified because they may be in a new, closed or cancelled status."))
                
        return    


    def complete_ticket(self, cr, uid, ids, context={}):  
                
        #Make sure that customer service person can't handoff a ticket that's not thiers! (unless they are a manager)  
        check_ticket_owner = True 
        group_id = self.pool.get('ir.model.data').get_object_reference(cr, 1, 'customer_service', 'customer_service_manager')
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        user_group = user.groups_id        
        user_group_ids = [g.id for g in user_group]        
        cs_group_id = group_id[1]
        
        # get current ticket owner
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        
        # Check to make sure the tickets are not marked as updatable
        for id in ids: 
            cs_ticket = self.pool.get('customer_service.ticket').browse(cr, uid, id)
            ticket_owner = cs_ticket.cs_ticket_owner            
        try:
            ticket_owner
        except:
            check_ticket_owner = False
                        
        # check if ticket owner before handing off    
        if  check_ticket_owner and (cs_group_id not in user_group_ids) and (ticket_owner.id != uid):
        
            ## Raise your error message
            raise osv.except_osv(_('Attentinon'), _("Ticket doesn't belong to you.  Only managers have ability to complete other user's tickets."))
        
        
        ticket_status = context.get('ticket_status', False)  
        self.state = 'done'
        self.write(cr, uid, ids, {'state': 'done'}, context=context)
        #return  #self._popup_customer_service_call_log(cr, uid, ids, False, context=context) #context          
        self.write(cr, uid, ids, {'next_workflow_task': 'Complete'}, context=context) 
        
        # Display pending form if pending
        if(ticket_status == 'pending'):
            return {
                    'name': 'My Pending Tickets',    
                    'view_type': 'form',
                    'view_mode': 'tree,form',
                    'res_model': 'customer_service.ticket',
                    'type': 'ir.actions.act_window',
                    'target': 'self',
                    'context': "{'search_default_active_ticket':'active'}",
                    'domain': "[('cs_ticket_owner','=',uid), ('state','!=','done'), ('state','!=','cancel'), ('ticket_status','=','pending')]",
                    #'search_view_id':'view_cs_ticket_filter',
                    'nodestroy': True,                 
            }
        else:
            return {
                    'name': 'My Tickets',    
                    'view_type': 'form',
                    'view_mode': 'tree,form',
                    'res_model': 'customer_service.ticket',
                    'type': 'ir.actions.act_window',
                    'target': 'self',
                    'context': "{'search_default_active_ticket':'active', 'search_default_active_ticket':'precomplete'}",
                    'domain': "[('cs_ticket_owner','=',uid), ('state','!=','done'), ('state','!=','cancel'), ('ticket_status','!=','pending')]",
                    #'search_view_id':'view_cs_ticket_filter',
                    'nodestroy': True,                 
            }
            
    def modify_ticket(self, cr, uid, ids, context={}):  
        self.state = 'modify'
         #context
        self.write(cr, uid, ids, {'state': 'modify'}, context=context) # {"views": [(self.pool.get('ir.model.data').get_object_reference(cr, uid, 'module', 'view_id')[1]}
        return self._popup_customer_service_call_log(cr, uid, ids, False, context=context)
    
    def cancel_ticket(self, cr, uid, ids, context={}):  
        ticket_status = context.get('ticket_status', False)  
        self.state = 'cancel'
        self.write(cr, uid, ids, {'state': 'cancel'}, context=context)
        #self._popup_customer_service_call_log(cr, uid, ids, False, context=context) #context
        return {
            
            'name': 'My Tickets',    
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'customer_service.ticket',
            'type': 'ir.actions.act_window',
            'target': 'self',
            'context': "{'search_default_active_ticket':'active', 'search_default_active_ticket':'precomplete'}",
            'domain': "[('cs_ticket_owner','=',uid), ('state','!=','done'), ('state','!=','cancel'), ('ticket_status','!=','pending')]",
            #'search_view_id':'view_cs_ticket_filter',
            'nodestroy': True,                 
            }
        
    def close_ticket(self, cr, uid, ids, context={}):  
        self.state = 'done'
        return self.write(cr, uid, ids, {'state': 'done', 'next_workflow_task': 'Complete'}, context=context) 
        #self._popup_customer_service_call_log(cr, uid, ids, False, context=context) #context
        
    def _popup_customer_service_call_log(self, cr, uid, is_initial_update, context={}):  
           
        # init contact log vars
        c_log_initial_note= ''
        c_log_type_initial_note = ''
        
        # Get vars
        cs_partner_id = context.get('new_ticket_customer_id')
        ticket_number = context.get('new_ticket_customer_ticket_number')
        current_ticket_ref = context.get('current_ticket_ref') 
        current_state = context.get('current_state') 
        c_log_type_initial_note = context.get('c_log_type_initial_note') 
        c_log_initial_note = context.get('c_log_initial_note') 
        
        
        # insert initial contact log notes if first time            
        if is_initial_update:
            c_notes = c_log_initial_note
            c_type = c_log_type_initial_note
        
            res = {'id': current_ticket_ref,
                   'state' : 'active',                   
                   'model': 'ir.actions.act_window',
                   'name': 'Contact Log',
                   #'view_id': 'customer_service_call_log_tree',
                   'type': 'ir.actions.act_window',
                   'res_model': 'customer_service_call_log.customer_service_call_log',
                   'view_type': 'form',
                   'view_mode': 'form,tree',
                   'target': 'new',         
                   'context': '{"new_ticket_customer_id":'+str(cs_partner_id[0])+',"new_ticket_customer_ticket_number":"'+(ticket_number[0])+'","current_ticket_ref":"'+str(current_ticket_ref)+'","c_log":"'+str(c_notes)+'","c_log_type":"'+str(c_type)+'"}'                   
                }
        else:
            res = {'state' : 'active',
                   'id': 'customer_service_call_log_action',
                   'model': 'ir.actions.act_window',
                   'name': 'Contact Log',
                   #'view_id': 'customer_service_call_log_tree',
                   'type': 'ir.actions.act_window',
                   'res_model': 'customer_service_call_log.customer_service_call_log',
                   'view_type': 'form',
                   'view_mode': 'form,tree',
                   'target': 'new',         
                   'context': '{"new_ticket_customer_id":'+str(cs_partner_id[0])+',"new_ticket_customer_ticket_number":"'+(ticket_number[0])+'","current_ticket_ref":"'+str(current_ticket_ref)+'"}'                   
                }
        
        
        # return popup window action
        
        #self.write(cr, uid, [], res, context=context)
        return res
    
    

    
     # Events
    def form_load(self, cr, uid, ids, partner_id, state, context=None):
                        
        result = {}
                        
        # Shipping Address Info Output  
        if partner_id:            
            shipping_address_clipboard_info = self.shipping_address_info(cr, uid, ids, partner_id, context=None)                        
            self.write(cr, uid, ids, {'shipping_address_clipboard' : shipping_address_clipboard_info}, context=context)
            result['value'] = {'shipping_address_clipboard' : shipping_address_clipboard_info}

            # warn if customer has ticket
            if self.has_active_tickets(cr, uid, ids, partner_id, True, context):
                ticket_warning = 'Attention - Customer currently has active ticket(s) in the system!'
            else:
                ticket_warning = ''
            self.write(cr, uid, ids, {'customer_active_tickets_warning': ticket_warning}, context=context)                
                        
        # Customer Type Tags                
        if state != 'draft':
            category_ids = self._get_cutomer_type_tag_list(cr, uid, ids, partner_id, context)
            self.write(cr, uid, ids, {'category_ids': '\n'+category_ids+'\n'}, context=context)
            
        
        return result    
                          
    def on_change_partner(self, cr, uid, ids, partner_id, context=None):
        result = {}
        values = {}
        
        
                    
        # Get customer information           
        if partner_id:
            
            # warn if customer has ticket
            if self.has_active_tickets(cr, uid, ids, partner_id, False, context):
                ticket_warning = 'Attention - Customer currently has active ticket(s) in the system!'
            else:
                ticket_warning = ''
                
            
            # Get partner information
            partner = self.pool.get('res.partner').browse(cr, uid, partner_id, context=context)    
                
            shipping_address_clipboard_info = self.shipping_address_info(cr, uid, ids, partner_id, context=None) 
                
            
            values = {
                #'ticket_date': current_date_time, 
                'customer_active_tickets_warning': ticket_warning,      
                'state': 'draft',      
                'name' : partner.name,
                'category_ids' : self._get_cutomer_type_tag_list(cr, uid, ids, partner_id, context),
                'image_medium': partner.image, 
                'street' : partner.street,
                'street2' : partner.street2,
                'city' : partner.city,
                'state_id' : partner.state_id and partner.state_id.id or False,
                'zip' : partner.zip,
                'country_id' : partner.country_id and partner.country_id.id or False,
                'email' : partner.email,
                'phone' : partner.phone,
                'mobile' : partner.mobile,
                'fax' : partner.fax,
                'website' : partner.website,
                'shipping_address_clipboard' : shipping_address_clipboard_info,
                
            }
        return {'value' : values}   
     
    def on_change_service_category(self, cr, uid, ids, service_category, context=None):
  
        return {'value' : {'ticket_type': ''},
            'domain' : {'ticket_type': [('service_category', '=', service_category)]}
                }   
        '''         
    def on_change_service_type(self, cr, uid, ids, ticket_type, context=None):
        value = {}
  
        if ticket_type.single_instance_item == True:
            value = {'state':'precomplete'}
        else:
            value = {'name':'precomplete'}  
        return {'value' : value}          
        '''     
        
    def on_change_status(self, cr, uid, ids, status, context=None):
  
        return {'value' : {'ticket_status': status}}           

class customer_service_user(osv.osv):
    """ Customer Service User """
    _name = 'customer_service.user'
    _rec_name = 'name'
    _columns = {
        
     
        # User Information
        'user_id': fields.many2one('customer_service.group', 'Users'),
        'name':  fields.many2one('res.users', 'Name', required=True),  # fields.char('Name', size=128, select=True),  #   
        'skip_user': fields.boolean('Don\'t tickets to this assign to user'),  
         
    }    

    _order = "name"
    
    _defaults = {
       'skip_user': lambda *a: False,
    }
    
    def onchange_customer_service_group_member_name(self, cr, uid, ids, name, context=None):
        if name:
            member_info =  self.pool.get('res.users').browse(cr,uid,name,context)

            return {'value': {
            'name':member_info.name,        
            }}       
        
#customer_service_user()



class customer_service_group(osv.osv):
    """ Customer Service Group """
    _name = 'customer_service.group'
    _rec_name = 'group_name'
    _columns = {
        
        'group_name': fields.char('Group Name', size=256, help='Name of the group where the ticket is directed', required=True),
        'group_manager': fields.many2one('res.users', 'Group Manager', required=True, ondelete="cascade", help="The manager of the group who will be notified if ticket is overdue"),
        'user_ids': fields.one2many('customer_service.user', 'user_id', 'Service Groups', readonly=False, ondelete="cascade", help="The list of users assigned to the ticket group"),                    
    }    
        
customer_service_group()

class customer_service_groups(osv.osv):
    """ Customer Service Groups """
    _name = 'customer_service.groups'
    _rec_name = 'group'
    _columns = {
        'group_id': fields.many2one('customer_service.workflow', 'Group ID', ondelete="cascade"),
        'service_task': fields.char('Service workflow Task Description', size=512, required=False),
        'group': fields.many2one('customer_service.group', 'Group', required=True, help="Group", ondelete="cascade"),    
        'sort_priority': fields.integer('Workflow Process Order', required=True, help="Priority of customer service groups"), 
    }    
    
    _defaults = {
        'sort_priority': 1,
    }    
    
    _order = "sort_priority, service_task"    
         
customer_service_groups()

class customer_service_workflow(osv.osv):
    """ Customer Status Workflow """
    _name = 'customer_service.workflow'
    _rec_name = 'workflow_name'
    _columns = {
        'workflow_name': fields.char('Service workflow Name', size=256, required=True),
        'group_ids': fields.one2many('customer_service.groups', 'group_id', 'Service Groups', readonly=False, help="The list of groups assigned to the workflow"),        
    }        

    def onchange_customer_service_group(self, cr, uid, ids, group_service, context=None):
        if group_service:
            group_info =  self.pool.get('customer_service.group').browse(cr,uid,group_service,context)
            dic ={
              'group_name': group_info.group_name,
            }
            return {'group_name': group_info.group_name}
        
customer_service_workflow()


class customer_service_category(osv.osv):
    """ Customer Service Sub-Type """
    _name = 'customer_service.service_category'
    _rec_name = 'service_category'
    _columns = {
        'service_category': fields.char('Service Category', size=256, required=True, help="The type of ticket that will be assigned to customer/client"),
        'sort_priority': fields.integer('List Order', required=True, help="Priority of customer service Category"), 
    }    
    
    _defaults = {
        'sort_priority': 1,
    }      
    
    _order = "sort_priority, service_category"
           
customer_service_category() 

class customer_service_type(osv.osv):
    """ Customer Service Type """
    _name = 'customer_service.service_type'
    _rec_name = 'ticket_type'
    _columns = {
        'ticket_type': fields.char('Ticket Type', size=256, required=True, help="The type of ticket that will be assigned to customer/client"),
        'service_category': fields.many2one('customer_service.service_category', 'Assigned Service Category', required=True, ondelete='set null', help="Category assigned to this type"), 
        'assigned_workflow':  fields.many2one('customer_service.workflow', 'Assigned Workflow', required=False, help="Workflow assigned to this group"), # ondelete='set null'
        'single_instance_item':  fields.boolean('Single instance type / Ignore workflow', required=False, help="When checked, no workflow is needed. i.e. Service type is a general question"),
        'due_date_expired_notification_days':  fields.integer('Due Date Timeframe (Days)', help="This will notify manager of overdue service"),
        'sort_priority': fields.integer('List Order', required=True, help="Priority of customer service sub types"), 
        'first_service_task': fields.char('First service workflow task description when ticket is created', size=512, required=False),
    }
    
    
    _defaults = {
        'due_date_expired_notification_days': 0,
        'first_service_task': 'Ticket is ready to handoff',
    }
    
    _order = "sort_priority, ticket_type"
    
    def onchange_customer_service_workflow(self, cr, uid, ids, workflow_service, context=None):
        if workflow_service:
            group_info =  self.pool.get('customer_service.group').browse(cr,uid,workflow_service,context)
            dic ={
              'workflow_name': group_info.workflow_name,
            }
            return {'workflow_name': group_info.workflow_name}           


class customer_status_type(osv.osv):
    """ Customer Status Type """
    _name = 'customer_service.status_type'
    _rec_name = 'status_type'
    _columns = {
        'status_type': fields.char('Status Type', size=256, required=True),
        'sort_priority': fields.integer('Sort Priority', help="Priority of the selectable status types"),
    }        
    _defaults = {
        'sort_priority': 0,
    }
    _order = "sort_priority, status_type"

customer_status_type()






# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: