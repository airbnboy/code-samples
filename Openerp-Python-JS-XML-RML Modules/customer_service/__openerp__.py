# -*- coding: utf-8 -*-
##############################################################################
# Module by Kranbery Techonolgies LLC
# Not for individual resale or redistribution without expressed written permission of Kranbery Technologies LLC
# Created July 21st 2013
##############################################################################

{
    'name': 'Customer Service',
    'version': '1.1',
    'author': 'Kranbery Technologies LLC',
    'category': 'Tools',
    'description': "Provides a ticketing and routing system for information handled by customer service specialists",
    'website': 'http://www.kranbery.com',
    'license': 'Other proprietary',
    'summary': 'Customer Service Ticketing by Kranbery',
    'depends': ['base', 'sale', 'account'],
    'data' : [
        'security/customer_service_security.xml',              
        'security/ir.model.access.csv',
        'customer_service_view.xml', 
        'customer_service_contact_log_view.xml',
    ],    
    'js': ['static/src/js/notifier.js'],
    'css': [ 'static/src/css/customer_service.css' ],
    "active": False,
    "installable": True
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
