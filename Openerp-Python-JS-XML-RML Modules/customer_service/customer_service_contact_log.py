# -*- coding: utf-8 -*-
##############################################################################
# Module by Kranbery Techonolgies LLC
##############################################################################

from osv import osv, fields
from datetime import *
#from tools.translate import _

class customer_service_call_log(osv.osv):
    
    _name = 'customer_service_call_log.customer_service_call_log'    
    _rec_name = 't_number'
        
    # Get-Fields    
    def cs_log_default_cusomter(self, cr, uid,  context=None):        
        get_var_res = context.get('new_ticket_customer_id', False)
        return get_var_res    
    
    def cs_log_default_t_number(self, cr, uid,  context=None):        
        get_var_res = context.get('new_ticket_customer_ticket_number', False)
        return get_var_res
    
    def cs_log_default_t_ref(self, cr, uid,  context=None):        
        get_var_res = context.get('current_ticket_ref', False)
        return get_var_res        

    def cs_log_default_log(self, cr, uid,  context=None):        
        get_var_res = context.get('c_log', False)
        return get_var_res 
        
    def cs_log_default_log_type(self, cr, uid,  context=None):        
        get_var_res = int(context.get('c_log_type', False))
        return get_var_res         
    
    _columns = {
                
        # Ticket state        
        'state': fields.selection([
            ('draft', 'Edit Notes'),
            ('done', 'Close Notes')],
            'Status', readonly=True, required=True,
            track_visibility='onchange',
            help='Indicates if the log is active or closed'),                   
         
        't_ref_id': fields.char('Ticket Ref ID', size=256, required=True, help='Ticket ref ID', readonly=True),       
        't_number': fields.char('Ticket Number', size=256, required=True, help='Customer\'s trouble ticket number', readonly=True),               
        'contact_date': fields.datetime('Date', states={'draft,done': [('readonly', True)]}, required=True,  help="Date of the contact/call", readonly=True),
        't_customer': fields.many2one('res.partner', 'Customer', required=True, readonly=True),
        't_user': fields.many2one('res.users', 'Owner', required=True, readonly=True),
        'c_log_issue': fields.text('Issue', readonly=False, required=True, help='Issue'),
        'c_log_resolution': fields.text('Resolution', readonly=False, required=True, help='Issue'),
        'c_log': fields.text('Contact Log', readonly=False, required=True, help='Customer contact information'),
        'c_log_type': fields.many2one('customer_service_call_log.customer_service_contact_type', 'Customer Contact Type', readonly=False, required=True), #, states={'done': [('readonly', True)]}
        
        }

    _order = "contact_date desc"

    _defaults = {
        'state': 'draft',
        't_customer': cs_log_default_cusomter,
        't_number': cs_log_default_t_number,
        't_ref_id': cs_log_default_t_ref,
        'c_log': cs_log_default_log,
        'c_log_type': cs_log_default_log_type,
        'contact_date': lambda *a: datetime.now().strftime('%Y-%m-%d %H:%M:%S'), #datetime.date.today().strftime('%Y-%m-%d %H:%M:%S'),
        't_user': lambda s, cr, u, c: u,
    }
    
    
    def update_contact_log(self, cr, uid, ids, context=None):  
        self.state = 'done'      
        return self.write(cr, uid, ids, {'state': 'done'}, context=context)   
        
customer_service_call_log()

class customer_service_contact_type(osv.osv):
        
    _name = 'customer_service_call_log.customer_service_contact_type'    
    _rec_name = 'contact_type'    
    _columns = {
        'contact_type': fields.char('Customer Contact Type', size=256, required=True, help='The type of contact to log'),
        'sort_priority': fields.integer('Sort Priority', help="Priority of the selectable status types"),
    }        
    _defaults = {
        'sort_priority': 0,
    }
    
    _order = "sort_priority"

customer_service_contact_type()



# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: