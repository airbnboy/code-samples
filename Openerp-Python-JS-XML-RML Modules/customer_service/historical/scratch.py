
        def _get_bom_product_ids(self, cr, uid, ids, name, arg, context=None):
        res = {}
        bom_obj = self.pool.get('mrp.bom')
        for r in self.browse(cr, uid, ids):
            product_ids = []
            bom = bom_obj.browse(cr, uid, bom_obj.search(cr, uid, [('routing_id', '=', r.routing_id.id)]), context=context)
            if bom:
                bom_ids = bom_obj.search(cr, uid, [('bom_id', '=', [bom[0].id])])
                for b in bom_obj.browse(cr, uid, bom_ids, context=context):
                    product_ids.append(b.product_id.id)
            res[r.id] = product_ids
        return res
    
    
    _columns {
        'bom_product_ids': fields.function(_get_bom_product_ids, method=True, type='many2many', relation='product.product', string='BOM Products' ),
        'bom_item_ids': fields.many2many('product.product', 'mrp_routing_workcenter_product_rel', 'routing_workcenter_id', 'product_id', 'Linked BOM Items', help="These are products that require to be consumed before this WO can procede"),
    }
    
    
    
                '''
            product_ids = []
            customers = customers_obj.browse(cr, uid, customers_obj.search(cr, uid, [('routing_id', '=', r.routing_id.id)]), context=context)
            if customers:
                customers_ids = customers_obj.search(cr, uid, [('customers_id', '=', [customers[0].id])])
                for b in customers_obj.browse(cr, uid, customers_ids, context=context):
                    product_ids.append(b.product_id.id)
            res[r.id] = product_ids
            '''
    
    
    
    
    
    
    
    
    
    def onchange_customer_service_group_member_name(self, cr, uid, ids, name, context=None):
        if name:
            member_info =  self.pool.get('res.users').browse(cr,uid,name,context)
            dic ={
              'name': member_info.name,
              'email': member_info.email,              
            }
            return {'value': dic}
            
            
            '''   def _check_duplicate_user_names(self, cr, uid, ids, context=None):
        previous_name = ''
        for users in self.browse(cr, uid, ids, context=context):            
            if users.user_ids == previous_name:
                return False
            else:
                previous = users.user_ids
        return True
'''

    _constraints = [
        (_check_duplicate_user_names, 'Error ! Closing Date cannot be set before Beginning Date.', ['user_ids']),
    ]
    
    
    
        _sql_constraints = [
            ('user_ids_unique', 'unique (user_ids)',
                'The users must be unique')
            ]


    def _check_duplicate_user_names(self, cr, uid, ids, context=None):
        previous_name = ''
        for users in self.browse(cr, uid, ids, context=context):            
            if users.user_ids == previous_name:
                return False
            else:
                previous = users.user_ids
        return True

    _constraints = [
        (_check_duplicate_user_names, 'Error ! Closing Date cannot be set before Beginning Date.', ['user_ids']),
    ]            
    