openerp.image_scanner_uploader = function(instance, m) {
var _t = instance.web._t,
    QWeb = instance.web.qweb;

    instance.web.Sidebar.include({
        redraw: function() {
        	// Add One Touch Scan in Attachment menu
            var self = this;
            this._super.apply(this, arguments);
            self.$el.find('.oe_sidebar_add_google_doc').after(QWeb.render('OnetouchscanItem', {widget: self}))
            self.$el.find('.oe_sidebar_add_one_touch_scan').on('click', function (e) {
                self.on_google_doc_test();
            });
        },
        on_google_doc_test: function() {
        	// Call py function for scanning and uploading image
            var self = this;
            var view = self.getParent();
            var ids = ( view.fields_view.type != "form" )? view.groups.get_selection().ids : [ view.datarecord.id ];
            if( !_.isEmpty(ids) ){
                view.sidebar_eval_context().done(function (context) {
                    var ds = new instance.web.DataSet(this, 'ir.attachment', context);
                    ds.call('scan_upload_image_get_test', [view.dataset.model, ids, context]).done(function(attachments) {
                        if (attachments == 'False') {
                            var params = {
                                error: response,
                                message: _t("Failed to scan/upload image")
                            }                            
                        }
                    }).done(function(attachments){
                        view.reload();
                    });
                });
            }
        }
    });
};
