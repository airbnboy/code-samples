import logging
from datetime import datetime

from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT
from openerp.osv import fields, osv
from openerp.tools.translate import _

import sys
sys.path.append('C:/Python26/Lib/site-packages')

import os
import base64

def scan(self, dpi): 
    #scan image from the scanner and return binary file
        
    if os.name == 'nt':
        # for windows
        import twain
        src_manager = twain.SourceManager(0)
        devices = src_manager.GetSourceList() # gives list of devices
        if devices:
            ss = src_manager.OpenSource(devices[1]) # opens 0th device by default, change as per the device selection
            ss.SetCapability(twain.ICAP_YRESOLUTION, twain.TWTY_FIX32, 200.0) 
            ss.RequestAcquire(0,0)
            info = ss.GetImageInfo()
            if info:
                (handle, count) = ss.XferImageNatively()
                # specified name with full path for file
                title = "%s %s" % ('C:/Program Files/OpenERP 7.0-20130606-231042/Server/server/openerp/addons/image', datetime.today().strftime(DEFAULT_SERVER_DATETIME_FORMAT))+'.bmp'
                str_image = twain.DIBToBMFile(handle, 'title') #save image in .bmp file
                twain.GlobalHandleFree(handle)
                ss.destroy()
                src_manager.destroy()
                file_open = open('title', 'rb')
                base_file = base64.encodestring(file_open.read()) #convert scanned file in to binary file 
                return base_file
        else:
            raise osv.except_osv(('Warning!'),_('Scanner not found'))
        
    elif os.name == 'posix':
        # for linux
        import sane
        sane.init()
        devices = sane.get_devices()
        for dev in devices:
            try:
                scanner = sane.open(dev[0])
                str_image = scanner.scan()
                str_image.save('test.bmp')
                scanner.close()
                sane.exit()
                file_open = open('test.bmp', 'rb')
                base_file = base64.encodestring(file_open.read()) #convert scanned file in to binary file
                return base_file
            except:
                continue
        else:
            raise osv.except_osv(('Warning!'),_('Scanner not found'))
    else:
        exit
        
class scan_upload_ir_attachment(osv.osv):
    _inherit = 'ir.attachment'

    def scan_upload_image_get_test(self, cr, uid, res_model, ids, context=None): 
        # create attachment for the scanned file
        image = scan(self, 200)
        title = ("%s %s" % (context.get("name","Untitled Document"), datetime.today().strftime(DEFAULT_SERVER_DATETIME_FORMAT)))+'.bmp' #name related to object with datetime
        parent_id = self.pool.get('document.directory').search(cr, uid, [('ressource_type_id','=',context.get('active_model'))])  # Directory for the created attachment
        if parent_id and parent_id[0]:
            parent_id_updated = parent_id[0]
        else:
            parent_id_updated = 1 or False
        ir_attachment = self.pool.get('ir.attachment').create(cr, uid, 
                                                              {'name': title,
                                                               'datas': image,
                                                               'datas_fname': title,
                                                               'res_model': context.get('active_model'),
                                                               'res_id': context.get('active_ids')[0],
                                                               'parent_id': parent_id_updated,
                                                               'type': 'binary'},
                                                              context=context)  
        return ir_attachment
    
class crm_lead_inherited(osv.osv):
    _inherit = 'crm.lead'
    
    _columns = {
                'upload_file': fields.binary("File"), # Add attachment from form view
    }                

    def scan_upload_image_get_test(self, cr, uid, ids, context=None):
        # create attachment for the scanned file
        
        if context is None:
            context = {}        
        image = scan(self, 200)
        name = self.read(cr, uid, ids)[0]['name'] or "Untitled Document"
        title = "%s %s" % (name, datetime.today().strftime(DEFAULT_SERVER_DATETIME_FORMAT))+'.bmp' #name related to object with datetime
        parent_id = self.pool.get('document.directory').search(cr, uid, [('ressource_type_id','=',context.get('active_model'))])  # Directory for the created attachment
        if parent_id and parent_id[0]:
            parent_id_updated = parent_id[0]
        else:
            parent_id_updated = 1 or False        
        ir_attachment = self.pool.get('ir.attachment').create(cr, uid, 
                                                              {'name': title,
                                                               'datas': image,
                                                               'datas_fname': title,
                                                               'res_model': 'crm.lead',
                                                               'res_id': ids[0],
                                                               'parent_id': parent_id_updated,
                                                               'type': 'binary'},
                                                              context=context)  
        return ir_attachment
    
class sale_order_inherited(osv.osv):
    _inherit = 'sale.order'
    
    _columns = {
                'upload_file': fields.binary("File"), # Add attachment from form view
    }                

    def scan_upload_image_get_test(self, cr, uid, ids, context=None):
        # create attachment for the scanned file
        
        if context is None:
            context = {}
        image = scan(self, 200)
        name = self.read(cr, uid, ids)[0]['name'] or "Untitled Document"
        title = "%s %s" % (name, datetime.today().strftime(DEFAULT_SERVER_DATETIME_FORMAT))+'.bmp' #name related to object with datetime
        parent_id = self.pool.get('document.directory').search(cr, uid, [('ressource_type_id','=',context.get('active_model'))]) # Directory for the created attachment
        if parent_id and parent_id[0]:
            parent_id_updated = parent_id[0]
        else:
            parent_id_updated = 1 or False
        ir_attachment = self.pool.get('ir.attachment').create(cr, uid, 
                                                              {'name': title,
                                                               'datas': image,
                                                               'datas_fname': title,
                                                               'res_model': 'sale.order',
                                                               'res_id': ids[0],
                                                               'parent_id': parent_id_updated,
                                                               'type': 'binary'},
                                                              context=context)  
        return ir_attachment

class customer_service_ticket_inherited(osv.osv):
    _inherit = 'customer_service.ticket' 
        
    _columns = {
                'upload_file': fields.binary("File"), # Add attachment from form view
    }                

    def scan_upload_image_get_test(self, cr, uid, ids, context=None):
        # create attachment for the scanned file
        
        if context is None:
            context = {}
        image = scan(self, 200)
        name = self.read(cr, uid, ids)[0]['name'] or "Untitled Document"
        title = "%s %s" % (name, datetime.today().strftime(DEFAULT_SERVER_DATETIME_FORMAT))+'.bmp' #name related to object with datetime
        parent_id = self.pool.get('document.directory').search(cr, uid, [('ressource_type_id','=',context.get('active_model'))]) # Directory for the created attachment
        if parent_id and parent_id[0]:
            parent_id_updated = parent_id[0]
        else:
            parent_id_updated = 1 or False
        ir_attachment = self.pool.get('ir.attachment').create(cr, uid, 
                                                              {'name': title,
                                                               'datas': image,
                                                               'datas_fname': title,
                                                               'res_model': 'sale.order',
                                                               'res_id': ids[0],
                                                               'parent_id': parent_id_updated,
                                                               'type': 'binary'},
                                                              context=context)  
        return ir_attachment           
