# -*- coding: utf-8 -*-
{
    'name': 'Image Scanner and Uploader',
    'version': '0.2',
    'author': 'Zaptech Solutions',
    'category': 'Tools',
    'description' : """ 
    This module adds One Touch Scan button in attachment menu and 
    Sales Order, Leads, Opportunity form view as well which will scan the image and upload it in OpenERP """ ,
    'installable': True,
    'auto_install': False,
    'js': ['static/src/js/add_scan_button.js'],
    'qweb': ['static/src/xml/add_scan_button.xml'],
    'data': [
        'scan_upload_view.xml'
    ],
    'depends': ['google_docs','sale','crm'],
}
