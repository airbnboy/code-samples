# -*- coding: utf-8 -*-
##############################################################################
# Module by Kranbery Techonolgies LLC
##############################################################################

from osv import osv, fields
from openerp.tools.translate import _
from datetime import *
import decimal_precision as dp

#from tools.translate import _

class outsourcing_manager_contract(osv.osv):

    """ outsourcing contract """
    _name = 'outsourcing_manager.contract'
    _rec_name = 'contract_number'   
    _inherit = ['mail.thread']
    
          
    # Column declarations
    _columns = {
                
                                                                                         
        # outsourcing Info     
        'summary': fields.char('Summary',size=512,required=True, help="Summary of project", readonly=False),             
        'outsourcing_partner_id':  fields.many2one('res.partner', 'Vendor', required=True, domain="[('supplier', '=', True)]", write=['outsourcing_manager.outsourcing_management_manager'],read=['outsourcing_manager.outsourcing_management_user'],create=['outsourcing_manager.outsourcing_management_manager']), #
        'category_ids': fields.char('Outsourcing Type Tags', size=128,required=False, help="Outsourcing Type Tags", readonly=True),         
        'image_medium': fields.related('outsourcing_partner_id','image',type='binary',string='Image', readonly=True),
        
        # 'outsourcingType': fields.related('outsourcing_partner_id','outsourcingType',type='char',string='outsourcing Type'),      
        'title': fields.related('outsourcing_partner_id','title',type='char',string='Title', readonly=True),     
            
                
        # Address info
        'street': fields.related('outsourcing_partner_id','street',type='char',string='Street', readonly=True),
        'street2': fields.related('outsourcing_partner_id','street2',type='char',string='Street2', readonly=True),
        'city': fields.related('outsourcing_partner_id','city',type='char',string='city', readonly=True),  
        'state_id': fields.related('outsourcing_partner_id','state_id',type='many2one', relation="res.country.state", string='State', readonly=True),
        'zip': fields.related('outsourcing_partner_id','zip',type='char',string='Zip', readonly=True),
        'country_id': fields.related('outsourcing_partner_id', 'country_id',
                    type='many2one', relation='res.country', string='Country', readonly=True, states={'done': [('readonly', True)]}),
        
        # contact info
        'phone': fields.related('outsourcing_partner_id','phone',type='char',string='Phone', readonly=True),
        'mobile': fields.related('outsourcing_partner_id','mobile',type='char',string='Mobile', readonly=True),
        'fax': fields.related('outsourcing_partner_id','fax',type='char',string='Fax', readonly=True),     
        'website': fields.related('outsourcing_partner_id','website',type='char',string='Website', readonly=True),   
        'email': fields.related('outsourcing_partner_id','email',type='char',string='Email', readonly=True),  
        'title': fields.related('outsourcing_partner_id','title',type='char',string='Title', readonly=True), 
            
        'details': fields.text('Details', readonly=False, required=True, help='Details'),    
            
        # contract info        
        'contract_date': fields.datetime('Start Date', help="Date of contract issue", readonly=True),

       'outsourcing_contract_owner': fields.many2one('res.users', 'Contract Manager', required=True, help="Owner of the contract"),
       'outsourcing_supplier': fields.many2one('res.users', 'Contract Vendor', required=True, help="Vendor of the contract"),
       
       'Price': fields.float('Price', required=True, default="0.00", digits_compute= dp.get_precision('Account')),
       'Timeframe': fields.char('Timeframe',size=50, default="TBD", required=True, help="Timeframe", readonly=False),
       

        'contract_type': fields.selection([('outsourcing', 'Outsourcing'),('vendor', 'Vendor'),('other', 'Other')],
            'Type', required=True,
            track_visibility='onchange',
            help='Type of contract'), 

        'contract_status': fields.selection([('draft', 'Draft'),('bidding', 'Waiting For Bid'),('pending', 'Pending Review'),('approved', 'Approved'),('declined', 'Declined'),('other', 'Other')],
            'Status', required=True,
            track_visibility='onchange',
            help='Status and priority of contract'), 
                
        #many2one('outsourcing_manager.status_type', 'contract Status', help="Status of the contract", required=True),
        'contract_number': fields.char('contract #',size=256,required=True, help="Number of outsourcing service contract", readonly=True),
        #'contract_priority': fields.selection([('priority_normal', 'Normal'), ('priority_high', 'High'), ('urgent', 'Urgent')], 'Priority', required=True, select=True, help="contract Status"),
        

                                        
    }
    
    _defaults = {
        'contract_status': 'draft',
        'contract_date': lambda *a: datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
        'contract_number': lambda self,cr,uid,context={}: self.pool.get('ir.sequence').get(cr, uid, 'outsourcing_manager.contract'),  # : lambda obj, cr, uid, context: obj.pool.get('outsourcing_manager.contract').seq_get(cr, uid),
        'outsourcing_contract_owner': lambda s, cr, u, c: u,  # current user 

    }
    
    _order = "contract_date desc, contract_status desc"

 
    def _get_cutomer_type_tag_list(self, cr, uid, ids, outsourcing_partner_id, context={}):
        # Get Category ID's          
        category_ids = ''    
        customer = self.pool.get('res.partner').browse(cr, uid, outsourcing_partner_id, context)
        if customer:
            for customer_category_id in customer.category_id:
                category_ids += customer_category_id.name + " / "
        return category_ids
 
    def on_change_partner(self, cr, uid, ids, outsourcing_partner_id, context=None):
        result = {}
        values = {}
        
        
                    
        # Get customer information           
        if outsourcing_partner_id:
            
            # warn if customer has ticket
            #if self.has_active_tickets(cr, uid, ids, partner_id, context):
            #    ticket_warning = 'Attention - Customer currently has active ticket(s) in the system!'
            #else:
            #    ticket_warning = ''
                
                
            partner = self.pool.get('res.partner').browse(cr, uid, outsourcing_partner_id, context=context)
            values = {
                #'ticket_date': current_date_time, 
                #'customer_active_tickets_warning': ticket_warning,      
                'state': 'draft',      
                'name' : partner.name,
                'category_ids' : self._get_cutomer_type_tag_list(cr, uid, ids, outsourcing_partner_id, context),
                'image_medium': partner.image, 
                'street' : partner.street,
                'street2' : partner.street2,
                'city' : partner.city,
                'state_id' : partner.state_id and partner.state_id.id or False,
                'zip' : partner.zip,
                'country_id' : partner.country_id and partner.country_id.id or False,
                'email' : partner.email,
                'phone' : partner.phone,
                'mobile' : partner.mobile,
                'fax' : partner.fax,
                'website' : partner.website,
                
            }
        return {'value' : values}   


    def multi_dup_bid(self, cr, uid, ids, context={}):
        group_id = self.pool.get('ir.model.data').get_object_reference(cr, 1, 'outsourcing_manager', 'outsourcing_management_manager')
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        user_group = user.groups_id
        
        user_group_ids = [g.id for g in user_group]        
        cs_group_id = group_id[1]
        if  cs_group_id not in user_group_ids:
            
            ## Raise your error message
            raise osv.except_osv(_('Access Rights Error'), _("Only managers have access to this function."))
        else:     
            # Check to make sure the tickets are not marked as updatable
            for id in ids:
                #bid = self.pool.get('outsourcing_manager.contract').browse(cr, uid, id)
                self.copy(cr, uid, id, {})
            
        return



outsourcing_manager_contract()





# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: