# -*- coding: utf-8 -*-
##############################################################################
# Module by Kranbery Techonolgies LLC
# Not for individual resale or redistribution without expressed written permission of Kranbery Technologies LLC
# Created July 21st 2013
##############################################################################

{
    'name': 'Outsourcing Manager',
    'version': '1.1',
    'author': 'Kranbery Technologies LLC',
    'category': 'Tools',
    'description': "Provides Outsourcing contract management",
    'website': 'http://www.kranbery.com',
    'summary': 'Outsourcing Manager Contracting by Kranbery',
    'depends': ['base', 'sale', 'account'],
    'data' : [
        'security/outsourcing_manager_security.xml',              
        'security/ir.model.access.csv',
        'outsourcing_manager_view.xml', 
        #'outsourcing_manager_contact_log_view.xml',
    ],    
    'js': ['static/src/js/notifier.js'],
    "active": False,
    "installable": True
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
