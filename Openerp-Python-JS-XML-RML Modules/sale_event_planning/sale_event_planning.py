# -*- coding: utf-8 -*-
##############################################################################
# Module by Kranbery Techonolgies LLC
##############################################################################

from datetime import datetime, timedelta
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp import SUPERUSER_ID
import sale
import account_voucher
import logging
_logger = logging.getLogger(__name__)
import pdb

# main
class sale_event(osv.osv):

    def _display_attending_members(self, cr, uid, ids, field_name, arg, context=None):
        #members_obj = self.pool.get('sale_event.sales_member')
        #members = self.pool.get('sale_event.sales_member').search(cr, uid, [('product_id','=',ids[0])])
        #for events in self.browse(cr, uid, ids, context=context):
            #attendee = events.
        res = {}
        for event_line in self.browse(cr, uid, ids, context=context):
            if event_line.s_event_sales_members:
                event_member_name = ''
                i=1
                for event_member in event_line.s_event_sales_members:   #self.pool.get('sale_event.sales_member').search(cr, uid, [], context=context)                    
                                                    
                    # list for two or more
                    if i > 2:
                        event_member_name = '('+str(i) + ' members attending)'
                                            
                    # Output Event member list   
                    else:
                        event_member_name += event_member.name.name + ', '
                        
                    # increment attendee count
                    i = i + 1
                        
                # output attendee info        
                if event_member_name.endswith(', '): # remove comma at end of string of naem list
                    event_member_name = event_member_name[:-2]                           
                res[event_line.id] = event_member_name
            
            # no event memeber list for this record    
            else:
                res[event_line.id] = '(none)' 
        return res


    def _check_chatter_access(self, cr, uid, ids, field_name, arg, context=None):
        
        res = {}
        for i in ids:
            sale_event_user = self.pool.get('ir.model.data').get_object_reference(cr, 1, 'sale_event_planning', 'sale_event_user')[1]
            sale_event_user_ids = self.pool.get('res.users').search(cr, 1, [('groups_id','=',sale_event_user),('active','=','True')])
        
            sale_event_manager = self.pool.get('ir.model.data').get_object_reference(cr, 1, 'sale_event_planning', 'sale_event_manager')[1]
            sale_event_manager_ids = self.pool.get('res.users').search(cr, 1, [('groups_id','=',sale_event_manager),('active','=','True')])
        
            sale_event_executive = self.pool.get('ir.model.data').get_object_reference(cr, 1, 'sale_event_planning', 'sale_event_executive')[1]
            sale_event_executive_ids = self.pool.get('res.users').search(cr, 1, [('groups_id','=',sale_event_executive),('active','=','True')])  #,('active','=','True')        
            user_ids_groups = sale_event_user_ids + sale_event_manager_ids + sale_event_executive_ids
            '''
            sale_event_all_viewer = self.pool.get('ir.model.data').get_object_reference(cr, 1, 'sale_event_planning', 'sale_event_all_viewer')[1]
            sale_event_all_viewer_ids = self.pool.get('res.users').search(cr, 1, [('groups_id','=',sale_event_all_viewer),('active','=','True')])
'''        
            chatter_invisible = not (uid in user_ids_groups)
            res[i] = chatter_invisible
        #self.write(cr, 1, ids, {'chatter_invisible': chatter_invisible}, context=context)    
        return res
    
            
    _name = 'sale_event.sale_event'
    _rec_name = 's_event_name' 
    _inherit = ['mail.thread']

    _date_name = "s_event_start_date"
    
    
    _columns = {

        # Sales Event state        
        'state': fields.selection([
            ('modify', 'Modify Event'),
            ('draft', 'Draft'),
            ('going', 'Going'),
            ('booking', 'Booking'),            
            ('done', 'Confirmed'),            
            ('nogo', 'Not Going')],
            'Event Status', readonly=True, required=True,
            track_visibility='onchange',
            help='Sales event workflow process'),   
                
        's_event_name': fields.char('Convention', size=256, help='Name of Sales event', required=True),        
        's_event_number': fields.char('Event ID',size=256,requifred=True, help="Unique event ID #", readonly=True),                               
        's_event_start_date': fields.datetime('Start Date', help="Start date of sales event", readonly=False, required=True),
        's_event_end_date': fields.datetime('End Date', help="End date of sales event", readonly=False, required=True),
        's_event_description': fields.char('Booth Space Information', size=256, help='The type of customer i.e. ortodontist, denitst, practicioner, etc', required=True),
        's_event_coordinator': fields.many2one('res.users', 'Coordinator', help="Coordinator of sales event.", readonly=False, required=True),
        's_event_sales_members': fields.one2many('sale_event.sales_member', 'user_id', 'Attending Members', readonly=False, help="The list of users assigned to the ticket group"),  
        's_event_notes': fields.text('Event Notes'),
        
        # Address info
        'location_name': fields.char('Location Name',size=256, help="Location of the event", readonly=False, required=False),  
        'street': fields.char('Address',size=256, help="Address", readonly=False, required=False),    
        'city': fields.char('City',size=256, help="City", readonly=False, required=False),  
        'state_id': fields.many2one('res.country.state', 'State', help="State", readonly=False, required=False),
        'zip': fields.char('Zip',size=256, help="Zip", readonly=False, required=False),
        'country_id': fields.many2one('res.country', 'Country', help="Owner of the ticket", readonly=False, required=False),
        'website': fields.char('Website',size=256,required=False, help="Website address", readonly=False), 
        
        # contact info
        'convention_partner_id':  fields.many2one('res.partner', 'Contact Name', required=True),       
        'phone': fields.related('convention_partner_id','phone',type='char',string='Phone', readonly=True),
        'mobile': fields.related('convention_partner_id','mobile',type='char',string='Mobile', readonly=True),
        'fax': fields.related('convention_partner_id','fax',type='char',string='Fax', readonly=True),       
        'email': fields.related('convention_partner_id','email',type='char',string='Email', readonly=True),  
        
        
        # Calculation sum fields at the bottom
        'attending_members_output': fields.function(_display_attending_members, type='char', size=128, string='Attending Members', readonly=True, help="The list of attending members."),
                
         # System       
        #'chatter_invisible': fields.boolean('Chatter Invisible', readonly=True),  
        'chatter_invisible': fields.function(_check_chatter_access, type='boolean', string='Chatter Invisible', readonly=True, help="Disables message notes for certain users"),
                        
        # Costs
        #'convention_cost': fields.float('Convention Cost', required=False, help="Cost of convention"),
        #'flight_cost': fields.float('Flight Cost', required=False, help="Cost of flight"),
        #'hotel_cost': fields.float('Hotel Cost', required=False, help="Cost of hotel"),
        #'vehicle_cost': fields.float('Vehicle Cost', required=False, help="Cost of vehicle"),
                
        }

    _defaults = {
        'state': 'draft',
        #'s_event_start_date': lambda *a: datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
        #'s_event_end_date': lambda *a: datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
        's_event_number': lambda self,cr,uid,context={}: self.pool.get('ir.sequence').get(cr, uid, 'sale_event.id'),  # : lambda obj, cr, uid, context: obj.pool.get('customer_service.ticket').seq_get(cr, uid),
        'country_id': lambda self, cr, uid, context: self.pool.get('res.country').browse(cr, uid, self.pool.get('res.country').search(cr, uid, [('code','=','US')]))[0].id,
        'chatter_invisible': False # lambda self, cr, uid, c: self._check_chatter_access(cr, uid, [],[], context=c), 
                #'use_exist': lambda self, cr, uid, c: self._use_exist_set(cr, uid, [], context=c),
    }
    
    _order = "s_event_start_date desc, state desc"
    
    
    
    # Helpers
    
    # Event Button Actions
    def attend_event(self, cr, uid, ids, context={}):  
        self.state = 'going'
        
        # Get Sales Events planner group ids to add to the event 
        group_id = self.pool.get('ir.model.data').get_object_reference(cr, 1, 'sale_event_planning', 'sale_event_executive')[1]
        user_ids = self.pool.get('res.users').search(cr, 1, [('groups_id','=',group_id),('active','=','True')])           
        self.message_subscribe_users(cr, 1, ids, user_ids, context=context)        
        return self.write(cr, uid, ids, {'state': 'going'}, context=context)#context

    def book_event(self, cr, uid, ids, context={}):  
        self.state = 'booking'
        
        # Get Sales Events planner group ids to add to the event 
        group_id = self.pool.get('ir.model.data').get_object_reference(cr, 1, 'sale_event_planning', 'sale_event_executive')[1]
        user_ids = self.pool.get('res.users').search(cr, 1, [('groups_id','=',group_id),('active','=','True')])             
        self.message_subscribe_users(cr, 1, ids, user_ids, context=context)           
        return self.write(cr, uid, ids, {'state': 'booking'}, context=context)#context
    
    def modify_event(self, cr, uid, ids, context={}):  
        self.state = 'modify'
                        
        pdb.set_trace()                
        # Get Sales Events planner group ids to add to the event 
        group_id = self.pool.get('ir.model.data').get_object_reference(cr, 1, 'sale_event_planning', 'sale_event_executive')[1]
        user_ids = self.pool.get('res.users').search(cr, 1, [('groups_id','=',group_id),('active','=','True')])           
        self.message_subscribe_users(cr, 1, ids, user_ids, context=context)    
        return self.write(cr, uid, ids, {'state': 'modify'}, context=context)#context

    def complete_booking(self, cr, uid, ids, context={}):  
                
        # Get Sales Events planner group ids to add to the event 
        group_id = self.pool.get('ir.model.data').get_object_reference(cr, 1, 'sale_event_planning', 'sale_event_executive')[1]
        user_ids = self.pool.get('res.users').search(cr, 1, [('groups_id','=',group_id),('active','=','True')])          
        self.message_subscribe_users(cr, 1, ids, user_ids, context=context)    
                
        current_id = context.get('current_id')
        event_member_id = 0
        event_member_name = ''
        
        self.state = 'done'        
        sale_event_obj = self.pool.get('sale_event.sale_event').browse(cr, uid, current_id, context=context)
        if sale_event_obj:
            for event_member in sale_event_obj.s_event_sales_members:
                event_member_id = event_member.name.id
                event_member_name += event_member.name.name + ', '
                
                
                post_vars = {'subject': "Convention Booking",
                            'body': "You have been booked for a convention. Please check your convention calendar.",
                            'user_ids': [(4, event_member_id)],} # Where "4" adds the ID to the list 
                                       # of followers and "3" is the partner ID 
                thread_pool = self.pool.get('mail.thread')
                thread_pool.message_post(
                                         cr, uid, False,
                                         type="notification",
                                         subtype="mt_comment",
                                         context=context,
                                         **post_vars)
                
                
            # post chatter message
            if event_member_name.endswith(', '): # remove comma at end of string of naem list
                event_member_name = event_member_name[:-2]    
            self.message_post(cr, uid, ids, _("The following members have been notified of convention confirmation: " + event_member_name), _( 'Convention Booking Confirmed'), context=context)      
                
            # Log message activity
            _logger.info('[CONVENTION SEND MAIL] The following members have been notified of convention confirmation: ' + event_member_name)
                                                                
        return self.write(cr, uid, ids, {'state': 'done'}, context=context)#context

    def not_going(self, cr, uid, ids, context={}):  
                
        # Get Sales Events planner group ids to add to the event 
        group_id = self.pool.get('ir.model.data').get_object_reference(cr, 1, 'sale_event_planning', 'sale_event_executive')[1]
        user_ids = self.pool.get('res.users').search(cr, 1, [('groups_id','=',group_id),('active','=','True')])            
        self.message_subscribe_users(cr, 1, ids, user_ids, context=context)    
                
        current_id = context.get('current_id')
        event_member_id = 0
        event_member_name = ''
        
        self.state = 'nogo'        
        sale_event_obj = self.pool.get('sale_event.sale_event').browse(cr, uid, current_id, context=context)
        if sale_event_obj:
            for event_member in sale_event_obj.s_event_sales_members:
                event_member_id = event_member.name.id
                event_member_name += event_member.name.name + ', '
                
                
                post_vars = {'subject': "Convention Booking - Not Going",
                            'body': "The following convention is marked as not going. Check your convention calendar",
                            'user_ids': [(4, event_member_id)],} # Where "4" adds the ID to the list 
                                       # of followers and "3" is the partner ID 
                thread_pool = self.pool.get('mail.thread')
                thread_pool.message_post(
                                         cr, uid, False,
                                         type="notification",
                                         subtype="mt_comment",
                                         context=context,
                                         **post_vars)
                
                
            # post chatter message
            if event_member_name.endswith(', '): # remove comma at end of string of naem list
                event_member_name = event_member_name[:-2]    
            self.message_post(cr, uid, ids, _("The following members have been notified of convention cancelation: " + event_member_name), _( 'Convention Not Going'), context=context)      
                
            # Log message activity
            _logger.info('[CONVENTION SEND MAIL] The following members have been notified of convention cancelation: ' + event_member_name)
                                                                
        return self.write(cr, uid, ids, {'state': 'nogo'}, context=context)#context    

    def reset_event(self, cr, uid, ids, context={}):   
                                                                               
        # Get Sales Events planner group ids to add to the event 
        group_id = self.pool.get('ir.model.data').get_object_reference(cr, 1, 'sale_event_planning', 'sale_event_executive')[1]
        user_ids = self.pool.get('res.users').search(cr, 1, [('groups_id','=',group_id),('active','=','True')])           
        self.message_subscribe_users(cr, 1, ids, user_ids, context=context)    
                                                                               
        return self.write(cr, uid, ids, {'state': 'draft'}, context=context)#context       
                
    def modify_event(self, cr, uid, ids, context={}): 
                 
        # Get Sales Events planner group ids to add to the event 
        group_id = self.pool.get('ir.model.data').get_object_reference(cr, 1, 'sale_event_planning', 'sale_event_executive')[1]
        user_ids = self.pool.get('res.users').search(cr, 1, [('groups_id','=',group_id),('active','=','True')])             
        self.message_subscribe_users(cr, 1, ids, user_ids, context=context)    
                 
        self.state = 'modify'
         #context
        return self.write(cr, uid, ids, {'state': 'modify'}, context=context) # {"views": [(self.pool.get('ir.model.data').get_object_reference(cr, uid, 'module', 'view_id')[1]}    
    
    def on_change_partner(self, cr, uid, ids, partner_id, context=None):
        result = {}
        values = {}
        
        
                    
        # Get customer information           
        if partner_id:
            
            partner = self.pool.get('res.partner').browse(cr, uid, partner_id, context=context)
            values = {
                'email' : partner.email,
                'phone' : partner.phone,
                'mobile' : partner.mobile,
                'fax' : partner.fax,                
            }
        return {'value' : values} 

sale_event()


class sale_event_member(osv.osv):
    """ Sales_event_sales_memeber """
    _name = 'sale_event.sales_member'
    _rec_name = 'name'
    _columns = {
        
     
        # User Information
        'user_id': fields.many2one('sale_event.sale_event', 'Attending Member'),
        'name':  fields.many2one('res.users', 'Name', required=True),            
        'convention_confirmed': fields.boolean(''),
        'convention_notes': fields.text('Convention Details'),     
        'flight_confirmed': fields.boolean(''),
        'flight_notes': fields.text('Flight Details'),  
        'hotel_confirmed': fields.boolean(''),
        'hotel_notes': fields.text('Hotel Details'), 
        'vehicle_confirmed': fields.boolean(''),
        'vehicle_notes': fields.text('Car Details'), 
  
         
    }    

    _order = "name"
    
    _defaults = {

    }
        
sale_event_member()

# inherited
class sale_order(osv.osv):
    _name = "sale.order"
    _inherit = "sale.order"

    _columns = {
             
        # User Information
        'sale_event_id': fields.many2one('sale_event.sale_event', 'Convention'),   
        #'s_event_start_date': fields.datetime('Start Date', help="Start date of sales event", readonly=False, required=True),  
        's_event_start_date': fields.related('sale_event_id','s_event_start_date',type='date',string='Con Start Date', readonly=False),
    }  

sale_order()

# inherited
class account_voucher(osv.osv):
    _name = "account.voucher"
    _inherit = "account.voucher"

    _columns = {
             
        # User Information
        'sale_event_id': fields.many2one('sale_event.sale_event', 'Sale Event'),     
    }  
    
account_voucher()




# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: