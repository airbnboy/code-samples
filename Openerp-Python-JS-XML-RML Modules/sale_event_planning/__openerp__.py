# -*- coding: utf-8 -*-
##############################################################################
# Module by Kranbery Techonolgies LLC
# Not for individual resale or redistribution without expressed written permission of Kranbery Technologies LLC
# Created July 21st 2013
##############################################################################

{
    'name': 'Sales Event Attendance Planning (Conventions)',
    'version': '1.1',
    'author': 'Kranbery Technologies LLC',
    'category': 'Tools',
    'description': "Provides sales event/convention planning, analysis of profits and purchases for event.",
    'website': 'http://www.kranbery.com',
    'license': 'Other proprietary',
    'summary': 'Sales event planning by Kranbery',
    'depends': ['base','mail','sale','account_voucher'],
    'data' : [
        'security/sale_event_planning_security.xml',              
        'security/ir.model.access.csv',
        'sale_event_planning_view.xml', 
    ],    
    #'js': ['static/src/js/notifier.js'],
    'css': [ 'static/src/css/sale_event_planning.css' ],
    "active": False,
    "installable": True
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
